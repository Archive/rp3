/* mainwin.h
 *
 * Copyright (C) 1999 Red Hat, Inc.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef RP3_MAINWIN_H
#define RP3_MAINWIN_H

#include <config.h>
#include <gnome.h>
#include "account.h"
#include "modem.h"

GtkWidget* rp3_mainwin_new(void);
void       rp3_set_tooltip(GtkWidget* w, const gchar* tip);
void       rp3_editable_enters   (GtkWindow* win,
                                  GtkEditable * editable);

/* mainwin owns any accounts added */
void       rp3_mainwin_add_account(GtkWidget* w, Account* a);
void       rp3_mainwin_add_modem (GtkWidget* w, Modem *modem);
/* destroys the account object */
void       rp3_mainwin_remove_account(GtkWidget* w, Account* a);
void       rp3_mainwin_account_changed(GtkWidget* w, Account* a);
/* Reload status of each account */
void       rp3_mainwin_update_accounts(GtkWidget* w);
void       rp3_mainwin_modem_changed(GtkWidget* w, Modem *modem);

/* Sets up the initial druid. */
void       initial_add_window (GtkWidget *mainwin);

void       rp3_exit(int status);

#endif
