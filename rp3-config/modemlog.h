#ifndef __MODEMLOG_H__
#define __MODEMLOG_H__

#include <config.h>
#include <gnome.h>
#include "wvlogrcv.h"

class ModemLog : public WvLogRcv
{
public:
	ModemLog(gint pipe1);
	virtual ~ModemLog();

	virtual void log(const WvLog *source, int loglevel,
			 const char *_buf, size_t len);
  	virtual void _begin_line();
	virtual void _mid_line(const char *str, size_t len);
	virtual void _end_line();

private:
	gint outfd;
};

#endif

