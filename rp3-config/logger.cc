/* -*- mode: C; c-file-style: "linux" -*- */

#include <config.h>
#include <gnome.h>
#include <unistd.h>
#include <errno.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <signal.h>
#include <stdio.h>
#include "logger.h"
#include "wvconf.h"
#include "wvdialer.h"
#include "modemlog.h"
#include "wvlog.h"
#include <signal.h>

/* These are from glibconfig-sysdefs.h */
//#define GLIB_SYSDEF_POLLIN 1
//#define GLIB_SYSDEF_POLLHUP 16

typedef struct  {
	GtkWidget *window;
	GtkWidget *text;
	GtkWidget *filesel;
	GtkWidget *cancel_button;
	gint in_tag;
	gint pid;
} LoggerInfo;

static void logger_cancel (GtkWidget *widget, gpointer data);


#define BUFSIZE 1024
static void
make_sensitive(GtkWidget *widget)
{
	gtk_widget_set_sensitive(GTK_WIDGET(widget), TRUE);
}

static void
nullify(GtkWidget *widget, gpointer data)
{
	gpointer *p = (gpointer*)data;
	*p = NULL;
}

static void
widget_destroy_if_not_null(GtkWidget *widget, gpointer data)
{
	gpointer *p = (gpointer*)data;
	if(*p != NULL) {
		gtk_widget_destroy(GTK_WIDGET(*p));
	}
	*p = NULL;
}

static void
logger_append (LoggerInfo *info, gchar *buffer, gint count)
{
	GtkAdjustment *adj;

	if (count < 0)
		count = strlen (buffer);

	if (info->text != NULL) {
		g_assert(GTK_IS_TEXT(info->text));
		gtk_text_insert (GTK_TEXT (info->text), NULL, NULL, NULL,
				 buffer, count);
		adj = GTK_TEXT(info->text)->vadj;
		gtk_adjustment_set_value (adj, adj->upper - adj->page_size);
	}
}

static void
logger_save_ok (GtkWidget *widget, gpointer data)
{
	LoggerInfo *info = (LoggerInfo *)data;
	gchar *filename;
	gchar *str;
	FILE *outfile;
	gboolean success = TRUE;

	filename = gtk_file_selection_get_filename (GTK_FILE_SELECTION (info->filesel));

	outfile = fopen (filename, "w");
	if (outfile == NULL) {
		gchar *str = g_strdup_printf ("Error opening '%s': %s\n",
					      filename,
					      g_strerror(errno));
		GtkWidget *msgbox = gnome_message_box_new (str, GNOME_MESSAGE_BOX_ERROR,
							   GNOME_STOCK_BUTTON_OK, NULL);
		gnome_dialog_set_parent (GNOME_DIALOG (msgbox),
					 GTK_WINDOW (info->filesel));
		gnome_dialog_run_and_close (GNOME_DIALOG (msgbox));
		g_free (str);
		return;
	}

	str = gtk_editable_get_chars (GTK_EDITABLE (info->text), 0, -1);
	if (fputs (str, outfile) == EOF) {
		gchar *str = g_strdup_printf ("Error writing file '%s': %s\n",
					      filename,
					      g_strerror(errno));
		GtkWidget *msgbox = gnome_message_box_new (str, GNOME_MESSAGE_BOX_ERROR,
							   GNOME_STOCK_BUTTON_OK, NULL);
		gnome_dialog_set_parent (GNOME_DIALOG (msgbox),
					 GTK_WINDOW (info->filesel));
		gnome_dialog_run_and_close (GNOME_DIALOG (msgbox));
		g_free (str);
	}

	g_free (str);
	fclose (outfile);
}

static void
logger_save (GtkWidget *widget, gpointer data)
{
	LoggerInfo *info = (LoggerInfo *)data;

	info->filesel = gtk_file_selection_new (_("Save Log File..."));
	gtk_signal_connect(GTK_OBJECT(info->filesel), "destroy",
			   GTK_SIGNAL_FUNC(nullify), &info->filesel);
	gtk_file_selection_hide_fileop_buttons (GTK_FILE_SELECTION (info->filesel));
	gtk_signal_connect(GTK_OBJECT(widget), "destroy",
			   GTK_SIGNAL_FUNC(widget_destroy_if_not_null),
			   &info->filesel);
	gtk_file_selection_set_filename (GTK_FILE_SELECTION (info->filesel),
					 "/tmp/ppp.log");
	gtk_signal_connect_object(GTK_OBJECT(info->filesel), "destroy",
				  GTK_SIGNAL_FUNC(make_sensitive),
				  GTK_OBJECT(info->window));
	gtk_widget_set_sensitive (info->window, FALSE);
	gtk_window_set_transient_for (GTK_WINDOW (info->filesel),
				      GTK_WINDOW (info->window));
	gtk_window_set_modal (GTK_WINDOW (info->filesel), TRUE);

	gtk_signal_connect (GTK_OBJECT (GTK_FILE_SELECTION (info->filesel)->ok_button),
			    "clicked", GTK_SIGNAL_FUNC (logger_save_ok), info);
	gtk_signal_connect_object (GTK_OBJECT (GTK_FILE_SELECTION (info->filesel)->ok_button),
				   "clicked",
				   GTK_SIGNAL_FUNC (gtk_widget_destroy),
				   GTK_OBJECT(info->filesel));
	gtk_signal_connect_object (GTK_OBJECT (GTK_FILE_SELECTION (info->filesel)->cancel_button),
				   "clicked",
				   GTK_SIGNAL_FUNC (gtk_widget_destroy),
				   GTK_OBJECT(info->filesel));

	gtk_widget_show_all (info->filesel);
}

static void
logger_cancel (GtkWidget *widget, gpointer data)
{
	LoggerInfo *info = (LoggerInfo *) data;

	if(widget != NULL) {
		logger_append (info, _("\nDIALING CANCELED BY USER\n"), -1);
	}

	if (info->pid != 0) {
		if (kill (info->pid, SIGTERM) < 0) {
			fprintf (stderr, "Error killing child %d: %s\n",
				 info->pid, g_strerror (errno));
		}
	}

	if (info->cancel_button) {
		gtk_widget_set_sensitive (info->cancel_button, FALSE);
	}
}

static void
logger_read (gpointer data, gint source, GdkInputCondition condition)
{
	LoggerInfo *info = (LoggerInfo *) data;
	char buffer[BUFSIZE];
	int count = 0;

	if(condition == GDK_INPUT_READ) {
		count = read (source, buffer, BUFSIZE);
	}

	if(count > 0) {
		logger_append (info, buffer, count);
	} else {
#if 0
		g_warning ("Error reading from child: %s\n",
			   g_strerror (errno));
#endif
		logger_cancel(NULL, info);

		waitpid (info->pid, NULL, 0);
		info->pid = 0;

		close(source);

		if(info->in_tag != 0) {
			gdk_input_remove(info->in_tag);
			info->in_tag = 0;
		}
	}
}

/* used only in logger_spawn_command */
static int want_to_die = false;
static void
logger_signal_handler(int signum)
{
	want_to_die = true;
}

static void
logger_spawn_command (LoggerInfo *info, Account *account)
{
	int fds[2];
	pid_t pid;

	if (pipe (fds) < 0) {
		g_warning ("Cannot create pipe: %s\n", g_strerror (errno));
		exit (1);
	}
	pid = fork();

	if (pid == 0) { /* Child */
		close (0);
		close (1);
		close (2);
		setpgid (0, 0);
		close (fds[0]);
		dup2 (fds[1], 1);
		close (fds[1]);

		WvConf cfg ("/etc/wvdial.conf");

		signal(SIGTERM, logger_signal_handler);
		signal(SIGINT, logger_signal_handler);
		signal(SIGHUP, logger_signal_handler);

		WvStringList *sections;
		sections = new WvStringList;

		ModemLog log (1);
		sections->append(new WvString(account->wvdial_sect), TRUE);

		WvDialer *dialer;
		dialer= new WvDialer(cfg,sections);
		if ((dialer->status () != WvDialer::ModemError) &&
		    (dialer->status () != WvDialer::OtherError)) {
			dialer->dial ();
			while( !want_to_die && dialer->isok() 
			      && dialer->status() != WvDialer::Idle ) {
				dialer->select( 100 );
				dialer->execute();
			
				if (dialer->weird_pppd_problem) {
					dialer->weird_pppd_problem = false;
					g_print (_("pppd error!  Look for an explanation in /var/log/messages.\n"));
				}
			}

			dialer->hangup();
		} else {
			g_print (_("ERROR: Unable to initialize modem\n"));
		}
		g_print (_("\nDIALING FINISHED\n"));
		delete (dialer);
		_exit (0);
	} else if (pid < 0) {
		g_warning (_("Cannot fork: %s\n"), g_strerror (errno));
		exit (1);
	}

	/* Parent */
	info->pid = pid;
	info->in_tag = gdk_input_add(fds[0], GDK_INPUT_READ,
				     logger_read, info);
	close (fds[1]);
}

static void
close_cb(GtkWidget *widget, gpointer data)
{
	LoggerInfo *info;

	info = (LoggerInfo*) data;

	logger_cancel(NULL, info);

	if(info->in_tag != 0) {
		gdk_input_remove (info->in_tag);
		info->in_tag = 0;
	}

	if(info->window != NULL) {
		gtk_widget_destroy(info->window);
	}

	g_free(info);
}

static LoggerInfo *
create_logger_window (Account *account)
{
	GtkWidget *vbox, *bbox;
	GtkWidget *button;
	GtkWidget *scrollwin;
	GtkWidget *separator;
	LoggerInfo *info;

	info = g_new (LoggerInfo, 1);

	info->window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	gtk_window_set_title (GTK_WINDOW (info->window),
			      _("Trying to connect..."));
	gtk_window_set_default_size (GTK_WINDOW (info->window), 400, 300);

	vbox = gtk_vbox_new (FALSE, GNOME_PAD);
	gtk_container_set_border_width (GTK_CONTAINER (vbox), GNOME_PAD);
	gtk_container_add (GTK_CONTAINER (info->window), vbox);
	gtk_widget_show(vbox);

	scrollwin = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrollwin),
					GTK_POLICY_NEVER, GTK_POLICY_AUTOMATIC);
	gtk_box_pack_start (GTK_BOX (vbox), scrollwin, TRUE, TRUE, 0);
	gtk_widget_show(scrollwin);

	info->text = gtk_text_new (NULL, NULL);
	gtk_container_add (GTK_CONTAINER (scrollwin), info->text);
	gtk_signal_connect(GTK_OBJECT(info->text), "destroy",
			   GTK_SIGNAL_FUNC(nullify), &info->text);
	gtk_widget_show(info->text);

	separator = gtk_hseparator_new ();
	gtk_box_pack_start (GTK_BOX (vbox), separator, FALSE, FALSE, 0);
	gtk_widget_show(separator);

	bbox = gtk_hbutton_box_new ();
	gtk_box_pack_start (GTK_BOX (vbox), bbox, FALSE, FALSE, 0);
	gtk_widget_show(bbox);

	button = gnome_pixmap_button (gnome_stock_pixmap_widget (NULL, GNOME_STOCK_PIXMAP_SAVE),
				      _("Save Log..."));
	gtk_signal_connect (GTK_OBJECT (button), "clicked",
			    GTK_SIGNAL_FUNC (logger_save), info);
	gtk_container_add (GTK_CONTAINER (bbox), button);
	gtk_widget_show(button);

	info->cancel_button = gnome_stock_button (GNOME_STOCK_BUTTON_CANCEL);
	gtk_signal_connect (GTK_OBJECT (info->cancel_button), "clicked",
			    GTK_SIGNAL_FUNC (logger_cancel), info);
	gtk_signal_connect (GTK_OBJECT (info->cancel_button), "destroy",
			    GTK_SIGNAL_FUNC (nullify), &info->cancel_button);
	gtk_container_add (GTK_CONTAINER (bbox), info->cancel_button);
	gtk_widget_show(info->cancel_button);

	button = gnome_stock_button (GNOME_STOCK_BUTTON_CLOSE);
	gtk_signal_connect (GTK_OBJECT (button), "clicked",
			    GTK_SIGNAL_FUNC (close_cb), info);
	gtk_container_add (GTK_CONTAINER (bbox), button);
	gtk_widget_show(button);

	logger_spawn_command (info, account);

	/* we need to free info when we close the window */
	return info;
}

void
account_dial_and_log (GtkWidget *mainwin, Account *account)
{
	LoggerInfo *info = NULL; 
	GtkWidget* window;

	info = create_logger_window (account);
	window = info->window;

	gtk_window_set_transient_for (GTK_WINDOW (window), GTK_WINDOW (mainwin));
	gtk_widget_set_sensitive(mainwin, FALSE);
	gtk_signal_connect_object(GTK_OBJECT(window), "destroy",
				  GTK_SIGNAL_FUNC(make_sensitive),
				  GTK_OBJECT(mainwin));
	gtk_signal_connect(GTK_OBJECT(window), "destroy",
			   GTK_SIGNAL_FUNC(nullify), &info->window);

	gtk_window_set_modal(GTK_WINDOW(window), TRUE);
	gtk_widget_show_all (window);
}

