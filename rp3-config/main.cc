/* main.cc
 *
 * Copyright (C) 1999 Red Hat, Inc.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <gnome.h>

#include "mainwin.h"

#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <signal.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <ctype.h>
#include "editwin.h"
#include "modem.h"
#include <netreport.h>
#include <pwd.h>

static gint session_die(GnomeClient* client, gpointer client_data);

static gint save_session(GnomeClient *client, gint phase, 
                         GnomeSaveStyle save_style,
                         gint is_shutdown, GnomeInteractStyle interact_style,
                         gint is_fast, gpointer client_data);

static char* interface  = NULL;

struct poptOption options[] = {
  { 
    "interface", 'i', POPT_ARG_STRING,
    &interface, 0,
    N_("Specify an interface to edit."),
    N_("INTERFACE NAME")
  },
  { NULL,'\0', 0, NULL, 0, NULL, NULL }
};

static int sigio_notify_pipe[2];

static void
connection_changed_cb(gpointer	    data,
                      gint		source,
                      GdkInputCondition condition)
{
  GtkWidget* mainwin = (GtkWidget*)data;
  char b;

  read(sigio_notify_pipe[0], &b, 1);

  rp3_mainwin_update_accounts(mainwin);
}

static void
signal_handler (int signo)
{
  /* If this fails there's really nothing to be done */
  if (signo == SIGIO)
    write(sigio_notify_pipe[1], "g", 1);
}

int 
main(int argc, char* argv[])
{
  struct sigaction act;
  sigset_t empty_mask;

  GtkWidget* mainwin;  
  poptContext pctx;
  const char** args;

  GnomeClient* client;

  bindtextdomain(PACKAGE, GNOMELOCALEDIR);  
  textdomain(PACKAGE);

  if (geteuid() != getuid())
    {
      struct passwd* sp;

      sp = getpwuid(getuid());

      setenv("HOME", sp->pw_dir, TRUE);

      gnome_client_disable_master_connection ();
    }
  
  gnome_init_with_popt_table(PACKAGE, VERSION, argc, argv, 
                             options, 0, &pctx);  

  /* Argument parsing */

  args = poptGetArgs(pctx);
  
  if (args != NULL)
    {
      GtkWidget* dialog;
      fprintf(stderr, _("Args aren't allowed!"));
      dialog = gnome_error_dialog(_("Invalid arguments passed to the Internet dialup configuration tool"));
      gnome_dialog_run_and_close(GNOME_DIALOG(dialog));
      return 1;
    }

  poptFreeContext(pctx);

  if (geteuid() != 0)
    {
      GtkWidget* dialog;

      dialog = gnome_error_dialog(_("You must have root (superuser) privileges\nto run the Internet dialup configuration tool"));

      gnome_dialog_run_and_close(GNOME_DIALOG(dialog));
      return 1;
    }
  
  /* Session Management */

  client = gnome_master_client ();
  if (client) {
	  gtk_signal_connect (GTK_OBJECT (client), "save_yourself",
			      GTK_SIGNAL_FUNC (save_session), argv[0]);
	  gtk_signal_connect (GTK_OBJECT (client), "die",
			      GTK_SIGNAL_FUNC (session_die), NULL);
  }
  /* Main app */

  mainwin = rp3_mainwin_new();
  gtk_window_set_default_size (GTK_WINDOW (mainwin), 500, 300);
  gtk_widget_show(mainwin);

  if (pipe(sigio_notify_pipe) < 0)
    {
      gchar* err = g_strconcat(_("Failed to open internal communication pipeline: "), strerror(errno), _("\nWon't notice if connection status changes."), NULL);
      gnome_error_dialog_parented(err, GTK_WINDOW(mainwin));
      g_free(err);
      sigio_notify_pipe[0] = -1;
      sigio_notify_pipe[1] = -1;
    }
  else
    {
      gdk_input_add(sigio_notify_pipe[0],
                    GDK_INPUT_READ,
                    connection_changed_cb,
                    mainwin);
    }

  sigemptyset (&empty_mask);
  act.sa_handler = signal_handler;
  act.sa_mask    = empty_mask;
  act.sa_flags   = 0;
  sigaction (SIGIO,  &act, 0);

  if (netreport_subscribe() != 0)
    {
      gnome_error_dialog_parented(_("Unable to monitor internet connection status.\nDials and hangups won't necessarily be displayed correctly."), GTK_WINDOW(mainwin));
    }
  
  if (!check_conf (NULL))
    {
      initial_add_window (mainwin);
    }

  gtk_main();

  /* Calls unsubscribe */
  rp3_exit(0);
  
  return 0; /* not reached */
}

static gint
save_session (GnomeClient *client, gint phase, GnomeSaveStyle save_style,
              gint is_shutdown, GnomeInteractStyle interact_style,
              gint is_fast, gpointer client_data)
{
  gchar** argv;
  guint argc;

  /* allocate 0-filled, so it will be NULL-terminated */
  argv = (gchar**)g_malloc0(sizeof(gchar*)*4);
  argc = 1;

  argv[0] = (gchar*)client_data;
  
  gnome_client_set_clone_command (client, argc, argv);
  gnome_client_set_restart_command (client, argc, argv);

  return TRUE;
}

static gint 
session_die(GnomeClient* client, gpointer client_data)
{
  rp3_exit(0);
  return TRUE;
}
