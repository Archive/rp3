/* modem-edit.h
 *
 * Copyright (C) 1999 Red Hat, Inc.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef __MODEM_EDIT_H__
#define __MODEM_EDIT_H__
#include <gnome.h>
#include <wvconf.h>
#include "modem.h"

typedef struct _ModemEdit ModemEdit;

struct _ModemEdit {
	GtkWidget *widget; /* A table */
	GtkWidget *device; /* A combo-box */
	GtkWidget *baud;   /* A combo-box */
	GtkWidget *volbut; /* A check-box */
	GtkWidget *volume; /* An hscale1 */
	GtkWidget *pulse;  /* A check-box */
};

ModemEdit *create_modem_edit ();
void       create_modem_edit_window (GtkWidget *mainwin, Modem *modem, GSList *list);
gboolean   check_modem_edit (ModemEdit *edit);
void       free_modem_edit ();
void       set_modem_edit (ModemEdit *edit, gchar *section, WvConf *cfg);
void       save_modem_edit (ModemEdit *edit, gchar *section, WvConf *cfg);

#endif
