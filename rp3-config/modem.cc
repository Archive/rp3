#include <config.h>
#include "modem.h"
#include "wvmodemscan.h"
#include "wvconf.h"
#include <ctype.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>
#include "modemlog.h"
#include "mainwin.h"

static gint pid = 0;
static guint msg_input_handler;
static guint format_input_handler;
static gint end_count;

static gboolean
check_conf_section (gchar *section, WvConf *cfg)
{
	const gchar *test;
	gint test_int;

	/* other fields are optional. These are mandatory */
	test = cfg->get(section, "Modem");
	if (test == NULL)
		return FALSE;
	test_int = cfg->getint(section, "Baud", 0);
	if (test_int == 0)
		return FALSE;

	test = cfg->get(section, "Init1");
	if (test == NULL)
		return FALSE;

	return TRUE;
}

ConfStatus
check_conf (WvConf *cfg)
{
	ConfStatus retval;
	gboolean free_conf = FALSE;

	if (!g_file_test ("/etc/wvdial.conf", G_FILE_TEST_ISFILE))
		return CONF_NONEXISTENT;

	if (cfg == NULL) {
		cfg = new WvConf ("/etc/wvdial.conf");
		free_conf = TRUE;
	}

	if (!cfg->isok ())
		return CONF_NONEXISTENT;

	if (!check_conf_section ("Modem0", cfg))
		return CONF_NONEXISTENT;

	retval = CONF_MODEM_FOUND;

	if (check_conf_section ("Modem1", cfg))
		retval = CONF_MODEMS_FOUND;

	if (check_conf_section ("Dialer Defaults", cfg)) {
		if (retval == CONF_MODEM_FOUND)
			retval = CONF_MODEM_DEFAULT_FOUND;
		else
			retval = CONF_MODEMS_DEFAULT_FOUND;
	}
	if (free_conf)
		delete cfg;
	return retval;
}

void
copy_sections (WvConf *cfg, gchar *section1, gchar *section2)
{
	const gchar *val;
	gint baud;

	g_return_if_fail(cfg != NULL);
	g_return_if_fail(section1 != NULL);
	g_return_if_fail(section2 != NULL);

	val = cfg->get (section1, "Modem");
	cfg->set (section2, "Modem", val);
	baud = cfg->getint(section1, "Baud", 0);
	cfg->setint(WvString(section2), WvString("Baud"), baud);
	val = cfg->get (section1, "Init1");
	cfg->set (section2, "Init1", val);
	val = cfg->get (section1, "Init2");
	cfg->set (section2, "Init2", val);
	val = cfg->get (section1, "Init3");
	cfg->set (section2, "Init3", val);
	val = cfg->get (section1, "Init4");
	cfg->set (section2, "Init4", val);
	val = cfg->get (section1, "Dial Command");
	cfg->set (section2, "Dial Command", val);
}
static void
format_read_func (gpointer	    data,
		  gint		    source,
		  GdkInputCondition condition)
{
	GtkWidget *dialog;
	GtkWidget *progress;
	GtkAdjustment *adjustment;
	int size;
	char buf[LINE_MAX];
	gchar *str;
	gint len;
	gint percent;

	g_return_if_fail (GNOME_IS_DIALOG (data));
	dialog = GTK_WIDGET (data);
	memset(buf, '\0', sizeof(buf));
	while ((size = read(source,buf,sizeof(buf)-1)) != -1) {

		if(size == 0) {
			break;
		}

		buf[size] = '\000'; /* Make sure it's NULL terminated */

		str = buf;
		while (*str) {
			switch (str[0]) {
			case 'B': /* BC##BC */
				/* BC stands for Beginning Count */
				/* just as a sanity check...\n*/
				if (strlen (str) <5) {
					*str = '\000';
					break;
				}
				while(!isdigit(*str) && (*str != '\0')) {
					str++;
				}
				len = 0;
				while(isdigit(str[len]) && (str[len] != '\0')){
					len++;
				}
				str[len] = '\000'; /* Nuke the B  (-:  */
				progress = GTK_WIDGET (gtk_object_get_data (GTK_OBJECT (dialog), "progress"));
				GTK_PROGRESS (progress)->adjustment->upper = atoi (str);
				str += (len + 2); /*go past the second BC */
				break;

			case 'E': /* EC##EC */
				/* EC stands for End Count */
				/* just as a sanity check...\n*/
				if (strlen (str) <5) {
					*str = '\000';
					break;
				}
				str += 2;
				if (str[1] == 'E')
					len = 1;
				else
					len = 2;
				str[len] = '\000'; /* Nuke the E  (-:  */
				end_count = atoi (str);
				str += (len + 2); /*go past the second EC */
				break;

			case 'U': /* UPDATE */
				/* Sanity check */
				progress = GTK_WIDGET (gtk_object_get_data (GTK_OBJECT (dialog), "progress"));
				if ((strlen (str) < strlen ("UP###UP")) ||
				    (progress == NULL))  {
					*str = '\000';
					break;
				}
				str += strlen ("UP");
				str[3] = '\000';
				percent = atoi (str);
				gtk_progress_set_percentage (GTK_PROGRESS (progress),
							     CLAMP (percent/100.0, 0.0, 1.0));
				str += 5;
				break;

			case 'D': /* DONE */
				/* Sanity check */
				if ((strlen (str) >= strlen ("DONE")) &&
				    (strncmp (str, "DONE", strlen ("DONE")) == 0))  {
					gtk_widget_destroy (GTK_WIDGET (data));
				}
				*str = '\000';/* force an exit, even if we aren't through with the read. */
				break;
			default:
				break;
			}
		}
		memset(buf, '\0', sizeof(buf));
	}
	if (pid == 0) {
		/* Catch if our child dies */
		gtk_input_remove (format_input_handler);
		format_input_handler = 0;
	}
}

static const gchar *
strrstr (const gchar *haystack, const gchar *needle)
{
	const gchar *str;
	const gchar *ptr;

	ptr = NULL;
	str = haystack;

	while ((str = strstr (str, needle)) != NULL) {
		if (*str == '\000')
			break;
		ptr = str;
		str += 1;
	}
	return ptr;
}

static void
msg_read_func (gpointer	    data,
	       gint		    source,
	       GdkInputCondition condition)
{
	GtkWidget *dialog;
	GtkWidget *label;
	GtkWidget *progress;
	int size;
	char buf[LINE_MAX];
	const gchar *str;

	g_return_if_fail (GNOME_IS_DIALOG (data));
	dialog = GTK_WIDGET (data);
	label = GTK_WIDGET (gtk_object_get_data (GTK_OBJECT (dialog), "label"));
	while ((size = read(source,buf,sizeof(buf)-1))>0) {
		buf[size] = '\000'; /* Make sure it's NULL terminated */

		str = strrstr (buf, "NEW_MSG");
		if ((str == NULL) || (strlen (str) < strlen ("NEW_MSG")))
			continue;
		gtk_label_set_text (GTK_LABEL (label), str + strlen ("NEW_MSG"));
	}
	if (pid == 0) {
		/* Catch if our child dies */
		gtk_input_remove (msg_input_handler);
		msg_input_handler = 0;
	}
}

static void
msg_write_func (WvModemScanList &l, int fd)
{
	gchar *msg = NULL;
        WvModemScanList::Iter i(l);

	if (l.isdone ())
	        return;

	for (i.rewind(); i.next(); ) {
		if (!i().isdone()) break;
	}
	switch (i().get_stage ()) {
	case WvModemScan::Startup:
		msg = g_strdup_printf (_("%sLooking at %s"), "NEW_MSG",(const gchar *)i().filename ());
		break;
	case WvModemScan::AT:
	case WvModemScan::ATZ:
	case WvModemScan::ATS0:
	case WvModemScan::Carrier:
		msg = g_strdup_printf (_("%sChecking for modem and dial tone"), "NEW_MSG");
		break;
	case WvModemScan::DTR:
	case WvModemScan::FastDial:
	case WvModemScan::FCLASS:
	case WvModemScan::GetIdent:
		msg = g_strdup_printf (_("%sDetermining modem type"), "NEW_MSG");
		break;
	case WvModemScan::BaudStep:
	case WvModemScan::Reinit:
		msg = g_strdup_printf (_("%sDetermining modem speed"), "NEW_MSG");
		break;
	default:
		msg = g_strdup_printf (_("%sFinished looking at %s"), "NEW_MSG",(const gchar *)i().filename ());
	}

	if (msg) {
		write (fd, msg, strlen (msg));
		g_free (msg);
	}
}

static void signal_handler (int parm)
{
	pid = 0;
}

void
create_modem_dialog (GtkWidget *parent, gchar *section)
{
	GtkWidget *dialog;
	GtkWidget *hbox;
	GtkWidget *vbox;
	GtkWidget *logo;
	GtkWidget *label;
	GtkWidget *progress;
	GtkRcStyle *rc_style;
	gint msg_pipe[2];
	gint format_pipe[2];
	gint status;

	end_count = 0;

	dialog = gnome_dialog_new (_("Searching for modems"), GNOME_STOCK_BUTTON_CANCEL, NULL);
	gtk_window_set_default_size (GTK_WINDOW (dialog), 400, 300);
	logo = gnome_pixmap_new_from_file_at_size ("/usr/share/pixmaps/rp3/rp3-icon.png", 48, 48);

	gtk_widget_show (logo);
//	gtk_widget_set_usize (logo, 48, 48);

	hbox = gtk_hbox_new (FALSE, GNOME_PAD);
	gtk_box_pack_start (GTK_BOX (GNOME_DIALOG (dialog)->vbox), hbox, TRUE, TRUE, 0);
	if (logo)
		gtk_box_pack_start (GTK_BOX (hbox), logo, FALSE, FALSE, GNOME_PAD_BIG);

	vbox = gtk_vbox_new (FALSE, GNOME_PAD_SMALL);
	gtk_box_pack_start (GTK_BOX (hbox), vbox, TRUE, TRUE, 0);
	label = gtk_label_new (_("Searching for any modems..."));
	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
	gtk_box_pack_start (GTK_BOX (vbox), label, FALSE, FALSE, 0);
	label = gtk_label_new ("");
	gtk_label_set_justify (GTK_LABEL (label), GTK_JUSTIFY_LEFT);
	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
	gtk_box_pack_start (GTK_BOX (vbox), label, FALSE, FALSE, 0);
	gtk_object_set_data (GTK_OBJECT (dialog), "label", label);

	progress = gtk_progress_bar_new ();
	gtk_box_pack_start (GTK_BOX (vbox), progress, FALSE, FALSE, 0);
	gtk_object_set_data (GTK_OBJECT (dialog), "progress", progress);
	gtk_widget_show_all (GNOME_DIALOG (dialog)->vbox);

	if ((pipe (msg_pipe) < 0) || (pipe (format_pipe) < 0))  {
		g_error ("unable to make a pipe\nexitting...\n");
		rp3_exit (1);
	}

	msg_input_handler = gtk_input_add_full (msg_pipe[0], GDK_INPUT_READ, msg_read_func, NULL, dialog, NULL);
	format_input_handler = gtk_input_add_full (format_pipe[0], GDK_INPUT_READ, format_read_func, NULL, dialog, NULL);
	fcntl(msg_pipe[0], F_SETFL, O_NONBLOCK);
	fcntl(format_pipe[0], F_SETFL, O_NONBLOCK);
	signal (SIGCHLD, signal_handler);

	pid = fork ();
	if (pid < 0) {
		g_error ("unable to fork().\nPlease free up some resources and try again.\n");
		rp3_exit (1);
	}
	if (pid == 0) {
		/* child */
		gchar *msg;
		close (msg_pipe[0]);
		close (format_pipe[0]);
//		ModemLog log (msg_pipe[1]);
		ModemLog log (-1);
		guint count;
		gfloat percent;

		WvModemScanList l;

		count = l.count();
		msg = g_strdup_printf ("BC%dBC", count);
		write (format_pipe[1], msg, strlen (msg));
		g_free (msg);

		WvModemScanList::Iter check(l);

		while (!l.isdone()) {
			l.execute();
			percent = 1.0;

			msg_write_func (l, msg_pipe[1]);
			for (check.rewind (); check.next ();) {
				if (count == 0)
					continue;
				if ((check ()).get_stage () == 0)
					percent -= 1/(float)count;
				else
					percent -= (1/(float)count - (((float)(check ()).get_stage ())/ WvModemScan::NUM_STAGES) * (1/(float) count));
			}
			CLAMP (percent, 0.0, 1.0);
			/* we guarentee 3 digits */
			msg = g_strdup_printf ("UP%.3dUP", (gint) (percent*100));
			write (format_pipe[1], msg, strlen (msg));
			g_free (msg);
		}
		msg = g_strdup_printf ("EC%dEC", l.count());
		write (format_pipe[1], msg, strlen (msg));
		g_free (msg);

		/* let's store to disk */
		if (l.count () > 0) {
			WvConf cfg("/etc/wvdial.conf");
			WvModemScanList::Iter i(l);
			gint j = 0;
			char temp_section[18];

			for (i.rewind(); i.next();) {
				/* We overwrite all other modems when we do this. */
				if (section)
					g_snprintf (temp_section, 17, "%s", section);
				else
					g_snprintf (temp_section, 17, "Modem%d", j);
				WvModemScan &m = i;
				WvString fn = m.filename (), init = m.initstr ();

				cfg.set(temp_section, "Modem", fn);
				cfg.setint(WvString(temp_section),
					   WvString("Baud"), m.maxbaud());
				cfg.set(temp_section, "Init1", "ATZ");
				cfg.set(temp_section, "Init2", init);
				j++;
			}
		}
		write (format_pipe[1], "DONE", strlen ("DONE"));
		close (msg_pipe[1]);
		close (format_pipe[1]);

		_exit (0);
	}

	/* parent */
	close (msg_pipe[1]);
	close (format_pipe[1]);
	gtk_widget_set_sensitive (parent, FALSE);
	gnome_dialog_set_parent (GNOME_DIALOG (dialog), GTK_WINDOW (parent));
	gnome_dialog_run_and_close (GNOME_DIALOG (dialog));
	if (pid != 0) {
		/* I don't know if this is good. */
		/* it could leave us in a broken state. */
		kill (pid, 9);
		pid = 0;
		gtk_input_remove (msg_input_handler);
		gtk_input_remove (format_input_handler);
		msg_input_handler = 0;
		format_input_handler = 0;
		end_count = 0;
	}
	signal (SIGCHLD, SIG_DFL);
	close (msg_pipe[0]);
	close (format_pipe[0]);

	gtk_widget_set_sensitive (parent, TRUE);
}

GSList *
load_modems ()
{
	GSList *retval = NULL;

	WvConf cfg ("/etc/wvdial.conf");
	WvConfigSectionList::Iter i (cfg);


	for (i.rewind (); i.next ();) {
		if (strncmp (i().name, "Modem", strlen ("Modem")) == 0) {
			Modem *modem = (Modem *) g_new (Modem, 1);
			modem->device = g_strdup (i().get ("Modem", ""));
			modem->speed = g_strdup (i().get ("Baud", "460800"));
			modem->section = g_strdup (i().name);
			retval = g_slist_prepend (retval, modem);
		}
	}
	return retval;
}

void
modem_destroy (Modem *modem)
{
	WvConf cfg ("/etc/wvdial.conf");
	gchar section[10];
	gint i, j;


	g_assert (modem->section != NULL);
	g_assert (strlen (modem->section) > strlen ("Modem"));

#if 0
	/* This is a little ugly, but necessary. */
	/* if we remove ModemN, we need to make sure that we fill in the gap. 
	 * To do this, we copy the last modem in to slot N. */

	/* Actually we don't do it. -jrb*/
	j = atoi (modem->section + strlen ("Modem"));
	i = j + 1;
	do {
		sprintf (section, "Modem%d", i);
		if (cfg[section] == NULL)
			break;
		i++;
	} while (i < 20);
	i = i - 1; 

	if (i == j) {
		cfg.delete_section (modem->section);
	} else {
		sprintf (section, "Modem%d", i);
		copy_sections (&cfg, section, modem->section);
		cfg.delete_section (section);
	}
#endif
	cfg.delete_section (modem->section);
	g_free (modem->device);
	g_free (modem->speed);
	g_free (modem->section);
	g_free (modem);
}

void
modem_save (Modem *modem)
{
	g_print ("UNIMPLEMENTED:  Save modem here\n");
}


