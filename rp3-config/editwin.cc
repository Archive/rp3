/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* editwin.cc
 *
 * Copyright (C) 1999 Red Hat, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <gnome.h>
#include "editwin.h"
#include <ctype.h>
#include <wvpapchap.h>
#include "shvar.h"
#include "account.h"
#include "interface.h"

static void
toggle_sensitivity(GtkWidget *cb, gpointer data)
{
    GtkWidget *target = GTK_WIDGET(data);
    gboolean active = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(cb));
    gtk_widget_set_sensitive(target, active);
}

static void
properties_dialog_help(GtkWidget *dialog, gint page_num, gpointer data) {
    static GnomeHelpMenuEntry help_ref = { "rp3", "s1-screens-edit-connection.html" };
    gnome_help_display(NULL, &help_ref);
}

static gint
read_dns (GtkWidget *entry1, GtkWidget *entry2, GtkWidget *entry3, GtkWidget *entry4, gchar **dns)
{
  gint retval;
  gchar *text1;
  gchar *text2;
  gchar *text3;
  gchar *text4;

  text1 = gtk_editable_get_chars (GTK_EDITABLE (entry1), 0, -1);
  text2 = gtk_editable_get_chars (GTK_EDITABLE (entry2), 0, -1);
  text3 = gtk_editable_get_chars (GTK_EDITABLE (entry3), 0, -1);
  text4 = gtk_editable_get_chars (GTK_EDITABLE (entry4), 0, -1);

  if (!strcmp (text1, "") &&
      !strcmp (text2, "") &&
      !strcmp (text3, "") &&
      !strcmp (text4, ""))
    {
      *dns = NULL;
      retval = 0;
    }
  else if (!strcmp (text1, "") ||
           !strcmp (text2, "") ||
           !strcmp (text3, "") ||
           !strcmp (text4, ""))
    {
      *dns = NULL;
      retval = -1;
    }
  else
    {
      *dns = g_strdup_printf ("%d.%d.%d.%d",
                              atoi (text1),
                              atoi (text2),
                              atoi (text3),
                              atoi (text4));
      retval = 0;
    }
  g_free (text1);
  g_free (text2);
  g_free (text3);
  g_free (text4);

  return retval;
}


static void
apply_cb(GtkWidget* pb, gint page, Account* a)
{
  GtkWidget* name_entry;
  GtkWidget* username_entry;
  GtkWidget* password_entry;
  GtkWidget* prefix_entry;
  GtkWidget* areacode_entry;
  GtkWidget* number_entry;

  gchar* name_str;
  gchar* username_str;
  gchar* password_str;
  gchar* prefix_str;
  gchar* areacode_str;
  gchar* number_str;
  WvConf *cfg = new WvConf ("/etc/wvdial.conf");

  /* Normal options  */
  if (page < 0)
    return;

  name_entry = (GtkWidget*) gtk_object_get_data(GTK_OBJECT(pb), "name_entry");
  username_entry = (GtkWidget*) gtk_object_get_data(GTK_OBJECT(pb), "username_entry");
  password_entry = (GtkWidget*) gtk_object_get_data(GTK_OBJECT(pb), "password_entry");
  prefix_entry = (GtkWidget*) gtk_object_get_data(GTK_OBJECT(pb), "prefix_entry");
  areacode_entry = (GtkWidget*) gtk_object_get_data(GTK_OBJECT(pb), "areacode_entry");
  number_entry = (GtkWidget*) gtk_object_get_data(GTK_OBJECT(pb), "number_entry");

  name_str = gtk_entry_get_text(GTK_ENTRY(name_entry));

  username_str = gtk_entry_get_text(GTK_ENTRY(username_entry));

  password_str = gtk_entry_get_text(GTK_ENTRY(password_entry));

  prefix_str = gtk_entry_get_text(GTK_ENTRY(prefix_entry));

  areacode_str = gtk_entry_get_text(GTK_ENTRY(areacode_entry));

  number_str = gtk_entry_get_text(GTK_ENTRY(number_entry));

  if (!valid_phone_number_string(prefix_str))
    {
      GtkWidget* d;
      gtk_widget_set_sensitive(pb, FALSE);
      d = gnome_warning_dialog_parented(_("Phone prefix is invalid"),
                                        GTK_WINDOW(pb));
      gnome_dialog_run_and_close(GNOME_DIALOG(d));
      gnome_property_box_changed(GNOME_PROPERTY_BOX(pb));
      gtk_widget_set_sensitive(pb, TRUE);
      return;
    }

  if (!valid_phone_number_string(areacode_str))
    {
      GtkWidget* d;
      gtk_widget_set_sensitive(pb, FALSE);
      d = gnome_warning_dialog_parented(_("Area code is invalid"),
                                        GTK_WINDOW(pb));
      gnome_dialog_run_and_close(GNOME_DIALOG(d));
      gnome_property_box_changed(GNOME_PROPERTY_BOX(pb));
      gtk_widget_set_sensitive(pb, TRUE);
      return;
    }

  gchar *old_name = g_strdup (a->name);
  account_set_name (a, name_str);
  gchar *dialer_str = g_strdup_printf ("Dialer %s", a->name);
  if (((*cfg)[dialer_str] != NULL) && (strcmp (a->name, old_name)))
    {
      GtkWidget* d;
      gtk_widget_set_sensitive(pb, FALSE);
      d = gnome_warning_dialog_parented(_("That account already exists"),
                                        GTK_WINDOW(pb));
      gnome_dialog_run_and_close(GNOME_DIALOG(d));
      gnome_property_box_changed(GNOME_PROPERTY_BOX(pb));
      gtk_widget_set_sensitive(pb, TRUE);
      g_free (dialer_str);
      account_set_name (a, old_name);
      g_free (old_name);
      return;
    }
  g_free (old_name);
  g_free (dialer_str);

  /* Advanced Options */
  GtkWidget *advanced_modem;
  GtkWidget *advanced_stupidmode;
  GtkWidget *advanced_onboot;
  GtkWidget *advanced_persist;
  GtkWidget *advanced_userctl;
  GtkWidget *advanced_route;
  GtkWidget *advanced_peerdns;
  GtkWidget *advanced_demand;
  GtkWidget *advanced_idletimeout;
  GtkWidget *entry1, *entry2, *entry3, *entry4;
  gchar *modem_section;
  gchar *dns1, *dns2;

  advanced_modem = (GtkWidget *) gtk_object_get_data (GTK_OBJECT (pb), "advanced_modem");
  advanced_stupidmode = (GtkWidget *) gtk_object_get_data (GTK_OBJECT (pb), "advanced_stupidmode");
  advanced_userctl = (GtkWidget *) gtk_object_get_data (GTK_OBJECT (pb), "advanced_userctl");
  advanced_onboot = (GtkWidget *) gtk_object_get_data (GTK_OBJECT (pb), "advanced_onboot");
  advanced_persist = (GtkWidget *) gtk_object_get_data (GTK_OBJECT (pb), "advanced_persist");
  advanced_route = (GtkWidget *) gtk_object_get_data (GTK_OBJECT (pb), "advanced_route");
  advanced_peerdns = (GtkWidget *) gtk_object_get_data (GTK_OBJECT (pb), "advanced_peerdns");
  advanced_demand = (GtkWidget *) gtk_object_get_data (GTK_OBJECT (pb), "advanced_demand");
  advanced_idletimeout = (GtkWidget *) gtk_object_get_data (GTK_OBJECT (pb), "advanced_idletimeout");

  entry1 = (GtkWidget *) gtk_object_get_data (GTK_OBJECT (pb), "advanced_dns1_a");
  entry2 = (GtkWidget *) gtk_object_get_data (GTK_OBJECT (pb), "advanced_dns1_b");
  entry3 = (GtkWidget *) gtk_object_get_data (GTK_OBJECT (pb), "advanced_dns1_c");
  entry4 = (GtkWidget *) gtk_object_get_data (GTK_OBJECT (pb), "advanced_dns1_d");

  if (read_dns (entry1, entry2, entry3, entry4, &dns1) == -1)
    {
      GtkWidget* d;
      gtk_widget_set_sensitive (pb, FALSE);
      d = gnome_warning_dialog_parented (_("The IP value for the primary DNS is invalid"),
                                        GTK_WINDOW (pb));
      gnome_dialog_run_and_close (GNOME_DIALOG (d));
      gnome_property_box_changed (GNOME_PROPERTY_BOX (pb));
      gtk_widget_set_sensitive (pb, TRUE);
      return;
    }

  entry1 = (GtkWidget *) gtk_object_get_data (GTK_OBJECT (pb), "advanced_dns2_a");
  entry2 = (GtkWidget *) gtk_object_get_data (GTK_OBJECT (pb), "advanced_dns2_b");
  entry3 = (GtkWidget *) gtk_object_get_data (GTK_OBJECT (pb), "advanced_dns2_c");
  entry4 = (GtkWidget *) gtk_object_get_data (GTK_OBJECT (pb), "advanced_dns2_d");

  if (read_dns (entry1, entry2, entry3, entry4, &dns2) == -1)
    {
      GtkWidget* d;
      gtk_widget_set_sensitive(pb, FALSE);
      d = gnome_warning_dialog_parented(_("The IP value for the secondary DNS is invalid"),
                                        GTK_WINDOW(pb));
      gnome_dialog_run_and_close(GNOME_DIALOG(d));
      gnome_property_box_changed(GNOME_PROPERTY_BOX(pb));
      gtk_widget_set_sensitive(pb, TRUE);
      return;
    }

  /* Now that we are inheriting a modem section, clean out the ones listed
   * explicitly here. */
  modem_section = (gchar *) gtk_object_get_data (GTK_OBJECT (advanced_modem), "label");
  cfg->set (a->wvdial_sect, "Inherits", modem_section);
  cfg->set (a->wvdial_sect, "Modem", NULL);
  cfg->set (a->wvdial_sect, "Baud", NULL);

  a->stupidmode = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (advanced_stupidmode));
  a->userctl = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (advanced_userctl));
  a->onboot = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (advanced_onboot));
  a->persist = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (advanced_persist));

  if (dns1)
    svSetValue (a->interface->ifcfg, "DNS1", dns1);

  if (dns2)
    svSetValue (a->interface->ifcfg, "DNS2", dns2);

  svSetValue (a->interface->ifcfg, "DEFROUTE", gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (advanced_route)) ? "yes" : "no");

  svSetValue (a->interface->ifcfg, "PEERDNS", gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (advanced_peerdns)) ? "yes" : "no");

  svSetValue (a->interface->ifcfg, "DEMAND", gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (advanced_demand)) ? "yes" : "no");

  svSetValue (a->interface->ifcfg, "IDLETIMEOUT", gtk_entry_get_text(GTK_ENTRY(advanced_idletimeout)));

  /* If we get this far, we want to set everything. */
  account_set_name(a, name_str);
  account_set_username(a, username_str);
  account_set_password(a, password_str);
  account_set_stupidmode(a, gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (advanced_stupidmode)));
  account_set_modem_sect(a, modem_section);
  phone_number_set_prefix(a->number, prefix_str);
  phone_number_set_areacode(a->number, areacode_str);
  phone_number_set_number(a->number, number_str);

  delete cfg;

  WvPapChap papchap;
  papchap.put_secret(username_str, password_str, svGetValue (a->interface->ifcfg, "DEVICE" ));

  rp3_mainwin_account_changed(GTK_WIDGET(gtk_object_get_data(GTK_OBJECT(pb), "mainwin")), a);
}

static void
changed_cb(GtkWidget* entry, GtkWidget* pb)
{
  gnome_property_box_changed(GNOME_PROPERTY_BOX(pb));
}

static void
menu_activate (GtkMenuItem *menuitem,
               gpointer user_data)
{
  gnome_property_box_changed(GNOME_PROPERTY_BOX(user_data));
  gtk_object_set_data (GTK_OBJECT (user_data), "advanced_modem", menuitem);
}
static void
insert_text_callback (GtkEditable    *editable, const gchar    *text,
                      gint length, gint *position,
                      void *data)
{
        gint i;

        if (length == 1 && (text[0] == '.' || text[0] == '\n'))
          {
            GtkWidget *next_widget = (GtkWidget *)gtk_object_get_data (GTK_OBJECT (editable), "next_entry");
            if (next_widget)
              {
                gtk_widget_grab_focus (next_widget);
              }
            gtk_signal_emit_stop_by_name (GTK_OBJECT (editable), "insert_text");
            return;
          }          
        for (i = 0; i < length; i++)
          {
            if (!isdigit(text[i]))
              {
                gtk_signal_emit_stop_by_name (GTK_OBJECT (editable), "insert_text");
                return;
              }
          }

        if ((atoi (gtk_editable_get_chars (editable,0,-1)) * 10 *length + atoi (text)) > 255)
          gtk_signal_emit_stop_by_name (GTK_OBJECT (editable), "insert_text");

        gnome_property_box_changed(GNOME_PROPERTY_BOX(data));
}
static void
delete_text_callback (GtkEditable    *editable,
                            gint            length,
                            gint           *position,
                            void *data)
{
        gnome_property_box_changed (GNOME_PROPERTY_BOX (data));
}

static gint
parse_dns (gchar *dns, gint dns_vals[4])
{
  gchar *ptr, *ptr2;

  ptr = rindex (dns, '.');
  if (ptr == NULL)
    return -1;
  dns_vals[3] = strtol (ptr+1, &ptr2, 10);
  if (*ptr2 != '\0')
    return -1;
  ptr[0] = '\0';

  ptr = rindex (dns, '.');
  if (ptr == NULL)
    return -1;
  dns_vals[2] = strtol (ptr+1, &ptr2, 10);
  if (*ptr2 != '\0')
    return -1;
  ptr[0] = '\0';


  ptr = rindex (dns, '.');
  if (ptr == NULL)
    return -1;
  dns_vals[1] = strtol (ptr+1, &ptr2, 10);
  if (*ptr2 != '\0')
    return -1;
  ptr[0] = '\0';


  dns_vals[0] = strtol (dns, &ptr2, 10);
  if (*ptr2 != '\0')
    return -1;

  return 0;

}
static GtkWidget*
make_advanced_page(GtkWidget* pb, Account* a)
{
  GtkWidget* table;
  GtkWidget* label;
  GtkWidget* entry;
  GtkWidget* old_entry;
  GtkWidget* button;
  GtkWidget* spacer;
  GtkWidget* hbox;
  GtkWidget* cbox;
  GtkWidget* align;
  GtkWidget *menu_item;
  GtkWidget *omenu;
  GtkWidget *menu;
  const gchar *modem;
  gchar dns_string [4];
  gint dns_vals [4];
  gchar *dns = NULL;
  gchar *route = NULL;
  gchar *peerdns = NULL;
  gchar *demand = NULL;
  gchar *timeout = NULL;
  gint idle_timeout = 600;
  gint i = 1;
  gint j = 0;
  const gchar *inherits_from = NULL;
  WvConf cfg ("/etc/wvdial.conf");

  table = gtk_table_new(8, 2, FALSE);
  gtk_container_set_border_width (GTK_CONTAINER (table), GNOME_PAD_SMALL);

  label = gtk_label_new(_("Modem device name:"));
  gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);

  gtk_table_attach(GTK_TABLE(table),
                   label,
                   0, 1,
                   0, 1,
                   (GtkAttachOptions) (GTK_SHRINK | GTK_EXPAND | GTK_FILL),
                   (GtkAttachOptions) (GTK_SHRINK),
                   0, 0);

  WvConfigSectionList::Iter iter (cfg);
  omenu = gtk_option_menu_new ();
  menu = gtk_menu_new ();

  /* First item */
  menu_item = gtk_menu_item_new_with_label ("Default Modem");
  gtk_object_set_data_full (GTK_OBJECT (menu_item), "label", g_strdup ("Dialer Defaults"), g_free);
  gtk_widget_show (menu_item);
  gtk_object_set_data (GTK_OBJECT (pb), "advanced_modem", menu_item);
  gtk_signal_connect (GTK_OBJECT (menu_item), "activate",
                      GTK_SIGNAL_FUNC (menu_activate),
                      pb);
  gtk_menu_append (GTK_MENU (menu), menu_item);



  /* rest of the items */
  inherits_from = cfg.get (a->wvdial_sect, "Inherits", "");

  for (iter.rewind (); iter.next ();)
    {
      if (strncmp (iter().name, "Modem", strlen ("Modem")) == 0)
        {
          modem = iter().get ("Modem", "");
          menu_item = gtk_menu_item_new_with_label (modem);
          gtk_object_set_data_full (GTK_OBJECT (menu_item), "label", g_strdup (iter().name), g_free);
          gtk_widget_show (menu_item);
          if (strcmp (iter().name, inherits_from) == 0) {
            gtk_object_set_data (GTK_OBJECT (pb), "advanced_modem", menu_item);
            j = i;
          }
          gtk_signal_connect (GTK_OBJECT (menu_item), "activate",
                              GTK_SIGNAL_FUNC (menu_activate),
                              pb);
          gtk_menu_append (GTK_MENU (menu), menu_item);
          i++;
        }
    }

  gtk_option_menu_set_menu (GTK_OPTION_MENU (omenu), menu);
  gtk_option_menu_set_history (GTK_OPTION_MENU (omenu), j);
  gtk_widget_show (menu);

  gtk_table_set_row_spacings (GTK_TABLE (table), GNOME_PAD_SMALL);

  gtk_table_attach(GTK_TABLE(table),
                   omenu,
                   1, 2,
                   0, 1,
                   (GtkAttachOptions) (GTK_SHRINK | GTK_EXPAND | GTK_FILL),
                   (GtkAttachOptions) (GTK_SHRINK),
                   0, 0);

  cbox = gtk_check_button_new_with_label (_("Let PPP do all authentication"));
  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (cbox), a->stupidmode);

  gtk_object_set_data (GTK_OBJECT (pb), "advanced_stupidmode", cbox);
  gtk_signal_connect (GTK_OBJECT (cbox), "toggled", GTK_SIGNAL_FUNC (changed_cb), pb);
  gtk_table_attach (GTK_TABLE (table),
                    cbox,
                    0, 2,
                    1, 2,
                    (GtkAttachOptions) (GTK_SHRINK | GTK_EXPAND | GTK_FILL),
                    (GtkAttachOptions) (GTK_SHRINK),
                    0, 0);

  cbox = gtk_check_button_new_with_label (_("Begin connection when computer is turned on"));
  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (cbox),  a->onboot);


  gtk_object_set_data (GTK_OBJECT (pb), "advanced_onboot", cbox);
  gtk_signal_connect (GTK_OBJECT (cbox), "toggled", GTK_SIGNAL_FUNC (changed_cb), pb);
  gtk_table_attach (GTK_TABLE (table),
                    cbox,
                    0, 2,
                    2, 3,
                    (GtkAttachOptions) (GTK_SHRINK | GTK_EXPAND | GTK_FILL),
                    (GtkAttachOptions) (GTK_SHRINK),
                    0, 0);

  cbox = gtk_check_button_new_with_label (_("Let user start and stop connection"));
  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (cbox), a->userctl);

  gtk_object_set_data (GTK_OBJECT (pb), "advanced_userctl", cbox);
  gtk_signal_connect (GTK_OBJECT (cbox), "toggled", GTK_SIGNAL_FUNC (changed_cb), pb);
  gtk_table_attach (GTK_TABLE (table),
                    cbox,
                    0, 2,
                    3, 4,
                    (GtkAttachOptions) (GTK_SHRINK | GTK_EXPAND | GTK_FILL),
                    (GtkAttachOptions) (GTK_SHRINK),
                    0, 0);

  cbox = gtk_check_button_new_with_label (_("Make this connection the default route"));
  route = svGetValue(a->interface->ifcfg, "DEFROUTE");
  if(route != NULL) {
    gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (cbox), strcmp(route, "no") != 0);
    g_free(route);
  } else {
    gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (cbox), TRUE);
  }
  gtk_object_set_data (GTK_OBJECT (pb), "advanced_route", cbox);
  gtk_signal_connect (GTK_OBJECT (cbox), "toggled", GTK_SIGNAL_FUNC (changed_cb), pb);
  gtk_table_attach (GTK_TABLE (table),
                    cbox,
                    0, 2,
                    4, 5,
                    (GtkAttachOptions) (GTK_SHRINK | GTK_EXPAND | GTK_FILL),
                    (GtkAttachOptions) (GTK_SHRINK),
                    0, 0);

  cbox = gtk_check_button_new_with_label (_("Configure name resolution automatically"));
  peerdns = svGetValue(a->interface->ifcfg, "PEERDNS");
  if(peerdns != NULL) {
    gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (cbox), strcmp(peerdns, "no") != 0);
    g_free(peerdns);
  } else {
    gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (cbox), TRUE);
  }
  gtk_object_set_data (GTK_OBJECT (pb), "advanced_peerdns", cbox);
  gtk_signal_connect (GTK_OBJECT (cbox), "toggled", GTK_SIGNAL_FUNC (changed_cb), pb);
  gtk_table_attach (GTK_TABLE (table),
                    cbox,
                    0, 2,
                    5, 6,
                    (GtkAttachOptions) (GTK_SHRINK | GTK_EXPAND | GTK_FILL),
                    (GtkAttachOptions) (GTK_SHRINK),
                    0, 0);

  cbox = gtk_check_button_new_with_label (_("Restart if connection dies"));
  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (cbox),  a->persist);
  gtk_object_set_data (GTK_OBJECT (pb), "advanced_persist", cbox);
  gtk_signal_connect (GTK_OBJECT (cbox), "toggled", GTK_SIGNAL_FUNC (changed_cb), pb);
  gtk_table_attach (GTK_TABLE (table),
                    cbox,
                    0, 2,
                    6, 7,
                    (GtkAttachOptions) (GTK_SHRINK | GTK_EXPAND | GTK_FILL),
                    (GtkAttachOptions) (GTK_SHRINK),
                    0, 0);

  cbox = gtk_check_button_new_with_label (_("Bring link up and down automatically"));
  demand = svGetValue(a->interface->ifcfg, "DEMAND");
  if(demand != NULL) {
    gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (cbox), strcmp(demand, "yes") == 0);
    g_free(demand);
  } else {
    gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (cbox), FALSE);
  }
  gtk_object_set_data (GTK_OBJECT (pb), "advanced_demand", cbox);
  gtk_signal_connect (GTK_OBJECT (cbox), "toggled", GTK_SIGNAL_FUNC (changed_cb), pb);
  gtk_table_attach (GTK_TABLE (table),
                    cbox,
                    0, 2,
                    7, 8,
                    (GtkAttachOptions) (GTK_SHRINK | GTK_EXPAND | GTK_FILL),
                    (GtkAttachOptions) (GTK_SHRINK),
                    0, 0);

  label = gtk_label_new (_("Maximum idle time"));
  gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
  gtk_table_attach (GTK_TABLE (table),
                    label,
                    0, 1,
                    8, 9,
                    (GtkAttachOptions) (GTK_SHRINK | GTK_EXPAND | GTK_FILL),
                    (GtkAttachOptions) (GTK_SHRINK),
                    0, 0);
 
  hbox = gtk_hbox_new(FALSE, 2); 
  entry = gtk_entry_new();
  gtk_widget_set_usize (entry, 35, -1);
  timeout = svGetValue(a->interface->ifcfg, "IDLETIMEOUT");
  toggle_sensitivity(cbox, entry);
  gtk_signal_connect(GTK_OBJECT(entry), "changed",
                     GTK_SIGNAL_FUNC(changed_cb),
                     pb);
  gtk_signal_connect(GTK_OBJECT(cbox), "toggled",
		     GTK_SIGNAL_FUNC(toggle_sensitivity),
		     entry);
  if(timeout != NULL) {
    gtk_entry_set_text(GTK_ENTRY(entry), timeout);
    g_free(timeout);
  } else {
    gtk_entry_set_text(GTK_ENTRY(entry), "600");
  }
  gtk_box_pack_start(GTK_BOX(hbox), entry, FALSE, FALSE, 0);
  gtk_table_attach (GTK_TABLE (table), hbox,
                    1, 2,
                    8, 9,
                    (GtkAttachOptions) (GTK_SHRINK | GTK_EXPAND | GTK_FILL),
                    (GtkAttachOptions) (GTK_SHRINK),
                    0, 0);
  gtk_object_set_data (GTK_OBJECT (pb), "advanced_idletimeout", entry);

  label = gtk_label_new (_("First DNS Server (Optional)"));
  gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
  gtk_table_attach (GTK_TABLE (table),
                    label,
                    0, 1,
                    10, 11,
                    (GtkAttachOptions) (GTK_SHRINK | GTK_EXPAND | GTK_FILL),
                    (GtkAttachOptions) (GTK_SHRINK),
                    0, 0);

  hbox = gtk_hbox_new (FALSE, 2);

  dns = svGetValue (a->interface->ifcfg, "DNS1");
  if (dns)
    {
      if (parse_dns (dns, dns_vals) != 0)
        {
          free (dns);
          dns = NULL;
        }
    }

  old_entry = entry = gtk_entry_new ();
  if (dns)
    {
      g_snprintf (dns_string, 4, "%d", dns_vals[0]);
      gtk_entry_set_text (GTK_ENTRY (entry), dns_string);
    }
  gtk_signal_connect (GTK_OBJECT (entry), "insert_text", GTK_SIGNAL_FUNC (insert_text_callback), pb);
  gtk_signal_connect (GTK_OBJECT (entry), "delete_text", GTK_SIGNAL_FUNC (delete_text_callback), pb);
  gtk_object_set_data (GTK_OBJECT (pb), "advanced_dns1_a", entry);
  gtk_widget_set_usize (entry, 35, -1);
  gtk_box_pack_start (GTK_BOX (hbox), entry, FALSE, FALSE, 0);
  gtk_box_pack_start (GTK_BOX (hbox), gtk_label_new ("."), FALSE, FALSE, 0);

  entry = gtk_entry_new ();
  gtk_object_set_data (GTK_OBJECT (old_entry), "next_entry", entry);
  old_entry = entry;
  if (dns)
    {
      g_snprintf (dns_string, 4, "%d", dns_vals[1]);
      gtk_entry_set_text (GTK_ENTRY (entry), dns_string);
    }
  gtk_signal_connect (GTK_OBJECT (entry), "insert_text", GTK_SIGNAL_FUNC (insert_text_callback), pb);
  gtk_signal_connect (GTK_OBJECT (entry), "delete_text", GTK_SIGNAL_FUNC (delete_text_callback), pb);
  gtk_object_set_data (GTK_OBJECT (pb), "advanced_dns1_b", entry);
  gtk_widget_set_usize (entry, 35, -1);
  gtk_box_pack_start (GTK_BOX (hbox), entry, FALSE, FALSE, 0);
  gtk_box_pack_start (GTK_BOX (hbox), gtk_label_new ("."), FALSE, FALSE, 0);

  entry = gtk_entry_new ();
  gtk_object_set_data (GTK_OBJECT (old_entry), "next_entry", entry);
  old_entry = entry;
  if (dns)
    {
      g_snprintf (dns_string, 4, "%d", dns_vals[2]);
      gtk_entry_set_text (GTK_ENTRY (entry), dns_string);
    }
  gtk_signal_connect (GTK_OBJECT (entry), "insert_text", GTK_SIGNAL_FUNC (insert_text_callback), pb);
  gtk_signal_connect (GTK_OBJECT (entry), "delete_text", GTK_SIGNAL_FUNC (delete_text_callback), pb);
  gtk_object_set_data (GTK_OBJECT (pb), "advanced_dns1_c", entry);
  gtk_widget_set_usize (entry, 35, -1);
  gtk_box_pack_start (GTK_BOX (hbox), entry, FALSE, FALSE, 0);
  gtk_box_pack_start (GTK_BOX (hbox), gtk_label_new ("."), FALSE, FALSE, 0);

  entry = gtk_entry_new ();
  gtk_object_set_data (GTK_OBJECT (old_entry), "next_entry", entry);
  old_entry = entry;
  if (dns)
    {
      g_snprintf (dns_string, 4, "%d", dns_vals[3]);
      gtk_entry_set_text (GTK_ENTRY (entry), dns_string);
    }
  gtk_signal_connect (GTK_OBJECT (entry), "insert_text", GTK_SIGNAL_FUNC (insert_text_callback), pb);
  gtk_signal_connect (GTK_OBJECT (entry), "delete_text", GTK_SIGNAL_FUNC (delete_text_callback), pb);
  gtk_object_set_data (GTK_OBJECT (pb), "advanced_dns1_d", entry);
  gtk_widget_set_usize (entry, 35, -1);
  gtk_box_pack_start (GTK_BOX (hbox), entry, FALSE, FALSE, 0);

  if (dns)
    free (dns);

  gtk_table_attach (GTK_TABLE (table),
                    hbox,
                    1, 2,
                    10, 11,
                    (GtkAttachOptions) (GTK_SHRINK | GTK_EXPAND | GTK_FILL),
                    (GtkAttachOptions) (GTK_SHRINK),
                    0, 0);


  label = gtk_label_new (_("Second DNS Server (Optional)"));
  gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
  gtk_table_attach (GTK_TABLE (table),
                    label,
                    0, 1,
                    11, 12,
                    (GtkAttachOptions) (GTK_SHRINK | GTK_EXPAND | GTK_FILL),
                    (GtkAttachOptions) (GTK_SHRINK),
                    0, 0);

  hbox = gtk_hbox_new (FALSE, 2);

  dns = svGetValue (a->interface->ifcfg, "DNS2");
  if (dns)
    {
      if (parse_dns (dns, dns_vals) != 0)
        {
          free (dns);
          dns = NULL;
        }
    }

  entry = gtk_entry_new ();
  gtk_object_set_data (GTK_OBJECT (old_entry), "next_entry", entry);
  old_entry = entry;
  if (dns)
    {
      g_snprintf (dns_string, 4, "%d", dns_vals[0]);
      gtk_entry_set_text (GTK_ENTRY (entry), dns_string);
    }
  gtk_signal_connect (GTK_OBJECT (entry), "insert_text", GTK_SIGNAL_FUNC (insert_text_callback), pb);
  gtk_signal_connect (GTK_OBJECT (entry), "delete_text", GTK_SIGNAL_FUNC (delete_text_callback), pb);
  gtk_object_set_data (GTK_OBJECT (pb), "advanced_dns2_a", entry);
  gtk_widget_set_usize (entry, 35, -1);
  gtk_box_pack_start (GTK_BOX (hbox), entry, FALSE, FALSE, 0);
  gtk_box_pack_start (GTK_BOX (hbox), gtk_label_new ("."), FALSE, FALSE, 0);

  entry = gtk_entry_new ();
  gtk_object_set_data (GTK_OBJECT (old_entry), "next_entry", entry);
  old_entry = entry;
  if (dns)
    {
      g_snprintf (dns_string, 4, "%d", dns_vals[1]);
      gtk_entry_set_text (GTK_ENTRY (entry), dns_string);
    }
  gtk_signal_connect (GTK_OBJECT (entry), "insert_text", GTK_SIGNAL_FUNC (insert_text_callback), pb);
  gtk_signal_connect (GTK_OBJECT (entry), "delete_text", GTK_SIGNAL_FUNC (delete_text_callback), pb);
  gtk_object_set_data (GTK_OBJECT (pb), "advanced_dns2_b", entry);
  gtk_widget_set_usize (entry, 35, -1);
  gtk_box_pack_start (GTK_BOX (hbox), entry, FALSE, FALSE, 0);
  gtk_box_pack_start (GTK_BOX (hbox), gtk_label_new ("."), FALSE, FALSE, 0);

  entry = gtk_entry_new ();
  gtk_object_set_data (GTK_OBJECT (old_entry), "next_entry", entry);
  old_entry = entry;
  if (dns)
    {
      g_snprintf (dns_string, 4, "%d", dns_vals[2]);
      gtk_entry_set_text (GTK_ENTRY (entry), dns_string);
    }
  gtk_signal_connect (GTK_OBJECT (entry), "insert_text", GTK_SIGNAL_FUNC (insert_text_callback), pb);
  gtk_signal_connect (GTK_OBJECT (entry), "delete_text", GTK_SIGNAL_FUNC (delete_text_callback), pb);
  gtk_object_set_data (GTK_OBJECT (pb), "advanced_dns2_c", entry);
  gtk_widget_set_usize (entry, 35, -1);
  gtk_box_pack_start (GTK_BOX (hbox), entry, FALSE, FALSE, 0);
  gtk_box_pack_start (GTK_BOX (hbox), gtk_label_new ("."), FALSE, FALSE, 0);

  entry = gtk_entry_new ();
  gtk_object_set_data (GTK_OBJECT (old_entry), "next_entry", entry);
  old_entry = entry;
  if (dns)
    {
      g_snprintf (dns_string, 4, "%d", dns_vals[3]);
      gtk_entry_set_text (GTK_ENTRY (entry), dns_string);
    }
  gtk_signal_connect (GTK_OBJECT (entry), "insert_text", GTK_SIGNAL_FUNC (insert_text_callback), pb);
  gtk_signal_connect (GTK_OBJECT (entry), "delete_text", GTK_SIGNAL_FUNC (delete_text_callback), pb);
  gtk_object_set_data (GTK_OBJECT (pb), "advanced_dns2_d", entry);
  gtk_widget_set_usize (entry, 35, -1);
  gtk_box_pack_start (GTK_BOX (hbox), entry, FALSE, FALSE, 0);

  gtk_table_attach (GTK_TABLE (table),
                    hbox,
                    1, 2,
                    11, 12,
                    (GtkAttachOptions) (GTK_SHRINK | GTK_EXPAND | GTK_FILL),
                    (GtkAttachOptions) (GTK_SHRINK),
                    0, 0);

  gtk_widget_show_all(table);

  return table;
}

static GtkWidget*
make_account_page(GtkWidget* pb, Account* a)
{
  GtkWidget* table;
  GtkWidget* label;
  GtkWidget* entry;

  table = gtk_table_new(4, 2, FALSE);

  label = gtk_label_new(_("Account Name:"));
  gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);

  entry = gtk_entry_new();

  gtk_object_set_data(GTK_OBJECT(pb), "name_entry", entry);

  gtk_entry_set_text(GTK_ENTRY(entry), a->name);

  gtk_signal_connect(GTK_OBJECT(entry), "changed",
                     GTK_SIGNAL_FUNC(changed_cb),
                     pb);

  gtk_table_attach(GTK_TABLE(table),
                   label,
                   0, 1,
                   0, 1,
                   (GtkAttachOptions) (GTK_SHRINK | GTK_EXPAND | GTK_FILL),
                   (GtkAttachOptions) (GTK_SHRINK | GTK_EXPAND | GTK_FILL),
                   GNOME_PAD_SMALL, GNOME_PAD_SMALL);

  gtk_table_attach(GTK_TABLE(table),
                   entry,
                   1, 2,
                   0, 1,
                   (GtkAttachOptions) (GTK_SHRINK | GTK_EXPAND | GTK_FILL),
                   (GtkAttachOptions) (GTK_SHRINK | GTK_EXPAND | GTK_FILL),
                   GNOME_PAD_SMALL, GNOME_PAD_SMALL);


  label = gtk_label_new(_("Username:"));
  gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);

  entry = gtk_entry_new();

  gtk_object_set_data(GTK_OBJECT(pb), "username_entry", entry);

  gtk_entry_set_text(GTK_ENTRY(entry), a->username);

  gtk_signal_connect(GTK_OBJECT(entry), "changed",
                     GTK_SIGNAL_FUNC(changed_cb),
                     pb);

  gtk_table_attach(GTK_TABLE(table),
                   label,
                   0, 1,
                   1, 2,
                   (GtkAttachOptions) (GTK_SHRINK | GTK_EXPAND | GTK_FILL),
                   (GtkAttachOptions) (GTK_SHRINK | GTK_EXPAND | GTK_FILL),
                   GNOME_PAD_SMALL, GNOME_PAD_SMALL);

  gtk_table_attach(GTK_TABLE(table),
                   entry,
                   1, 2,
                   1, 2,
                   (GtkAttachOptions) (GTK_SHRINK | GTK_EXPAND | GTK_FILL),
                   (GtkAttachOptions) (GTK_SHRINK | GTK_EXPAND | GTK_FILL),
                   GNOME_PAD_SMALL, GNOME_PAD_SMALL);

  label = gtk_label_new(_("Password:"));
  gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);

  entry = gtk_entry_new();

  gtk_object_set_data(GTK_OBJECT(pb), "password_entry", entry);

  gtk_entry_set_visibility(GTK_ENTRY(entry), FALSE);

  gtk_entry_set_text(GTK_ENTRY(entry), a->password);

  gtk_signal_connect(GTK_OBJECT(entry), "changed",
                     GTK_SIGNAL_FUNC(changed_cb),
                     pb);

  gtk_table_attach(GTK_TABLE(table),
                   label,
                   0, 1,
                   2, 3,
                   (GtkAttachOptions) (GTK_SHRINK | GTK_EXPAND | GTK_FILL),
                   (GtkAttachOptions) (GTK_SHRINK | GTK_EXPAND | GTK_FILL),
                   GNOME_PAD_SMALL, GNOME_PAD_SMALL);

  gtk_table_attach(GTK_TABLE(table),
                   entry,
                   1, 2,
                   2, 3,
                   (GtkAttachOptions) (GTK_SHRINK | GTK_EXPAND | GTK_FILL),
                   (GtkAttachOptions) (GTK_SHRINK | GTK_EXPAND | GTK_FILL),
                   GNOME_PAD_SMALL, GNOME_PAD_SMALL);

  {
    GtkWidget* hbox;

	hbox = gtk_hbox_new (FALSE, GNOME_PAD_SMALL);

	label = gtk_label_new (_("Prefix:"));
	gtk_box_pack_start (GTK_BOX (hbox), label, TRUE, FALSE, 0);
	entry = gtk_entry_new ();

    gtk_object_set_data(GTK_OBJECT(pb), "prefix_entry", entry);

    gtk_entry_set_text(GTK_ENTRY(entry), a->number->dial_prefix);

    gtk_signal_connect(GTK_OBJECT(entry), "changed",
                       GTK_SIGNAL_FUNC(changed_cb),
                       pb);


	gtk_widget_set_usize (entry, 35, -1);
	gnome_widget_add_help (entry, _("Enter any numbers you need to dial before the others.  For example, if you need to reach an outside line, or disable call waiting, put the necessary numbers here."));
	gtk_box_pack_start (GTK_BOX (hbox), entry, FALSE, FALSE, 0);
	label = gtk_label_new (_(" Area code:"));
	gtk_box_pack_start (GTK_BOX (hbox), label, TRUE, FALSE, 0);
	entry = gtk_entry_new ();

    gtk_object_set_data(GTK_OBJECT(pb), "areacode_entry", entry);

    gtk_entry_set_text(GTK_ENTRY(entry), a->number->area_code);

    gtk_signal_connect(GTK_OBJECT(entry), "changed",
                       GTK_SIGNAL_FUNC(changed_cb),
                       pb);


	gtk_widget_set_usize (entry, 50, -1);
	gnome_widget_add_help (entry, _("Enter the area/city/country code needed to reach your internet provider"));
	gtk_box_pack_start (GTK_BOX (hbox), entry, FALSE, FALSE, 0);

	label = gtk_label_new (_(" Phone number:"));
	gtk_box_pack_start (GTK_BOX (hbox), label, TRUE, FALSE, 0);
	entry = gtk_entry_new ();

    gtk_object_set_data(GTK_OBJECT(pb), "number_entry", entry);

    gtk_entry_set_text(GTK_ENTRY(entry), a->number->number);

    gtk_signal_connect(GTK_OBJECT(entry), "changed",
                       GTK_SIGNAL_FUNC(changed_cb),
                       pb);

	gnome_widget_add_help (entry, _("Enter the local phone number for your internet provider here"));
	gtk_box_pack_start (GTK_BOX (hbox), entry, TRUE, TRUE, 0);

    gtk_table_attach(GTK_TABLE(table),
                     hbox,
                     0, 2,
                     3, 4,
                     (GtkAttachOptions) (GTK_SHRINK | GTK_EXPAND | GTK_FILL),
                     (GtkAttachOptions) (GTK_SHRINK | GTK_EXPAND | GTK_FILL),
                     GNOME_PAD_SMALL, GNOME_PAD*2);
  }

  gtk_widget_show_all(table);

  return table;
}


GtkWidget*
rp3_editwin(GtkWidget* mainwin, Account* a)
{
  GtkWidget* win;

  win = gnome_property_box_new();

  gtk_object_set_data(GTK_OBJECT(win), "account", a);
  gtk_object_set_data(GTK_OBJECT(win), "mainwin", mainwin);

  gtk_window_set_modal(GTK_WINDOW(win), TRUE);

  gtk_window_set_title(GTK_WINDOW(win), _("Edit Internet Connection"));

  gnome_property_box_append_page(GNOME_PROPERTY_BOX(win),
                                 make_account_page(win, a),
                                 gtk_label_new(_("Account Info")));

  gnome_property_box_append_page(GNOME_PROPERTY_BOX(win),
                                 make_advanced_page(win, a),
                                 gtk_label_new(_("Advanced")));

  gtk_signal_connect(GTK_OBJECT(win), "apply",
                     GTK_SIGNAL_FUNC(apply_cb), a);
  gtk_signal_connect_object (GTK_OBJECT (win), "help",
                             GTK_SIGNAL_FUNC (properties_dialog_help), NULL);

  return win;
}
