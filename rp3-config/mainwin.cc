/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* mainwin.cc
 *
 * Copyright (C) 1999 Red Hat, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <gnome.h>
#include "mainwin.h"
#include "adddruid.h"
#include "editwin.h"
#include "modem.h"
#include "modem-edit.h"
#include "logger.h"
typedef struct _MWin MWin;

struct _MWin {
  GtkWidget* win;
  GtkWidget* clist;
  GtkWidget* add_button;
  GtkWidget* remove_button;
  GtkWidget* edit_button;
  GtkWidget* clone_button;
  GtkWidget* dial_button;
  gint selected_row;
  Account* selected_account;

  GtkWidget* modem_clist;
  GtkWidget* modem_add_button;
  GtkWidget* modem_edit_button;
  GtkWidget* modem_delete_button;
  gint modem_selected_row;
  Modem *selected_modem;

  GSList* accounts;
  GSList* modems;
  GtkWidget* edit_window;
  GtkWidget* add_window;
};

static void rp3_mainwin_add_account_without_save(GtkWidget* w, Account* a);
static void edit_cb(GtkWidget* button, gpointer data);
static void main_help_cb(GtkWidget* button, gpointer data);
static void rp3_mainwin_remove_modem (GtkWidget* w, Modem* modem);
static void modem_edit_cb(GtkWidget* button, gpointer data);
static void update_default_modem (MWin *mw);

MWin*
mw_new(void)
{
  MWin* mw;

  mw = g_new0(MWin, 1);

  mw->selected_row = -1;
  return mw;
}

void
mw_destroy(MWin* mw)
{
  if (mw->edit_window)
    gtk_widget_destroy(mw->edit_window);

  if (mw->add_window)
    gtk_widget_destroy(mw->add_window);

  g_assert(mw->edit_window == NULL); /* we have a callback to do this */

  g_free(mw);
}

void
rp3_set_tooltip(GtkWidget* w, const gchar* tip)
{
  GtkTooltips* t = gtk_tooltips_new();

  gtk_tooltips_set_tip (t, w, tip, NULL);
}

static GtkWidget *
left_aligned_button (gchar *label)
{
  GtkWidget *button = gtk_button_new_with_label (label);
  gtk_misc_set_alignment (GTK_MISC (GTK_BIN (button)->child),
                          0.0, 0.5);
  gtk_misc_set_padding (GTK_MISC (GTK_BIN (button)->child),
                        GNOME_PAD_SMALL, 0);

  return button;
}

static void
update_selected_row(MWin* mw)
{
  if (mw->selected_account != NULL)
    {
      gtk_widget_set_sensitive(mw->remove_button, TRUE);
      gtk_widget_set_sensitive(mw->edit_button, TRUE);
      gtk_widget_set_sensitive(mw->clone_button, TRUE);
      gtk_widget_set_sensitive(mw->dial_button, TRUE);

      /* Use "Debug" if status is unknown */
      gtk_label_set_text(GTK_LABEL(GTK_BIN(mw->dial_button)->child),
                         (mw->selected_account->status == AccountUp) ?
                         _("Hang Up") : _("Debug"));
      rp3_set_tooltip(mw->dial_button,
		      (mw->selected_account->status == AccountUp) ?
		      _("Shut down the selected Internet connection") :
		      _("Dial the selected Internet connection"));
    }
  else
    {
      gtk_widget_set_sensitive(mw->remove_button, FALSE);
      gtk_widget_set_sensitive(mw->edit_button, FALSE);
      gtk_widget_set_sensitive(mw->clone_button, FALSE);
      gtk_widget_set_sensitive(mw->dial_button, FALSE);
    }
}

static void
select_row          (GtkCList       *clist,
                     gint            row,
                     gint            column,
                     GdkEvent       *event,
                     gpointer data)
{
  MWin* mw = (MWin*)data;

  mw->selected_row = row;

  mw->selected_account =
    (Account*)gtk_clist_get_row_data(GTK_CLIST(mw->clist), row);

  update_selected_row(mw);

  if (event && event->type == GDK_2BUTTON_PRESS)
    edit_cb (NULL, data);
}


static void
unselect_row        (GtkCList       *clist,
                     gint            row,
                     gint            column,
                     GdkEvent       *event,
                     gpointer data)
{
  MWin* mw = (MWin*)data;

  mw->selected_row = -1;

  mw->selected_account = NULL;

  update_selected_row(mw);
}


static void
modem_select_row          (GtkCList       *clist,
                           gint            row,
                           gint            column,
                           GdkEvent       *event,
                           gpointer data)
{
  MWin* mw = (MWin*)data;

  mw->modem_selected_row = row;
  mw->selected_modem =
    (Modem*)gtk_clist_get_row_data(GTK_CLIST(mw->modem_clist), row);

  if (event && event->type == GDK_2BUTTON_PRESS)
    modem_edit_cb (NULL, data);
}

static void
modem_unselect_row        (GtkCList       *clist,
                           gint            row,
                           gint            column,
                           GdkEvent       *event,
                           gpointer data)
{
  MWin* mw = (MWin*)data;

  mw->modem_selected_row = -1;

  mw->selected_modem = NULL;
}


static gint
delete_event_cb(GtkWidget* win, GdkEventAny* event, gpointer user_data)
{
  gtk_main_quit();

  rp3_exit(0);

  return FALSE;
}

static void
addwin_closed(GtkWidget* button, gpointer data)
{
  MWin* mw = (MWin*)data;

  mw->add_window = NULL;

  gtk_widget_set_sensitive(mw->win, TRUE);
}

static void
add_cb(GtkWidget* button, gpointer data)
{
  MWin* mw = (MWin*)data;

  gtk_widget_set_sensitive(mw->win, FALSE);

  mw->add_window = add_druid_new(mw->win);

  gtk_window_set_transient_for(GTK_WINDOW(mw->add_window), GTK_WINDOW(mw->win));

  gtk_signal_connect(GTK_OBJECT(mw->add_window),
                     "destroy",
                     GTK_SIGNAL_FUNC(addwin_closed),
                     mw);

  gtk_widget_set_sensitive(mw->win, FALSE);

  gtk_widget_show(mw->add_window);
}

static void
main_help_cb(GtkWidget* button, gpointer data)
{
    static GnomeHelpMenuEntry help_ref = { "rp3", "s1-screens-accounts-edit.html" };
    gnome_help_display(NULL, &help_ref);
}

static void
remove_cb(GtkWidget* button, gpointer data)
{
  MWin* mw = (MWin*)data;

  g_return_if_fail(mw->selected_account != NULL);

  rp3_mainwin_remove_account(mw->win, mw->selected_account);
}


static void
editwin_closed(GtkWidget* button, gpointer data)
{
  MWin* mw = (MWin*)data;

  mw->edit_window = NULL;

  gtk_widget_set_sensitive(mw->win, TRUE);
}

static void
edit_cb(GtkWidget* button, gpointer data)
{
  MWin* mw = (MWin*)data;

  g_return_if_fail(mw->selected_account != NULL);

  g_return_if_fail(mw->edit_window == NULL); /* since it's modal */

  mw->edit_window = rp3_editwin(mw->win, mw->selected_account);

  /* Zero edit_window on destroy */
  gtk_signal_connect(GTK_OBJECT(mw->edit_window),
                     "destroy",
                     GTK_SIGNAL_FUNC(editwin_closed),
                     mw);

  gtk_window_set_transient_for(GTK_WINDOW(mw->edit_window), GTK_WINDOW(mw->win));

  gtk_widget_set_sensitive(mw->win, FALSE);

  gtk_widget_show(mw->edit_window);
}

static void
clone_cb(GtkWidget* button, gpointer data)
{
  MWin* mw = (MWin*)data;

  g_return_if_fail(mw->selected_account != NULL);

  rp3_mainwin_add_account(mw->win, account_copy (mw->selected_account, mw->accounts));
}

static void
dial_cb(GtkWidget* button, gpointer data)
{
  MWin* mw = (MWin*)data;

  g_return_if_fail(mw->selected_account != NULL);

  if (mw->selected_account->status == AccountUp)
    account_hangup(mw->selected_account);
  else
    account_dial_and_log (mw->win, mw->selected_account);
}


static void
modem_add_cb(GtkWidget* button, gpointer data)
{
  MWin* mw = (MWin*)data;

  create_modem_edit_window (mw->win, NULL, mw->modems);
}


static void
modem_remove_cb(GtkWidget* button, gpointer data)
{
  MWin* mw = (MWin*)data;

  g_return_if_fail(mw->selected_modem != NULL);

  rp3_mainwin_remove_modem(mw->win, mw->selected_modem);
}

static void
modem_edit_cb(GtkWidget* button, gpointer data)
{
  MWin* mw = (MWin*)data;

  create_modem_edit_window (mw->win, mw->selected_modem, NULL);
}


GtkWidget*
rp3_mainwin_new(void)
{
  MWin* mw;
  GtkWidget* win;
  GtkWidget* vbox, *hbox;
  GtkWidget* notebook;
  GtkWidget* bbox;
  GtkWidget* clist;
  GtkWidget* button;
  GtkWidget* sw;
  GtkRcStyle *rc_style;
  GSList *ptr = NULL;
  GSList *list = NULL;
  gchar* titles[] = { _("Account Name"),
                      _("Phone Number"),
                      _("Status") };
  gchar* modem_titles[] = { _("Default"),
                            _("Modem Device"),
                            _("Speed") };

  mw = g_new0(MWin, 1);

  win = gnome_app_new ("rp3-config", _("Internet Connections"));

  gtk_window_set_title(GTK_WINDOW(win), _("Internet Connections"));

  gtk_object_set_data_full(GTK_OBJECT(win), "mw",
                           mw, (GtkDestroyNotify)mw_destroy);

  mw->win = win;

  gtk_signal_connect(GTK_OBJECT(win), "delete_event",
                     GTK_SIGNAL_FUNC(delete_event_cb),
                     NULL);

  gtk_window_set_default_size(GTK_WINDOW(win), 350, -1);

  notebook = gtk_notebook_new ();

  gtk_container_set_border_width (GTK_CONTAINER (notebook), GNOME_PAD_SMALL);

  vbox = gtk_vbox_new (FALSE, GNOME_PAD);

  gtk_notebook_append_page (GTK_NOTEBOOK (notebook),
                            vbox, gtk_label_new (_("Accounts")));


  /*Set up the Accounts page */
  hbox = gtk_hbox_new (FALSE, GNOME_PAD);

  gtk_container_set_border_width (GTK_CONTAINER (vbox), GNOME_PAD);

  gnome_app_set_contents (GNOME_APP (win), notebook);

  gtk_box_pack_start (GTK_BOX (vbox), hbox, TRUE, TRUE, 0);

  bbox = gtk_vbox_new (FALSE, GNOME_PAD_SMALL);

  button = left_aligned_button(_("Add..."));

  rp3_set_tooltip(button, _("Add a new Internet connection"));

  mw->add_button = button;
  gtk_signal_connect (GTK_OBJECT (button), "clicked",
                      GTK_SIGNAL_FUNC(add_cb), mw);

  gtk_box_pack_start (GTK_BOX (bbox), button, FALSE, FALSE, 0);

  button = left_aligned_button(_("Edit..."));

  mw->edit_button = button;

  gtk_signal_connect (GTK_OBJECT(button), "clicked",
                      GTK_SIGNAL_FUNC(edit_cb), mw);

  rp3_set_tooltip(button, _("Modify the selected Internet connection"));

  gtk_box_pack_start (GTK_BOX (bbox), button, FALSE, FALSE, 0);

  button = left_aligned_button(_("Delete"));

  mw->remove_button = button;

  gtk_signal_connect (GTK_OBJECT(button), "clicked",
                      GTK_SIGNAL_FUNC(remove_cb), mw);

  rp3_set_tooltip(button, _("Remove the selected Internet connection"));

  gtk_box_pack_start (GTK_BOX (bbox), button, FALSE, FALSE, 0);

  button = left_aligned_button(_("Copy"));

  mw->clone_button = button;

  gtk_signal_connect (GTK_OBJECT(button), "clicked",
                      GTK_SIGNAL_FUNC(clone_cb), mw);

  rp3_set_tooltip(button, _("Duplicate the selected Internet connection"));

  gtk_box_pack_start (GTK_BOX (bbox), button, FALSE, FALSE, 0);

  button = left_aligned_button(_("Dial"));

  mw->dial_button = button;

  gtk_signal_connect (GTK_OBJECT(button), "clicked",
                      GTK_SIGNAL_FUNC(dial_cb), mw);

  rp3_set_tooltip(button, _("Dial the selected Internet connection"));

  gtk_box_pack_start (GTK_BOX (bbox), button, FALSE, FALSE, 0);



  button = left_aligned_button(_("Close"));

  gtk_signal_connect (GTK_OBJECT(button), "clicked",
                      GTK_SIGNAL_FUNC(delete_event_cb), mw);

  rp3_set_tooltip(button, _("Close this dialog"));

  gtk_box_pack_end (GTK_BOX (bbox), button, FALSE, FALSE, 0);

  button = left_aligned_button(_("Help..."));

  gtk_signal_connect (GTK_OBJECT(button), "clicked",
                      GTK_SIGNAL_FUNC(main_help_cb), mw);

  rp3_set_tooltip(button, _("Help with configuring your Internet connections"));

  gtk_box_pack_end (GTK_BOX (bbox), button, FALSE, FALSE, 0);

  gtk_widget_set_sensitive(mw->remove_button, FALSE);
  gtk_widget_set_sensitive(mw->edit_button, FALSE);
  gtk_widget_set_sensitive(mw->clone_button, FALSE);
  gtk_widget_set_sensitive(mw->dial_button, FALSE);

  sw = gtk_scrolled_window_new(NULL,NULL);

  gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(sw),
                                 GTK_POLICY_AUTOMATIC,
                                 GTK_POLICY_AUTOMATIC);

  clist = gtk_clist_new_with_titles(3, titles);

  mw->clist = clist;

  gtk_clist_set_column_auto_resize(GTK_CLIST(clist), 0, TRUE);
  gtk_clist_set_column_auto_resize(GTK_CLIST(clist), 1, TRUE);
  gtk_clist_set_column_auto_resize(GTK_CLIST(clist), 2, TRUE);
  gtk_clist_set_selection_mode (GTK_CLIST(clist), GTK_SELECTION_BROWSE);

  gtk_container_add (GTK_CONTAINER(sw), clist);

  gtk_box_pack_start (GTK_BOX (hbox), sw, TRUE, TRUE, 0);
  gtk_box_pack_start (GTK_BOX (hbox), bbox, FALSE, FALSE, 0);

  gtk_signal_connect(GTK_OBJECT(clist),
                     "select_row",
                     GTK_SIGNAL_FUNC(select_row),
                     mw);


  gtk_signal_connect(GTK_OBJECT(clist),
                     "unselect_row",
                     GTK_SIGNAL_FUNC(unselect_row),
                     mw);

  gtk_widget_show_all(vbox);

  list = load_accounts ();
  for (ptr = list;ptr;ptr = ptr->next)
    {
      account_update ((Account *)ptr->data);
      rp3_mainwin_add_account_without_save (win, (Account *)ptr->data);
    }
  g_slist_free (list);

  /* Set up the modems page */

  hbox = gtk_hbox_new (FALSE, GNOME_PAD);

  gtk_container_set_border_width (GTK_CONTAINER (hbox), GNOME_PAD);


  gtk_notebook_append_page (GTK_NOTEBOOK (notebook),
                            hbox, gtk_label_new (_("Modems")));

  sw = gtk_scrolled_window_new(NULL,NULL);

  gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(sw),
                                 GTK_POLICY_AUTOMATIC,
                                 GTK_POLICY_AUTOMATIC);

  clist = gtk_clist_new_with_titles(3, modem_titles);
  mw->modem_clist = clist;
  gtk_clist_set_column_auto_resize(GTK_CLIST(clist), 0, TRUE);
  gtk_clist_set_column_auto_resize(GTK_CLIST(clist), 1, TRUE);
  gtk_clist_set_column_auto_resize(GTK_CLIST(clist), 2, TRUE);
  gtk_clist_set_selection_mode (GTK_CLIST(clist), GTK_SELECTION_BROWSE);

  gtk_signal_connect(GTK_OBJECT(clist),
                     "select_row",
                     GTK_SIGNAL_FUNC(modem_select_row),
                     mw);

  gtk_signal_connect(GTK_OBJECT(clist),
                     "unselect_row",
                     GTK_SIGNAL_FUNC(modem_unselect_row),
                     mw);


  /* Set up the modems */
  list = load_modems ();
  WvConf *cfg;
  const gchar *default_modem;
  cfg = new WvConf ("/etc/wvdial.conf");
  default_modem = cfg->get ("Dialer Defaults", "Modem");
  for (ptr = list;ptr;ptr = ptr->next)
  {
      rp3_mainwin_add_modem (win, (Modem *)ptr->data);
  }
  delete (cfg);
  g_slist_free (list);

  gtk_container_add (GTK_CONTAINER(sw), clist);

  gtk_box_pack_start (GTK_BOX (hbox), sw, TRUE, TRUE, 0);

  vbox = gtk_vbox_new (FALSE, GNOME_PAD_SMALL);

  button = left_aligned_button (_("Add..."));
  mw->modem_add_button = button;
  gtk_signal_connect (GTK_OBJECT (button),
                      "clicked", GTK_SIGNAL_FUNC (modem_add_cb),
                      mw);
  gtk_box_pack_start (GTK_BOX (vbox), button, FALSE, FALSE, 0);

  button = left_aligned_button (_("Edit..."));
  mw->modem_edit_button = button;
  gtk_signal_connect (GTK_OBJECT (button),
                      "clicked", GTK_SIGNAL_FUNC (modem_edit_cb),
                      mw);
  gtk_box_pack_start (GTK_BOX (vbox), button, FALSE, FALSE, 0);

  button = left_aligned_button (_("Delete"));
  mw->modem_delete_button = button;
  gtk_signal_connect (GTK_OBJECT (button),
                      "clicked", GTK_SIGNAL_FUNC (modem_remove_cb),
                      mw);
  gtk_box_pack_start (GTK_BOX (vbox), button, FALSE, FALSE, 0);

  button = left_aligned_button (_("Close"));
  gtk_signal_connect (GTK_OBJECT (button), "clicked", GTK_SIGNAL_FUNC (delete_event_cb), NULL);
  gtk_box_pack_end (GTK_BOX (vbox), button, FALSE, FALSE, 0);

  button = left_aligned_button (_("Help..."));

  gtk_signal_connect (GTK_OBJECT(button), "clicked",
                      GTK_SIGNAL_FUNC(main_help_cb), mw);

  gtk_box_pack_end (GTK_BOX (vbox), button, FALSE, FALSE, 0);

  gtk_box_pack_start (GTK_BOX (hbox), vbox, FALSE, FALSE, 0);
  gtk_widget_show_all (notebook);
  return win;
}

static void
text_from_account(gchar* cols[3], Account* a)
{
  cols[0] = a->name ? a->name : (gchar*)"Unknown account";
  cols[1] = a->number->complete;

  switch (a->status)
    {
    case AccountUnknown:
      cols[2] = _("Unknown status");
      break;
    case AccountUp:
      cols[2] = _("Connected");
      break;
    case AccountDown:
      cols[2] = _("Not connected");
      break;
    default:
      g_assert_not_reached();
    }
}

static void
rp3_mainwin_add_account_without_save(GtkWidget* w, Account* a)
{
  gchar* cols[3];
  gint row;
  MWin* mw;

  mw = (MWin*)gtk_object_get_data(GTK_OBJECT(w), "mw");
  g_assert(mw != NULL);

  text_from_account(cols, a);

  row = gtk_clist_append(GTK_CLIST(mw->clist), cols);

  gtk_clist_set_row_data(GTK_CLIST(mw->clist), row, a);

  gtk_clist_select_row(GTK_CLIST(mw->clist),
                       row, 0);

  mw->accounts = g_slist_prepend(mw->accounts, a);
}

void
rp3_mainwin_add_account(GtkWidget* w, Account* a)
{
  rp3_mainwin_add_account_without_save (w, a);

  account_save (a);
}

void
rp3_mainwin_add_modem(GtkWidget* w, Modem *modem)
{
  gchar* cols[3];
  gint row;
  MWin* mw;

  mw = (MWin*)gtk_object_get_data(GTK_OBJECT(w), "mw");
  g_assert(mw != NULL);

  cols[0] = "";
  cols[1] = modem->device;
  cols[2] = modem->speed;

  row = gtk_clist_append(GTK_CLIST(mw->modem_clist), cols);

  gtk_clist_set_row_data(GTK_CLIST(mw->modem_clist), row, modem);

  gtk_clist_select_row(GTK_CLIST(mw->modem_clist),
                       row, 0);

  mw->modems = g_slist_prepend(mw->modems, modem);
  update_default_modem (mw);
}


void
rp3_mainwin_remove_account(GtkWidget* w, Account* a)
{
  gint row;
  MWin* mw;

  mw = (MWin*)gtk_object_get_data(GTK_OBJECT(w), "mw");

  g_assert(mw != NULL);

  row = gtk_clist_find_row_from_data(GTK_CLIST(mw->clist),
                                     a);

  g_return_if_fail(row >= 0);

  gtk_clist_remove(GTK_CLIST(mw->clist), row);

  mw->accounts = g_slist_remove(mw->accounts, a);

  account_destroy(a);
}

static void
rp3_mainwin_remove_modem (GtkWidget* w, Modem* modem)
{
  gint row;
  MWin* mw;

  mw = (MWin*)gtk_object_get_data(GTK_OBJECT(w), "mw");

  g_assert(mw != NULL);

  row = gtk_clist_find_row_from_data(GTK_CLIST(mw->modem_clist),
                                     modem);

  g_return_if_fail(row >= 0);

  gtk_clist_remove(GTK_CLIST(mw->modem_clist), row);

  mw->modems = g_slist_remove(mw->modems, modem);

  modem_destroy(modem);
}

void
rp3_mainwin_account_changed(GtkWidget* w, Account* a)
{
  gchar* cols[3];
  gint row;
  MWin* mw;

  mw = (MWin*)gtk_object_get_data(GTK_OBJECT(w), "mw");

  g_assert(mw != NULL);

  text_from_account(cols, a);

  row = gtk_clist_find_row_from_data(GTK_CLIST(mw->clist),
                                     a);

  g_return_if_fail(row >= 0);

  gtk_clist_set_text(GTK_CLIST(mw->clist), row, 0, cols[0]);
  gtk_clist_set_text(GTK_CLIST(mw->clist), row, 1, cols[1]);
  gtk_clist_set_text(GTK_CLIST(mw->clist), row, 2, cols[2]);

  update_selected_row(mw);

  account_save (a);
}

static void
update_default_modem (MWin *mw)
{
  WvConf cfg = WvConf ("/etc/wvdial.conf");
  const gchar *default_device;
  Modem *modem;
  gint i;

  default_device = cfg.get ("Dialer Defaults", "Modem", "");

  for (i = 0; i < GTK_CLIST (mw->modem_clist)->rows; i++)
    {
      modem = (Modem *) gtk_clist_get_row_data (GTK_CLIST (mw->modem_clist), i);
      if (strcmp (modem->device, default_device) == 0) 
        {
          gtk_clist_set_text (GTK_CLIST (mw->modem_clist), i, 0, _("yes"));
        }
      else
        {
          gtk_clist_set_text (GTK_CLIST (mw->modem_clist), i, 0, "");
        }          
    }
}

void
rp3_mainwin_modem_changed(GtkWidget* w, Modem* modem)
{
  gint row;
  MWin* mw;

  mw = (MWin*)gtk_object_get_data(GTK_OBJECT(w), "mw");

  g_assert(mw != NULL);

  row = gtk_clist_find_row_from_data(GTK_CLIST(mw->modem_clist),
                                     modem);

  g_return_if_fail(row >= 0);

  gtk_clist_set_text(GTK_CLIST(mw->modem_clist), row, 1, modem->device);
  gtk_clist_set_text(GTK_CLIST(mw->modem_clist), row, 2, modem->speed);

  update_default_modem (mw);
}

void
rp3_editable_enters   (GtkWindow* win,
					   GtkEditable * editable)
{
  g_return_if_fail(win != NULL);
  g_return_if_fail(editable != NULL);
  g_return_if_fail(GTK_IS_WINDOW(win));
  g_return_if_fail(GTK_IS_EDITABLE(editable));

  gtk_signal_connect_object(GTK_OBJECT(editable), "activate",
                            GTK_SIGNAL_FUNC(gtk_window_activate_default),
                            GTK_OBJECT(win));
}

void
rp3_mainwin_update_accounts(GtkWidget* w)
{
  MWin* mw;
  gint nrows;
  gint row;

  /* This is inefficient but not importantly so */

  mw = (MWin*)gtk_object_get_data(GTK_OBJECT(w), "mw");

  nrows = GTK_CLIST(mw->clist)->rows;

  row = 0;
  while (row < nrows)
    {
      Account* a = (Account *) gtk_clist_get_row_data(GTK_CLIST(mw->clist),
                                                      row);

      g_assert(a != NULL);

      account_update(a);

      rp3_mainwin_account_changed(w, a);

      ++row;
    }
}

void
initial_add_window (GtkWidget *mainwin)
{
  MWin *mw = (MWin *) gtk_object_get_data (GTK_OBJECT (mainwin), "mw");
  add_cb (NULL, mw);
}

#include <netreport.h>

void
rp3_exit(int status)
{
  netreport_unsubscribe();
  exit(status);
}
