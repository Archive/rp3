/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* account.cc
 *
 * Copyright (C) 1999 Red Hat, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <gnome.h>
#include "account.h"

#include <interface.h>
#include <wvconf.h>

static gchar *get_new_account_name (gchar *old_name, GSList *accounts);

static void
set_string(gchar** dest, const gchar* src)
{
  if (*dest != NULL)
    g_free(*dest);

  *dest = src ? g_strdup(src) : NULL;
}

static void
phone_number_update_complete(PhoneNumber* pn)
{
  gchar *temp;
  if (pn->complete)
    g_free(pn->complete);

  if (pn->dial_prefix && *(pn->dial_prefix))
    temp = g_strconcat (pn->dial_prefix, ",", NULL);
  else
    temp = g_strdup ("");
  if (pn->area_code && *(pn->area_code))
    temp = g_strconcat (temp, pn->area_code, " ", NULL);
  temp = g_strconcat (temp, pn->number, NULL);
  pn->complete = temp;
}

PhoneNumber*
phone_number_new(void)
{
  PhoneNumber* pn;

  pn = g_new0(PhoneNumber, 1);

  pn->complete = g_strdup("");

  pn->dial_prefix = g_strdup("");
  pn->area_code = g_strdup("");
  pn->number = g_strdup("");

  return pn;
}

void
phone_number_destroy(PhoneNumber* pn)
{
  g_assert(pn != NULL);
  if(pn->dial_prefix)
    g_free(pn->dial_prefix);
  if(pn->area_code)
    g_free(pn->area_code);
  if(pn->number)
    g_free(pn->number);
  if(pn->complete)
    g_free(pn->complete);
  g_free(pn);
}

void
phone_number_set_prefix(PhoneNumber* pn, const gchar* prefix)
{
  set_string(&pn->dial_prefix, prefix);

  phone_number_update_complete(pn);
}

void
phone_number_set_areacode(PhoneNumber* pn, const gchar* ac)
{
  set_string(&pn->area_code, ac);
  phone_number_update_complete(pn);
}

void
account_set_stupidmode(Account* a, gboolean b)
{
  a->stupidmode = b;
}

void
phone_number_set_number(PhoneNumber* pn, const gchar* n)
{
  set_string(&pn->number, n);
  phone_number_update_complete(pn);
}

Account*
account_new (struct _interface* interface)
{
  Account* a;

  a = g_new0(Account, 1);
  if (interface != NULL) {
    a->interface = interface;

    gchar* tmp = svGetValue(a->interface->ifcfg, "WVDIALSECT");

    if (tmp == NULL)
      {
        tmp = g_strconcat("Dialer ", a->name, NULL);
        svSetValue(a->interface->ifcfg, "WVDIALSECT", a->name);
        a->wvdial_sect = tmp;
      }
    else
      {
        /* get in g_ memory space */
        a->wvdial_sect = g_strdup_printf("Dialer %s", tmp);
        free(tmp);
      }
  }

  a->number = phone_number_new();

  a->name = g_strdup("");
  a->username = g_strdup("");
  a->password = g_strdup("");
  a->modem_sect = g_strdup("");
  a->status = AccountUnknown;

  a->interface = interface;

  a->status = AccountUnknown;
  a->stupidmode = FALSE;
  a->onboot = FALSE;
  a->persist = FALSE;
  a->userctl = TRUE;
  return a;
}

static void
fix_name (gchar *name)
{
  gchar *ptr;

  for (ptr = name; *ptr != '\000'; ptr++)
    { 
      if (*ptr == ' ')
        {
          *ptr = '_';
        }
    }
}

void
account_set_name  (Account* a, const gchar* name)
{
  set_string(&a->name, name);
  fix_name (a->name);
}

void
account_set_username(Account* a, const gchar* name)
{
  set_string(&a->username, name);
}

void
account_set_password(Account* a, const gchar* pw)
{
  set_string(&a->password, pw);
}

void
account_set_number(Account* a, PhoneNumber* number)
{
  if (a->number)
    phone_number_destroy(a->number);

  a->number = number;
}

void
account_set_modem_sect(Account* a, const gchar* m)
{
  set_string(&a->modem_sect, m);
}


void
account_destroy   (Account* a)
{
  WvConf cfg("/etc/wvdial.conf");

  gchar *iface_file = NULL;
  if (a->number)
    phone_number_destroy(a->number);

  cfg.delete_section (a->wvdial_sect);

  g_free(a->name);
  g_free(a->username);
  g_free(a->password);
  g_free(a->wvdial_sect);


  iface_file = g_strdup_printf ("/etc/sysconfig/network-scripts/ifcfg-%s", a->interface->interfaceName);
  unlink (iface_file);
  g_free (iface_file);
  interface_free(a->interface);
  g_free(a);
}

Account*
account_clone     (Account* a,
                   GSList*  accounts)
{
  Account* dest;
  interface* iface;
  gchar* interface_name;
  gchar* friendly_clone_name;

  interface_name = interface_ppp_clone_new (a->interface->logicalName);

  friendly_clone_name = get_new_account_name (a->name, accounts);
  iface = interface_create (interface_name, a->interface->logicalName, friendly_clone_name);

  dest = account_new(iface);

  phone_number_set_number(dest->number, a->number->number);
  phone_number_set_prefix(dest->number, a->number->dial_prefix);
  phone_number_set_areacode(dest->number, a->number->area_code);

  account_set_name(dest, friendly_clone_name);
  account_set_username(dest, a->username);
  account_set_password(dest, a->password);
  account_set_modem_sect(dest, a->modem_sect);

  /* not necessarily share this */
  dest->status = AccountUnknown;
  g_free (friendly_clone_name);

  return dest;
}

Account*
account_copy     (Account* a,
                  GSList*  accounts)
{
  Account* dest;
  interface* iface;
  gchar* friendly_name;
  gchar* logical_name;


  logical_name = interface_logical_ppp_name_new();
  friendly_name = get_new_account_name (a->name, accounts);
  iface = interface_create (logical_name, logical_name, friendly_name);

  dest = account_new(iface);

  phone_number_set_number(dest->number, a->number->number);
  phone_number_set_prefix(dest->number, a->number->dial_prefix);
  phone_number_set_areacode(dest->number, a->number->area_code);

  account_set_name(dest, friendly_name);
  account_set_username(dest, a->username);
  account_set_password(dest, a->password);
  account_set_modem_sect(dest, a->modem_sect);

  dest->status = AccountUnknown;
  g_free (friendly_name);

  account_save (dest);
  return dest;
}

static const gchar valid_chars[] = "0123456789 ,-()*#";

gboolean
valid_phone_number_string(const gchar* text)
{
  const gchar *temp;

  if (text == NULL)
    return FALSE;

  for (temp = text; *temp; temp++) {
    if (strchr (valid_chars, *temp) == NULL)
      return FALSE;
  }
  return TRUE;
}

/* Link to wvdial stuff */

void
account_dial      (Account* a)
{
  interface_status_change(a->interface, INTERFACE_UP);
}

void
account_hangup    (Account* a)
{
  interface_status_change(a->interface, INTERFACE_DOWN);
}

gboolean
account_save      (Account* a)
{
  /* Update wvdial prefix, deleting old section if needed. */
  gchar* new_section;
  gchar* old_section;
  gboolean retval;
  struct _interface *interface;

  if (a->interface == NULL) {
    gchar* logical_name;
    logical_name = interface_logical_ppp_name_new();
    interface = interface_create (logical_name,logical_name, a->name);
    free (logical_name);
    a->interface = interface;

    gchar* tmp = svGetValue(a->interface->ifcfg, "WVDIALSECT");

    if (tmp == NULL)
      {
        tmp = g_strconcat("Dialer ", a->name, NULL);
        svSetValue(a->interface->ifcfg, "WVDIALSECT", a->name);
        a->wvdial_sect = tmp;
      }
    else
      {
        /* get in g_ memory space */
        a->wvdial_sect = g_strdup_printf ("Dialer %s",tmp);
        free(tmp);
      }
  }

  g_assert(a->wvdial_sect != NULL);
  svSetValue(a->interface->ifcfg, "WVDIALSECT", a->name);
  new_section = g_strconcat("Dialer ", a->name, NULL);
  old_section = a->wvdial_sect;
  a->wvdial_sect = new_section;

  WvConf cfg("/etc/wvdial.conf");

  cfg.set(a->wvdial_sect, "Username", a->username);
  cfg.set(a->wvdial_sect, "Password", a->password);
  cfg.set(a->wvdial_sect, "Phone", a->number->number);
  cfg.set(a->wvdial_sect, "Dial Prefix", a->number->dial_prefix);
  cfg.set(a->wvdial_sect, "Area Code", a->number->area_code);
  cfg.set(a->wvdial_sect, "Inherits", a->modem_sect);
  cfg.setint(a->wvdial_sect, WvString("Stupid mode"),
	     (a->stupidmode ? true : false));

  svSetValue(a->interface->ifcfg, "MODEMPORT", cfg.get(a->modem_sect, "Modem",
			  			       "/dev/modem"));
  svSetValue(a->interface->ifcfg, "LINESPEED", cfg.get(a->modem_sect, "Baud",
		  				       "57600"));
  svSetValue(a->interface->ifcfg, "NAME", a->name);
  svSetValue(a->interface->ifcfg, "PAPNAME", a->username);
  svSetValue(a->interface->ifcfg, "USERCTL",  (a->userctl?"true":"false"));
  svSetValue(a->interface->ifcfg, "ONBOOT", (a->onboot?"yes":"no"));
  svSetValue(a->interface->ifcfg, "PERSIST", (a->persist?"yes":"no"));
  /* Return true on successful write */
  retval = (svWriteFile(a->interface->ifcfg, 0644) == 0);

  /* Delete the old wvdial.conf section if appropriate,
     and free the old_section */
  if (strcmp(new_section, old_section) != 0)
    {
      cfg.delete_section(old_section);
    }
  g_free(old_section);

  return retval;
}

void
account_update    (Account* a)
{
  switch (interface_status_update(a->interface))
    {
    case INTERFACE_UNKNOWN:
      a->status = AccountUnknown;
      break;
    case INTERFACE_UP:
      a->status = AccountUp;
      break;
    case INTERFACE_DOWN:
      a->status = AccountDown;
      break;
    default:
      g_assert_not_reached();
      break;
    }
}

GSList*
load_accounts     (void)
{
  GList* ifs;
  GList* tmp;
  GSList* accounts = NULL;

  ifs = interface_list_generate();

  tmp = ifs;
  while (tmp != NULL)
    {
      interface* iface = (interface*)tmp->data;
      tmp = g_list_next(tmp);

      if (strncmp(iface->logicalName, "ppp", strlen("ppp")) == 0)
        {
          Account* a;
          char* temp;

          temp = svGetValue (iface->ifcfg, "WVDIALSECT");
          if (temp == NULL) 
            {
              interface_free (iface);
              continue;
            }
          free (temp);
          a = account_new(iface);

          WvConf *cfg = new WvConf("/etc/wvdial.conf");
          account_set_name(a, iface->friendlyName);

          account_set_username(a, cfg->get(a->wvdial_sect, "Username", ""));
          account_set_password(a, cfg->get(a->wvdial_sect, "Password", ""));
          account_set_modem_sect(a, cfg->get(a->wvdial_sect, "Inherits",
				 "Dialer Defaults"));

          a->stupidmode = cfg->getint(a->wvdial_sect, WvString("Stupid mode"),
				      false);
          a->onboot = svTrueValue (iface->ifcfg, "ONBOOT", TRUE);
          a->onboot = svTrueValue (iface->ifcfg, "PERSIST", TRUE);
          a->userctl = svTrueValue (iface->ifcfg, "USERCTL", FALSE);
          phone_number_set_prefix(a->number, cfg->get(a->wvdial_sect, "Dial Prefix", ""));
          phone_number_set_areacode(a->number, cfg->get(a->wvdial_sect, "Area Code", ""));
          phone_number_set_number(a->number, cfg->get(a->wvdial_sect, "Phone", ""));

          accounts = g_slist_prepend(accounts, a);

          g_free (cfg);
        }
      else
        {
          interface_free(iface);
        }
    }
  g_list_free(ifs);
  return accounts;
}

gboolean
is_unique_name (GSList *accounts, gchar *name)
{
  GSList *list;
  g_return_val_if_fail (name != NULL, FALSE);
  for (list = accounts; list; list = list->next)
    {
      if (list->data && !strcmp (name, ((Account *)list->data)->name))
        {
          return FALSE;
        }
    }
  return TRUE;
}

static gchar *
add_to_name (gchar *old_name, GSList *accounts)
{
  gint i = 2;
  gchar *retval;

  while (TRUE)
    {
      retval = g_strdup_printf ("%s<%d>", old_name, i);
      if (is_unique_name (accounts, retval)) /* check to see if it exists. */
        {
          return retval;
        }
      g_free (retval);
      i++;
    }
}

static gchar *
get_new_account_name (gchar  *old_name,
                      GSList *accounts)
{
  gchar *lbrace, *rbrace, *numptr;
  gint num;

  g_assert (old_name != NULL);
  /* First, is this of the format name<##> */
  if (old_name [strlen (old_name) -1] != '>')
    {
      return add_to_name (old_name, accounts);
    }
  lbrace = strrchr (old_name, '<');
  rbrace = strrchr (old_name, '>');
  if (lbrace == NULL)
    {
      return add_to_name (old_name, accounts);
    }

  num = strtol (lbrace + 1, &numptr, 0);

  if (numptr == rbrace)
    {
      gchar *base_string;
      base_string = (gchar *)g_malloc (sizeof (old_name));
      strncpy (base_string, old_name, lbrace - old_name);
      base_string[lbrace-old_name] = '\000';

      while (TRUE)
        {
          gchar *retval;
          retval = g_strdup_printf ("%s<%d>", base_string, ++num);
          if (is_unique_name (accounts, retval))
            {
              g_free (base_string);
              return retval;
            }
          else
            {
              g_free (retval);
            }
        }
    }
  else
    {
      return add_to_name (old_name, accounts);
    }
}
