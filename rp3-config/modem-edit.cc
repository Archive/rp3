/* modem-edit.cc
 *
 * Copyright (C) 1999 Red Hat, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "modem-edit.h"
#include "wvconf.h"
#include <unistd.h>
#include "mainwin.h"

static void
toggle_vol_cb (GtkWidget *widget, gpointer data)
{
	gtk_widget_set_sensitive (GTK_WIDGET (data), GTK_TOGGLE_BUTTON (widget)->active);
}

ModemEdit *
create_modem_edit ()
{
	ModemEdit *retval;
	GtkWidget *table1;
	GtkWidget *combo1;
	GList *combo1_items = NULL;
	GtkWidget *entry1;
	GtkWidget *label;
	GtkWidget *combo2;
	GList *combo2_items = NULL;
	GtkWidget *entry2;
	GtkWidget *hbox1;
	GtkWidget *label5;
	GtkWidget *hscale1;
	GtkWidget *label6;
	GtkWidget *hseparator1;

	retval = g_new (ModemEdit, 1);
	retval->widget = table1 = gtk_table_new (6, 2, FALSE);

	gtk_table_set_row_spacings (GTK_TABLE (table1), GNOME_PAD);
	gtk_table_set_col_spacings (GTK_TABLE (table1), GNOME_PAD_SMALL);

	combo1 = gtk_combo_new ();
	retval->device = combo1;
	gtk_table_attach (GTK_TABLE (table1), combo1, 1, 2, 0, 1,
			  (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
			  (GtkAttachOptions) (0), 0, 0);

	/* we don't want to add the mouse device to the list */
	gchar mouse_device[15];
	gint count;
	count = readlink ("/dev/mouse", mouse_device, sizeof(mouse_device) - 1);
	if (count != -1)
		mouse_device[count] = '\000';
	if (count == -1 || strcmp (mouse_device, "ttyS0"))
		combo1_items = g_list_append (combo1_items, (char*)"/dev/ttyS0");
	if (count == -1 || strcmp (mouse_device, "ttyS1"))
		combo1_items = g_list_append (combo1_items, (char*)"/dev/ttyS1");
	if (count == -1 || strcmp (mouse_device, "ttyS2"))
		combo1_items = g_list_append (combo1_items, (char*)"/dev/ttyS2");
	if (count == -1 || strcmp (mouse_device, "ttyS3"))
		combo1_items = g_list_append (combo1_items, (char*)"/dev/ttyS3");
	gtk_combo_set_popdown_strings (GTK_COMBO (combo1), combo1_items);
	g_list_free (combo1_items);

	label = gtk_label_new (_("Modem Device:"));
	gtk_table_attach (GTK_TABLE (table1), label, 0, 1, 0, 1,
			  (GtkAttachOptions) (GTK_EXPAND | GTK_SHRINK | GTK_FILL),
			  (GtkAttachOptions) (0), 0, 0);
	gtk_label_set_justify (GTK_LABEL (label), GTK_JUSTIFY_LEFT);
	gtk_misc_set_alignment (GTK_MISC (label), 0, 0.5);
	gtk_misc_set_padding (GTK_MISC (label), 2, 0);

	label = gtk_label_new (_("Baud Rate:"));
	gtk_table_attach (GTK_TABLE (table1), label, 0, 1, 1, 2,
			  (GtkAttachOptions) (GTK_EXPAND | GTK_SHRINK | GTK_FILL),
			  (GtkAttachOptions) (0), 0, 0);
	gtk_misc_set_alignment (GTK_MISC (label), 0, 0.5);
	gtk_misc_set_padding (GTK_MISC (label), 2, 0);

	label = gtk_label_new (_("Modem Volume:"));
	gtk_table_attach (GTK_TABLE (table1), label, 0, 1, 3, 4,
			  (GtkAttachOptions) (GTK_EXPAND | GTK_SHRINK | GTK_FILL),
			  (GtkAttachOptions) (0), 0, 0);
	gtk_label_set_justify (GTK_LABEL (label), GTK_JUSTIFY_LEFT);
	gtk_misc_set_alignment (GTK_MISC (label), 7.45058e-09, 0.5);
	gtk_misc_set_padding (GTK_MISC (label), 2, 0);

	combo2 = gtk_combo_new ();
	entry1 = GTK_COMBO (combo2)->entry;
	gtk_entry_set_editable (GTK_ENTRY (entry1), FALSE);
	retval->baud = combo2;
	gtk_table_attach (GTK_TABLE (table1), combo2, 1, 2, 1, 2,
			  (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
			  (GtkAttachOptions) (0), 0, 0);
	combo2_items = g_list_append (combo2_items, (gchar*)_("460800"));
	combo2_items = g_list_append (combo2_items, (gchar*)_("230400"));
	combo2_items = g_list_append (combo2_items, (gchar*)_("115200"));
	combo2_items = g_list_append (combo2_items, (gchar*)_("57600"));
	combo2_items = g_list_append (combo2_items, (gchar*)_("38400"));
	combo2_items = g_list_append (combo2_items, (gchar*)_("19200"));
	combo2_items = g_list_append (combo2_items, (gchar*)_("9600"));
	combo2_items = g_list_append (combo2_items, (gchar*)_("4800"));
	combo2_items = g_list_append (combo2_items, (gchar*)_("2400"));
	combo2_items = g_list_append (combo2_items, (gchar*)_("1200"));
	combo2_items = g_list_append (combo2_items, (gchar*)_("300"));

	gtk_combo_set_popdown_strings (GTK_COMBO (combo2), combo2_items);
	gtk_entry_set_text (GTK_ENTRY (entry1), "57600");
	g_list_free (combo2_items);

	hbox1 = gtk_hbox_new (FALSE, 0);
	retval->volbut = gtk_check_button_new_with_label (_("Set modem volume?"));
	gtk_signal_connect (GTK_OBJECT (retval->volbut), "toggled", GTK_SIGNAL_FUNC (toggle_vol_cb), hbox1);
	gtk_table_attach (GTK_TABLE (table1), retval->volbut, 0, 2, 2, 3,
			  (GtkAttachOptions) (GTK_FILL),
			  (GtkAttachOptions) (GTK_FILL), 0, 0);
	gtk_table_attach (GTK_TABLE (table1), hbox1, 1, 2, 3, 4,
			  (GtkAttachOptions) (GTK_FILL),
			  (GtkAttachOptions) (GTK_FILL), 0, 0);
	label = gtk_label_new (_("Quiet"));
	gtk_box_pack_start (GTK_BOX (hbox1), label, FALSE, FALSE, 0);

	hscale1 = gtk_hscale_new (GTK_ADJUSTMENT (gtk_adjustment_new (0, 0, 4, 1, 1, 1)));
	retval->volume = hscale1;
	gtk_box_pack_start (GTK_BOX (hbox1), hscale1, TRUE, TRUE, 2);
	gtk_scale_set_draw_value (GTK_SCALE (hscale1), FALSE);
	gtk_scale_set_digits (GTK_SCALE (hscale1), 0);
	gtk_range_set_update_policy (GTK_RANGE (hscale1), GTK_UPDATE_DISCONTINUOUS);

	label = gtk_label_new (_("Loud"));
	gtk_box_pack_start (GTK_BOX (hbox1), label, FALSE, FALSE, 0);
	gtk_widget_set_sensitive (GTK_WIDGET (hbox1), GTK_TOGGLE_BUTTON (retval->volbut)->active);

	retval->pulse = gtk_check_button_new_with_label (_("Use touch tone dialing?"));
	gtk_table_attach (GTK_TABLE (table1), retval->pulse, 0, 2, 4, 5,
			  (GtkAttachOptions) (GTK_EXPAND | GTK_SHRINK | GTK_FILL),
			  (GtkAttachOptions) (0), 0, 0);
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (retval->pulse), TRUE);

#if 0
	hseparator1 = gtk_hseparator_new ();
	gtk_table_attach (GTK_TABLE (table1), hseparator1, 0, 2, 3, 4,
			  (GtkAttachOptions) (GTK_SHRINK | GTK_FILL),
			  (GtkAttachOptions) (GTK_EXPAND | GTK_FILL), 0, 0);
#endif
	return retval;
}

gboolean
check_modem_edit (ModemEdit *edit)
{
	GtkWidget *entry;
	gchar *device;

	entry = GTK_COMBO (edit->device)->entry;
	device = gtk_entry_get_text (GTK_ENTRY (entry));
	if (device == NULL || *device == '\000' || *device != '/')
		return FALSE;
	return TRUE;
}

void
free_modem_edit (ModemEdit *edit)
{
	g_assert (edit != NULL);
        g_free (edit);
}

void
save_modem_edit (ModemEdit *edit, gchar *section, WvConf *cfg)
{
	g_return_if_fail (check_modem_edit (edit));

	GtkWidget *widget;
	gchar *text;
	WvConf *internal_cfg;

	if (cfg == NULL)
		internal_cfg = new WvConf ("/etc/wvdial.conf");
	else
		internal_cfg = cfg;

	widget = GTK_COMBO (edit->device)->entry;
	text = gtk_entry_get_text (GTK_ENTRY (widget));
	if (text == NULL || *text =='\000')
		return;
	internal_cfg->set(section, "Modem", text);

	widget = GTK_COMBO (edit->baud)->entry;
	text = gtk_entry_get_text (GTK_ENTRY (widget));
	if (text == NULL || *text =='\000')
		return;
	internal_cfg->set(section, "Baud", text);
	internal_cfg->set(section, "Init1", "ATZ");
	internal_cfg->setint(section, "SetVolume", GTK_TOGGLE_BUTTON (edit->volbut)->active);


	if (GTK_TOGGLE_BUTTON (edit->pulse)->active)
		internal_cfg->set (section, "Dial Command", "ATDT");
	else
		internal_cfg->set (section, "Dial Command", "ATDP");


	if (GTK_TOGGLE_BUTTON (edit->volbut)->active) {
		switch ((gint)GTK_RANGE (edit->volume)->adjustment->value) {
		case 0:
			internal_cfg->set (section, "Init4", "ATM0");
			break;
		case 1:
			internal_cfg->set (section, "Init4", "ATM1L1");
			break;
		case 2:
			internal_cfg->set (section, "Init4", "ATM1L2");
			break;
		case 3:
			internal_cfg->set (section, "Init4", "ATM1L3");
			break;
		default:
			g_warning ("UNKNOWN Volume %d\n", (gint) GTK_RANGE (edit->volume)->adjustment->value);
			break;
		}
	} else {
		internal_cfg->set (section, "Init4", NULL);
	}
	if (cfg == NULL)
		delete internal_cfg;
}
void
set_modem_edit (ModemEdit *edit, gchar *section, WvConf *cfg)
{
	const gchar *text;
	WvConf *internal_cfg;

	if (cfg == NULL)
		internal_cfg = new WvConf ("/etc/wvdial.conf");
	else
		internal_cfg = cfg;

	gtk_entry_set_text (GTK_ENTRY (GTK_COMBO (edit->device)->entry),
			    internal_cfg->get (section, "Modem", "/dev/ttyS0"));
	gtk_entry_set_text (GTK_ENTRY (GTK_COMBO (edit->baud)->entry),
			    internal_cfg->get (section, "Baud", "57600"));
	text = internal_cfg->get (section, "Dial Command", "ATDT");
	if (text && strcmp (text, "ATDT") == 0)
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (edit->pulse), TRUE);
	else
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (edit->pulse), FALSE);
	if (internal_cfg->get (section, "SetVolume", FALSE))
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (edit->volbut), TRUE);
	else {
		/* Toggle it to make sure that the hbox gets set correctly */
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (edit->volbut), TRUE);
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (edit->volbut), FALSE);
	}

	text = internal_cfg->get (section, "Init4", NULL);
	if (text && strcmp (text, "ATM0") == 0)
		gtk_adjustment_set_value (GTK_RANGE (edit->volume)->adjustment, 0);
	else if (text && strcmp (text, "ATM1L1") == 0)
		gtk_adjustment_set_value (GTK_RANGE (edit->volume)->adjustment, 1);
	else if (text && strcmp (text, "ATM1L2") == 0)
		gtk_adjustment_set_value (GTK_RANGE (edit->volume)->adjustment, 2);
	else
		gtk_adjustment_set_value (GTK_RANGE (edit->volume)->adjustment, 3);

	if (cfg == NULL)
		delete internal_cfg;
}


static void
auto_config_cb (GtkWidget *button, gpointer data)
{
	WvConf *cfg;
	ModemEdit *edit = (ModemEdit *) data;

	cfg = new WvConf ("/etc/wvdial.conf");
	/* blow away the "TEST_SECTION" section */
	cfg->delete_section ("TEST_SECTION");
	delete cfg;

	create_modem_dialog (gtk_widget_get_toplevel (button), "TEST_SECTION");

	cfg = new WvConf ("/etc/wvdial.conf");
	if ((*cfg)["TEST_SECTION"] == NULL) {
		GtkWidget *dialog;

		dialog = gnome_message_box_new (_("No modem was found on your system.\n\nYou should confirm that your modem is correctly\ninstalled, and try again.  You can explicitly set\nit up here, but it is unlikely to work.\nIf you have a Windows(R) specific software\nmodem, it is almost certainly not supported."), GNOME_MESSAGE_BOX_WARNING, GNOME_STOCK_BUTTON_CLOSE, NULL);
		gnome_dialog_set_parent (GNOME_DIALOG (dialog), GTK_WINDOW (gtk_widget_get_toplevel (edit->widget)));
		gtk_widget_set_sensitive (gtk_widget_get_toplevel (edit->widget), FALSE);
		gnome_dialog_run (GNOME_DIALOG (dialog));
		gtk_widget_set_sensitive (gtk_widget_get_toplevel (edit->widget), TRUE);
	} else {
		set_modem_edit (edit, "TEST_SECTION", cfg);
	}
	cfg->delete_section ("TEST_SECTION");
	delete cfg;
}

void
create_modem_edit_window (GtkWidget *mainwin, Modem *modem, GSList *list)
	/* modem and list are only needed when editting. */
{
	GtkWidget *win;
	GtkWidget *hbox;
	GtkWidget *label;
	GtkWidget *frame;
	ModemEdit *edit;
	GtkWidget *vbox;
	GtkWidget *button;
	GtkWidget *default_button;

	edit = create_modem_edit ();
	gtk_container_set_border_width (GTK_CONTAINER (edit->widget), GNOME_PAD_SMALL);
	win = gnome_dialog_new (_("Edit Modem Properties"), GNOME_STOCK_BUTTON_OK, GNOME_STOCK_BUTTON_CANCEL, NULL);

	gtk_object_set_data(GTK_OBJECT(win), "modem", modem);
	gtk_object_set_data(GTK_OBJECT(win), "mainwin", mainwin);

	if (modem == NULL) {
		/* if modem is NULL, we assume that we're creating a new modem. */
		WvConf *cfg = new WvConf ("/etc/wvdial.conf");

		vbox = GNOME_DIALOG (win)->vbox;
		label = gtk_label_new (_("Enter the information for your modem manually below, or press the \"Auto Configure\" button and rp3 will try to do it for you."));
		/* \n\nHint: If your modem is located at COM1 in MS-DOS, it's /dev/ttyS0 in Linux; COM2 is /dev/ttyS1, COM3 is /dev/ttyS2, COM4 is /dev/ttyS3.*/
		gtk_label_set_line_wrap (GTK_LABEL (label), TRUE);
		gtk_widget_set_usize (label, 400, -1);
		gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
		gtk_box_pack_start (GTK_BOX (vbox), label, FALSE, FALSE, 0);
		frame = gtk_frame_new (_("Modem Properties"));

		gtk_container_add (GTK_CONTAINER (frame), edit->widget);
		gtk_box_pack_start (GTK_BOX (vbox), frame, FALSE, FALSE, 0);

		/* check to see if the default matches */
		hbox = gtk_hbox_new (FALSE, 0);
		default_button = gtk_check_button_new_with_label (_("Make this modem the default modem"));
		gtk_widget_set_sensitive (default_button, TRUE);
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (default_button), FALSE);

		gtk_box_pack_start (GTK_BOX (hbox), default_button, FALSE, FALSE, 0);
		gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, FALSE, 0);
		hbox = gtk_hbox_new (FALSE, 0);
		button = gtk_button_new_with_label (_("Auto Configure"));
		gtk_signal_connect (GTK_OBJECT (button), "clicked", GTK_SIGNAL_FUNC (auto_config_cb), edit);
		gtk_box_pack_start (GTK_BOX (hbox), button, FALSE, FALSE, 0);
		gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, FALSE, 0);

		delete cfg;
	} else {
		/* We are editting the window, not creating it. */
		WvConf *cfg = new WvConf ("/etc/wvdial.conf");

		vbox = GNOME_DIALOG (win)->vbox;
		set_modem_edit (edit, modem->section, NULL);
		gtk_box_pack_start (GTK_BOX (vbox), edit->widget, FALSE, FALSE, 0);
		hbox = gtk_hbox_new (FALSE, 0);
		default_button = gtk_check_button_new_with_label (_("Make this modem the default modem"));
		if (strcmp (cfg->get ("Dialer Defaults", "Modem"), modem->device) == 0) {
			gtk_widget_set_sensitive (default_button, FALSE);
			gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (default_button), TRUE);
		}
		gtk_box_pack_start (GTK_BOX (hbox), default_button, FALSE, FALSE, 0);
		gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, FALSE, 0);
		gtk_widget_set_sensitive (edit->device, FALSE);
		delete cfg;
	}

	gtk_widget_set_sensitive(mainwin, FALSE);
	gnome_dialog_set_parent (GNOME_DIALOG (win), GTK_WINDOW (mainwin));
	gtk_widget_show_all (GNOME_DIALOG (win)->vbox);
	switch (gnome_dialog_run (GNOME_DIALOG (win))) {
	case 0:
		if (modem == NULL){
			/* We want to confirm that this is the only device of this type. */
			for (;list;list = list->next) {
				if (strcmp (((Modem*) list->data)->device, 
					    gtk_editable_get_chars (GTK_EDITABLE (GTK_COMBO (edit->device)->entry), 0, -1)) == 0) {
					modem = (Modem *)list->data;
					break;
				}
			}
		}
		if (modem) {
			save_modem_edit (edit, modem->section, NULL);
			if (GTK_TOGGLE_BUTTON (default_button)->active)
				save_modem_edit (edit, "Dialer Defaults", NULL);
			g_free (modem->device);
			modem->device = g_strdup (gtk_editable_get_chars (GTK_EDITABLE (GTK_COMBO (edit->device)->entry), 0, -1));
			g_free (modem->speed);
			modem->speed = g_strdup (gtk_editable_get_chars (GTK_EDITABLE (GTK_COMBO (edit->baud)->entry), 0, -1));
			rp3_mainwin_modem_changed (mainwin, modem);
		} else {
			gchar section [10];
			gint i;
			WvConf *cfg = new WvConf ("/etc/wvdial.conf");

			for (i = 0; i < 20; i++) {
				sprintf (section, "Modem%d", i);
				if ((*cfg)[section] == NULL)
					break;
			}
			save_modem_edit (edit, section, cfg);
			if (GTK_TOGGLE_BUTTON (default_button)->active)
				save_modem_edit (edit, "Dialer Defaults", cfg);
			modem = (Modem *) g_new (Modem, 1);
			modem->device = g_strdup ((*cfg)[section]->get ("Modem", ""));
			modem->speed = g_strdup ((*cfg)[section]->get ("Baud", "460800"));
			modem->section = g_strdup (section);
			delete cfg;

			rp3_mainwin_add_modem (mainwin, modem);
		}
	case 1:
		gtk_widget_destroy (win);
	}
	gtk_widget_set_sensitive(mainwin, TRUE);
}

