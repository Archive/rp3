#include <modemlog.h>
#include <errno.h>

ModemLog::ModemLog(gint pipe1)
  : WvLogRcv(WvLog::Debug5)
{
	outfd = pipe1;
}

ModemLog::~ModemLog()
{
}

void ModemLog::log(const WvLog *source, int loglevel,
		   const char *_buf, size_t len)
{
        /* I don't know if I want to use this */
	if (outfd == -1)
		return;
	if (write (outfd, _buf, len) == -1)
		g_print ("error %s\n", strerror (errno));
}

void ModemLog::_begin_line()
{
}

void ModemLog::_mid_line(const char *str, size_t len)
{
}

void ModemLog::_end_line()
{
}


