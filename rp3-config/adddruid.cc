#include <config.h>
#include <gnome.h>
#include "adddruid.h"
#include "account.h"
#include "mainwin.h"
#include "modem.h"
#include "modem-edit.h"
#include <interface.h>

static GdkImlibImage *logo = NULL;
GdkImlibImage *watermark = NULL;
static gchar *intro_text = \
N_("This dialog will help you set up a new Internet Connection.\n\n"\
"You can hit cancel at any point to stop.");

/* We use this to keep track of our Druid. */
static struct _DruidFlow {
	GtkWidget *intro_page;            /* Beginning */
	GtkWidget *modem_warning_page;    /* A - Warning page */
	GtkWidget *modem_setup_page;      /* B - you do want to add one! */
	GtkWidget *add_extra_modem_page;  /* C - do you want to add one? */
	GtkWidget *manual_modem_page;     /* D - Add modem page */
	GtkWidget *modem_select_page;     /* E - select default page */
	GtkWidget *name_page;             /* F - Canaan. */
} druid_flow;

static WvConf *cfg;

static void
warning_dialog (const gchar *message, GtkWindow * parent)
{
	GtkWidget *mbox;

        g_return_if_fail(GTK_IS_WINDOW(parent));

	mbox = gnome_message_box_new (message, GNOME_MESSAGE_BOX_WARNING,
				      GNOME_STOCK_BUTTON_OK, NULL);

	if (parent != NULL) {
		gnome_dialog_set_parent(GNOME_DIALOG (mbox),
                                        GTK_WINDOW(parent));
	}

        gtk_widget_set_sensitive(GTK_WIDGET(parent), FALSE);
	gnome_dialog_run_and_close (GNOME_DIALOG (mbox));
        gtk_widget_set_sensitive(GTK_WIDGET(parent), TRUE);
}

static GtkWidget *
create_druid_standard (gchar *title)
{
	GtkWidget *druid_page;
	GdkColor color;
	GnomeCanvasItem *item;
	static GdkImlibImage *image = NULL;

	if (image == NULL)
		image = gdk_imlib_load_image ("/usr/share/pixmaps/rp3/rp3-top.png");

	druid_page = gnome_druid_page_standard_new_with_vals (title, logo);
	color.red = 71*255;
	color.green = 107*255;
	color.blue = 179*255;
	gnome_druid_page_standard_set_bg_color (GNOME_DRUID_PAGE_STANDARD (druid_page),
						&color);

	item = gnome_canvas_item_new (gnome_canvas_root (GNOME_CANVAS (GNOME_DRUID_PAGE_STANDARD (druid_page)->canvas)),
				      gnome_canvas_image_get_type (),
				      "image", image,
				      "x", 0.0,
				      "y", 0.0,
				      "anchor", GTK_ANCHOR_NORTH_WEST,
				      "width", (gfloat) 462,
				      "height", (gfloat) 67,
				      NULL);
	gnome_canvas_item_raise_to_top (GNOME_DRUID_PAGE_STANDARD (druid_page)->title_item);
	return druid_page;
}

/* Global Callbacks */
static void
druid_cancel_callback (GtkWidget *widget, gpointer window)
{
	gtk_widget_destroy (gtk_widget_get_toplevel (widget));
	delete (cfg);
}

static gboolean
generic_back_callback (GtkWidget *druid_page, GtkWidget *druid, gpointer data)
{
	GtkWidget *back_page = (GtkWidget *) (gtk_object_get_data (GTK_OBJECT (druid_page), "back"));

	if (back_page) {
		gnome_druid_set_page (GNOME_DRUID (druid), GNOME_DRUID_PAGE (back_page));
		return TRUE;
	}
	return FALSE;
}

/* Intro page */
static gboolean
intro_page_next_callback (GtkWidget *druid_page, GtkWidget *druid, gpointer data)
{
	ConfStatus status = check_conf (cfg);
	if ((status == CONF_MODEM_DEFAULT_FOUND) || (status == CONF_MODEMS_DEFAULT_FOUND)) {
		/* We already have our modem configured. */
		gtk_object_set_data (GTK_OBJECT (druid_flow.name_page), "back", druid_page);
		gnome_druid_set_page (GNOME_DRUID (druid), GNOME_DRUID_PAGE (druid_flow.name_page));
		return TRUE;
	} else if (status == CONF_MODEM_FOUND || status == CONF_MODEMS_FOUND) {
	        /* I can't imagine this ever happening, but... */
		/* Here, we have found our modem, but haven't set a default */
		/* we prolly want something different for CONF_MODEM_FOUND */
		gtk_object_set_data (GTK_OBJECT (druid_flow.modem_select_page), "back", druid_page);
		gnome_druid_set_page (GNOME_DRUID (druid), GNOME_DRUID_PAGE (druid_flow.modem_select_page));
		return TRUE;
	}
	gtk_object_set_data (GTK_OBJECT (druid_flow.modem_warning_page), "back", NULL);
	return FALSE;
}

static GtkWidget *
create_intro_page (Account *account)
{
	GtkWidget *druid_page;
	GdkColor color;
	GnomeCanvasItem *item;
	static GdkImlibImage *image = NULL;

	if (image == NULL)
		image = gdk_imlib_load_image ("/usr/share/pixmaps/rp3/rp3-top.png");

	druid_page = gnome_druid_page_start_new_with_vals (_("Create a new Internet Connection"), _(intro_text), logo, watermark);
	gtk_signal_connect (GTK_OBJECT (druid_page), "next", GTK_SIGNAL_FUNC (intro_page_next_callback), account);
	druid_flow.intro_page = druid_page;
	color.red = 71*255;
	color.green = 107*255;
	color.blue = 179*255;
	gnome_druid_page_start_set_bg_color (GNOME_DRUID_PAGE_START (druid_page),
					     &color);

	item = gnome_canvas_item_new (gnome_canvas_root (GNOME_CANVAS (GNOME_DRUID_PAGE_START (druid_page)->canvas)),
				      gnome_canvas_image_get_type (),
				      "image", image,
				      "x", 0.0,
				      "y", 0.0,
				      "anchor", GTK_ANCHOR_NORTH_WEST,
				      "width", (gfloat) 462,
				      "height", (gfloat) 67,
				      NULL);
	gnome_canvas_item_raise_to_top (GNOME_DRUID_PAGE_START (druid_page)->title_item);

	return druid_page;
}


/* A - Modem warning page */
static gboolean
modem_warning_page_next_callback (GtkWidget *druid_page, GtkWidget *druid, gpointer data)
{
	GtkWidget *toplevel;

	toplevel = gtk_widget_get_toplevel (druid);
	delete (cfg);
	cfg = NULL;
	create_modem_dialog (toplevel, NULL);

	cfg = new WvConf ("/etc/wvdial.conf");

	ConfStatus status = check_conf (cfg);
	if (status == CONF_NONEXISTENT) {
		/* GOTO B */
		gtk_object_set_data (GTK_OBJECT (druid_flow.modem_setup_page), "back", NULL);
		return FALSE;
	}

	/* GOTO C */
	gtk_object_set_data (GTK_OBJECT (druid_flow.add_extra_modem_page), "back", druid_page);
	gnome_druid_set_page (GNOME_DRUID (druid), GNOME_DRUID_PAGE (druid_flow.add_extra_modem_page));
	return TRUE;
}

static GtkWidget *
create_modem_warning_page (GtkWidget* druid, Account *account)
{
	GtkWidget *druid_page;
        GtkWidget *toplevel;
	GtkWidget *align;
	GtkWidget *vbox, *vbox2;
	GtkWidget *hbox;
	GtkWidget *label;
	GtkWidget *button;

        toplevel = gtk_widget_get_toplevel(druid);

	druid_page = create_druid_standard (_("Select Modem"));
	druid_flow.modem_warning_page = druid_page;

	vbox = GNOME_DRUID_PAGE_STANDARD (druid_page)->vbox;
	gtk_box_set_spacing (GTK_BOX (vbox), 0);
	gtk_container_set_border_width (GTK_CONTAINER (vbox), GNOME_PAD);
	gtk_signal_connect (GTK_OBJECT (druid_page), "next", GTK_SIGNAL_FUNC (modem_warning_page_next_callback), account);
	gtk_signal_connect (GTK_OBJECT (druid_page), "back", GTK_SIGNAL_FUNC (generic_back_callback), NULL);

	align = gtk_alignment_new (0.0, 0.0, 0.0, 0.0);
	gtk_box_pack_start (GTK_BOX (vbox), align, FALSE, FALSE, 0);
	vbox2 = gtk_vbox_new (FALSE, GNOME_PAD_SMALL);
	gtk_container_add (GTK_CONTAINER (align), vbox2);
	hbox = gtk_hbox_new (FALSE, 0);
	label = gtk_label_new (_("You do not appear to have a modem configured yet.  You must configure one before going on.\n\nClick the \"Next\" button to do so."));
	gtk_label_set_line_wrap (GTK_LABEL (label), TRUE);
	gtk_label_set_justify (GTK_LABEL (label), GTK_JUSTIFY_LEFT);
	gtk_box_pack_start (GTK_BOX (hbox), label, TRUE, TRUE, 0);
	gtk_box_pack_start (GTK_BOX (vbox2), hbox, FALSE, FALSE, 0);
	gtk_widget_show_all (vbox2);
	return druid_page;
}

/* B - Modem setup page */
static gboolean
modem_setup_page_next_callback (GtkWidget *druid_page, GtkWidget *druid, gpointer data)
{
	ModemEdit *edit;
	Modem *modem;
	GtkWidget *mainwin;

	edit = (ModemEdit *)gtk_object_get_data (GTK_OBJECT (druid_page), "modem_edit");
	if (!check_modem_edit (edit)) {
		warning_dialog (_("You must enter a valid file name for your modem device."), GTK_WINDOW (gtk_widget_get_toplevel (druid)));
		return TRUE;
	}

	save_modem_edit (edit, "Modem0", cfg);
	save_modem_edit (edit, "Dialer Defaults", cfg);
	modem = (Modem *) g_new (Modem, 1);
	modem->device = g_strdup ((*cfg)["Modem0"]->get ("Modem", ""));
	modem->speed = g_strdup ((*cfg)["Modem0"]->get ("Baud", "460800"));
	modem->section = g_strdup ("Modem0");
	
	delete cfg;
        mainwin = GTK_WIDGET(gtk_object_get_data(GTK_OBJECT(druid), "mainwin"));
	rp3_mainwin_add_modem (mainwin, modem);
	cfg = new WvConf ("/etc/wvdial.conf");

	/* GOTO F */
	gtk_object_set_data (GTK_OBJECT (druid_flow.name_page), "back", druid_page);
	gnome_druid_set_page (GNOME_DRUID (druid), GNOME_DRUID_PAGE (druid_flow.name_page));
	return TRUE;
}

static GtkWidget *
create_modem_setup_page (GtkWidget* druid, Account *account)
{
	GtkWidget *druid_page;
        GtkWidget *toplevel;
	GtkWidget *align;
	GtkWidget *vbox, *vbox2;
	GtkWidget *hbox;
	GtkWidget *label;
	GtkWidget *frame;
	ModemEdit *edit;

        toplevel = gtk_widget_get_toplevel(druid);

	druid_page = create_druid_standard (_("Enter a modem"));
	druid_flow.modem_setup_page = druid_page;

	vbox = GNOME_DRUID_PAGE_STANDARD (druid_page)->vbox;
	gtk_box_set_spacing (GTK_BOX (vbox), 0);
	gtk_container_set_border_width (GTK_CONTAINER (vbox), GNOME_PAD);
	gtk_signal_connect (GTK_OBJECT (druid_page), "next", GTK_SIGNAL_FUNC (modem_setup_page_next_callback), account);
	gtk_signal_connect (GTK_OBJECT (druid_page), "back", GTK_SIGNAL_FUNC (generic_back_callback), NULL);

	align = gtk_alignment_new (0.0, 0.0, 0.0, 0.0);
	gtk_box_pack_start (GTK_BOX (vbox), align, FALSE, FALSE, 0);
	vbox2 = gtk_vbox_new (FALSE, GNOME_PAD);
	gtk_container_add (GTK_CONTAINER (align), vbox2);
	hbox = gtk_hbox_new (FALSE, 0);
	label = gtk_label_new (_("No modems were detected on your system.\n\nPlease enter one manually below:"));
	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
	gtk_label_set_line_wrap (GTK_LABEL (label), TRUE);
	gtk_label_set_justify (GTK_LABEL (label), GTK_JUSTIFY_LEFT);
	gtk_box_pack_start (GTK_BOX (hbox), label, TRUE, TRUE, 0);
	gtk_box_pack_start (GTK_BOX (vbox2), hbox, FALSE, FALSE, 0);
	frame = gtk_frame_new (_("Modem Settings"));
	edit = create_modem_edit ();
	gtk_object_set_data (GTK_OBJECT (druid_page), "modem_edit", edit);
	gtk_container_set_border_width (GTK_CONTAINER (edit->widget), GNOME_PAD_SMALL);
	gtk_container_add (GTK_CONTAINER (frame), edit->widget);
	gtk_box_pack_start (GTK_BOX (vbox2), frame, FALSE, FALSE, 0);
	gtk_widget_show_all (vbox2);
	return druid_page;
}

/* C - Modem setup page */
static gboolean
add_extra_modem_page_next_callback (GtkWidget *druid_page, GtkWidget *druid, gpointer data)
{
	ConfStatus status;
	GtkWidget *radio2;
	status = check_conf (cfg);
	if (status == CONF_NONEXISTENT) {
		warning_dialog (_("You have lost your modems, somehow.  Reconfiguring.\n"), GTK_WINDOW (gtk_widget_get_toplevel (druid)));
		/* Back to square 1 */
		gtk_object_set_data (GTK_OBJECT (druid_flow.name_page), "back", NULL);
		gnome_druid_set_page (GNOME_DRUID (druid), GNOME_DRUID_PAGE (druid_flow.modem_warning_page));
		return TRUE;
	}


	radio2 = GTK_WIDGET (gtk_object_get_data (GTK_OBJECT (druid_page), "radio2"));
	if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (radio2))) {
		gtk_object_set_data (GTK_OBJECT (druid_flow.manual_modem_page), "back", druid_page);
		gnome_druid_set_page (GNOME_DRUID (druid), GNOME_DRUID_PAGE (druid_flow.manual_modem_page));
		return TRUE;
	}

	if (status == CONF_MODEMS_FOUND || status == CONF_MODEMS_DEFAULT_FOUND) {
		gtk_object_set_data (GTK_OBJECT (druid_flow.modem_select_page), "back", druid_page);
		gnome_druid_set_page (GNOME_DRUID (druid), GNOME_DRUID_PAGE (druid_flow.modem_select_page));
		return TRUE;
	}

	if (status == CONF_MODEM_FOUND) {
		/* We only have one modem. */
		/* We want to set the default. */
		copy_sections (cfg, "Modem0", "Dialer Defaults");
	}
	/* The new modem was added fine */
	/* GOTO F */
	gtk_object_set_data (GTK_OBJECT (druid_flow.name_page), "back", druid_page);
	gnome_druid_set_page (GNOME_DRUID (druid), GNOME_DRUID_PAGE (druid_flow.name_page));
	return TRUE;
}

static void
prepare_add_extra_modem_page_callback (GtkWidget *druid_page, GtkWidget *druid, gpointer data)
{
	ConfStatus status;
	GtkWidget *found_label;
	GtkWidget *clist;
	GtkWidget *radio1;
	GtkWidget *radio2;
	gint i = 0;

	status = check_conf (cfg);
	if (status == CONF_NONEXISTENT) {
		warning_dialog (_("You seem to have lost your modems.\n"), GTK_WINDOW (gtk_widget_get_toplevel (druid)));
		/* I wonder if this works.  It might actually, but I'm not sure what will happen. */
		/* In any case, back to square 1 */
		gnome_druid_set_page (GNOME_DRUID (druid), GNOME_DRUID_PAGE (druid_flow.modem_warning_page));
		return;
	}


	if (status == CONF_MODEM_DEFAULT_FOUND || status == CONF_MODEMS_DEFAULT_FOUND)
		/* I need to think about this more.  If we're coming back, then this is the case. */
		/* we're prolly safe leaving everything alone */
		return;
	found_label = GTK_WIDGET (gtk_object_get_data (GTK_OBJECT (druid_page), "found_label"));
	clist = GTK_WIDGET (gtk_object_get_data (GTK_OBJECT (druid_page), "clist"));
	radio1 = GTK_WIDGET (gtk_object_get_data (GTK_OBJECT (druid_page), "radio1"));
	radio2 = GTK_WIDGET (gtk_object_get_data (GTK_OBJECT (druid_page), "radio2"));

	gchar *text[2];
	gint baud;

	gtk_clist_freeze (GTK_CLIST (clist));
	gtk_clist_clear (GTK_CLIST (clist));

	WvConfigSectionList::Iter iter (*cfg);
	for (iter.rewind (); iter.next ();) {
		if  (strncmp (iter ().name, "Modem", strlen ("Modem")) != 0)
			continue;
		text[0] = g_strdup(iter().get("Modem"));
		if (text[0] == NULL)
			break;
		baud = atoi (iter().get("Baud", 0));
		if (baud == 0)
			break;

		/* Gratuituous reuse of variable. */
		text[1] = g_strdup_printf ("%d", baud);
		gtk_clist_insert (GTK_CLIST (clist), 0, text);
		g_free(text[0]);
		g_free(text[1]);
		i++;
	}
	gtk_clist_columns_autosize (GTK_CLIST (clist));
	gtk_clist_thaw (GTK_CLIST (clist));

	if (i > 1) {
		gtk_label_set_text (GTK_LABEL (found_label),
				    _("The following modems were found on your system."));
		gtk_label_set_text (GTK_LABEL (GTK_BIN (radio1)->child),
				    _("Keep these modems."));
		gtk_label_set_text (GTK_LABEL (GTK_BIN (radio2)->child),
				    _("Modify a modem manually, or add a new modem."));
	} else {
		gtk_label_set_text (GTK_LABEL (found_label),
				    _("The following modem was found on your system."));
		gtk_label_set_text (GTK_LABEL (GTK_BIN (radio1)->child),
				    _("Keep this modem."));
		gtk_label_set_text (GTK_LABEL (GTK_BIN (radio2)->child),
				    _("Modify this modem manually, or add a new modem."));
	}

}
static GtkWidget *
create_add_extra_modem_page (GtkWidget* druid, Account *account)
{
	GtkWidget *druid_page;
        GtkWidget *toplevel;
	GtkWidget *align;
	GtkWidget *vbox, *vbox2;
	GtkWidget *hbox;
	GtkWidget *label;
	GtkWidget *clist;
	GtkWidget *radio;
	GSList    *radio_grp = NULL;
	gchar *titles[] = {"Device", "Baud"};

        toplevel = gtk_widget_get_toplevel(druid);

	druid_page = create_druid_standard (_("Modem found"));
	druid_flow.add_extra_modem_page = druid_page;

	vbox = GNOME_DRUID_PAGE_STANDARD (druid_page)->vbox;
	gtk_box_set_spacing (GTK_BOX (vbox), 0);
	gtk_container_set_border_width (GTK_CONTAINER (vbox), GNOME_PAD);
	gtk_signal_connect (GTK_OBJECT (druid_page), "next", GTK_SIGNAL_FUNC (add_extra_modem_page_next_callback), account);
	gtk_signal_connect (GTK_OBJECT (druid_page), "prepare", GTK_SIGNAL_FUNC (prepare_add_extra_modem_page_callback), account);
	gtk_signal_connect (GTK_OBJECT (druid_page), "back", GTK_SIGNAL_FUNC (generic_back_callback), NULL);

	align = gtk_alignment_new (0.0, 0.0, 0.0, 0.0);
	gtk_box_pack_start (GTK_BOX (vbox), align, FALSE, FALSE, 0);
	vbox2 = gtk_vbox_new (FALSE, GNOME_PAD_SMALL);
	gtk_container_add (GTK_CONTAINER (align), vbox2);
	label = gtk_label_new ("The following modems have been found");
	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
	gtk_object_set_data (GTK_OBJECT (druid_page), "found_label", label);
	gtk_label_set_line_wrap (GTK_LABEL (label), TRUE);
	gtk_label_set_justify (GTK_LABEL (label), GTK_JUSTIFY_LEFT);
	gtk_box_pack_start (GTK_BOX (vbox2), label, TRUE, TRUE, 0);

	clist = gtk_clist_new_with_titles (2, titles);
	gtk_object_set_data (GTK_OBJECT (druid_page), "clist", clist);
	gtk_widget_set_usize (clist, -1, 100);
	gtk_clist_set_selection_mode (GTK_CLIST (clist), GTK_SELECTION_SINGLE);
	gtk_box_pack_start (GTK_BOX (vbox2), clist, FALSE, FALSE, 0);

	radio= gtk_radio_button_new_with_label (NULL, "");
	gtk_object_set_data (GTK_OBJECT (druid_page), "radio1", radio);
        gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (radio), TRUE);
        radio_grp = gtk_radio_button_group (GTK_RADIO_BUTTON (radio));
        gtk_box_pack_start (GTK_BOX (vbox2), radio, FALSE, FALSE, 0);

        radio = gtk_radio_button_new_with_label(radio_grp, "");
	gtk_object_set_data (GTK_OBJECT (druid_page), "radio2", radio);
        gtk_box_pack_start (GTK_BOX (vbox2), radio, TRUE, TRUE, 0);

	gtk_widget_show_all (vbox2);
	return druid_page;
}


/* D - Manual Modem page */
static gboolean
manual_modem_page_next_callback (GtkWidget *druid_page, GtkWidget *druid, gpointer data)
{
	/* we want to save the modem.  If the device already exists, we
	 * overwrite it.  if we have more then one modem, we go to E,
	 * otherwise, we go to F */
	ModemEdit *edit;
	ConfStatus status;
	gchar section[10];
	gchar *device;
	const gchar *old_device = NULL;
	gint i;

	edit = (ModemEdit *)gtk_object_get_data (GTK_OBJECT (druid_page), "modem_edit");
	device = gtk_entry_get_text (GTK_ENTRY (GTK_COMBO (edit->device)->entry));

	for (i = 0; i < 20; i++) {
		/* no one with more then 20 modems is using this tool, right... */
		g_snprintf (section, 9, "Modem%d", i);
		old_device = cfg->get(section, "Modem");
		if ((old_device == NULL) ||
		    (!strcmp (old_device, device)))
			break;
	}

	save_modem_edit (edit, section, cfg);
	status = check_conf (cfg);
	if (status == CONF_MODEMS_FOUND || status == CONF_MODEMS_DEFAULT_FOUND) {
		/* We need to set a default */
		gtk_object_set_data (GTK_OBJECT (druid_flow.modem_select_page), "back", druid_page);
		gnome_druid_set_page (GNOME_DRUID (druid), GNOME_DRUID_PAGE (druid_flow.modem_select_page));
		return TRUE;
	}
	if (status == CONF_MODEM_FOUND) {
		/* Set the Default!!! */
		copy_sections (cfg, section, "Dialer Defaults");
	}

	gtk_object_set_data (GTK_OBJECT (druid_flow.name_page), "back", druid_flow.manual_modem_page);
	gnome_druid_set_page (GNOME_DRUID (druid), GNOME_DRUID_PAGE (druid_flow.name_page));
	return TRUE;
}

static void
prepare_manual_modem_page_callback (GtkWidget *druid_page, GtkWidget *druid, gpointer data)
{
	ModemEdit *edit;
	const gchar *text;
	gchar *baud_string;
	gint baud;

	edit = (ModemEdit *)gtk_object_get_data (GTK_OBJECT (druid_page), "modem_edit");
	/* This is sort of ugly.  If we have never been here before, then
	 * baud will be empty.  If we have, then it will have a value. */
	text = gtk_entry_get_text (GTK_ENTRY (GTK_COMBO (edit->baud)->entry));

	if (text != NULL && *text == '\000') {

		text = cfg->get("Modem0", "Modem");
		gtk_entry_set_text (GTK_ENTRY (GTK_COMBO (edit->device)->entry), text);
		baud = cfg->getint("Modem0", "Baud", 0);
		if (baud != 0) {
			baud_string = g_strdup_printf ( "%d", baud);
			gtk_entry_set_text (GTK_ENTRY (GTK_COMBO (edit->baud)->entry), baud_string);
			g_free (baud_string);
		}
	}
}

static GtkWidget *
create_manual_modem_page (GtkWidget* druid, Account *account)
{

	GtkWidget *druid_page;
        GtkWidget *toplevel;
	GtkWidget *align;
	GtkWidget *vbox, *vbox2;
	GtkWidget *hbox;
	GtkWidget *label;
	GtkWidget *frame;
	ModemEdit *edit;

        toplevel = gtk_widget_get_toplevel(druid);

	druid_page = create_druid_standard (_("Manually create a modem"));
	druid_flow.manual_modem_page = druid_page;

	vbox = GNOME_DRUID_PAGE_STANDARD (druid_page)->vbox;
	gtk_box_set_spacing (GTK_BOX (vbox), 0);
	gtk_container_set_border_width (GTK_CONTAINER (vbox), GNOME_PAD);
	gtk_signal_connect (GTK_OBJECT (druid_page), "next", GTK_SIGNAL_FUNC (manual_modem_page_next_callback), account);
	gtk_signal_connect (GTK_OBJECT (druid_page), "prepare", GTK_SIGNAL_FUNC (prepare_manual_modem_page_callback), account);
	gtk_signal_connect (GTK_OBJECT (druid_page), "back", GTK_SIGNAL_FUNC (generic_back_callback), NULL);

	align = gtk_alignment_new (0.0, 0.0, 0.0, 0.0);
	gtk_box_pack_start (GTK_BOX (vbox), align, FALSE, FALSE, 0);
	vbox2 = gtk_vbox_new (FALSE, GNOME_PAD);
	gtk_container_add (GTK_CONTAINER (align), vbox2);
	hbox = gtk_hbox_new (FALSE, 0);
	label = gtk_label_new (_("Set up your modem below:"));
	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
	gtk_label_set_line_wrap (GTK_LABEL (label), TRUE);
	gtk_label_set_justify (GTK_LABEL (label), GTK_JUSTIFY_LEFT);
	gtk_box_pack_start (GTK_BOX (hbox), label, TRUE, TRUE, 0);
	gtk_box_pack_start (GTK_BOX (vbox2), hbox, FALSE, FALSE, 0);
	frame = gtk_frame_new (_("Modem Settings"));
	edit = create_modem_edit ();
	gtk_object_set_data (GTK_OBJECT (druid_page), "modem_edit", edit);
	gtk_container_set_border_width (GTK_CONTAINER (edit->widget), GNOME_PAD_SMALL);
	gtk_container_add (GTK_CONTAINER (frame), edit->widget);
	gtk_box_pack_start (GTK_BOX (vbox2), frame, FALSE, FALSE, 0);
	gtk_widget_show_all (vbox2);
	return druid_page;

}


/* E - Modem select page */
static gboolean
modem_select_page_next_callback (GtkWidget *druid_page, GtkWidget *druid, gpointer data)
{
	/* now I need to find the right one. */
	/* Gosh, this is ugly */
	GtkWidget *menu_item;
	gchar section[10];
	gchar *old_device;
	const gchar *device = NULL;

	gint i;

	menu_item = (GtkWidget *) gtk_object_get_data (GTK_OBJECT (druid_page), "active_item");

	if (menu_item == NULL) {
		gtk_object_set_data (GTK_OBJECT (druid_flow.name_page), "back", NULL);
		return FALSE;
	}
	old_device = (gchar *) gtk_object_get_data (GTK_OBJECT (menu_item), "label");
	for (i = 0; i < 20; i++) {
		/* no one with more then 20 modems is using this tool, right... */
		g_snprintf (section, 9, "Modem%d", i);
		device = cfg->get(section, "Modem");
		if ((old_device == NULL) ||
		    (!strcmp (old_device, device)))
			break;
	}

	copy_sections (cfg, section, "Dialer Defaults");

	gtk_object_set_data (GTK_OBJECT (druid_flow.name_page), "back", NULL);
	return FALSE;
}

static void
menu_activate (GtkMenuItem *menuitem,
	       gpointer user_data)
{
	gtk_object_set_data (GTK_OBJECT (user_data), "active_item", menuitem);
}

static void
prepare_modem_select_page_callback (GtkWidget *druid_page, GtkWidget *druid, gpointer data)
{
	GtkWidget *menu_item;
	GtkWidget *omenu;
	GtkWidget *menu;
	gchar section[10];
	const gchar *device;
	gint i;

	omenu = GTK_WIDGET (gtk_object_get_data (GTK_OBJECT (druid_page), "omenu"));
	menu = gtk_option_menu_get_menu (GTK_OPTION_MENU (omenu));

	if (menu)
		gtk_widget_destroy (menu);

	menu = gtk_menu_new ();

	for (i = 0; i < 20; i++) {
		/* no one with more then 20 modems is using this tool, right... */
		g_snprintf (section, 9, "Modem%d", i);
		device = cfg->get(section, "Modem");
		if (device == NULL)
			break;

		menu_item = gtk_menu_item_new_with_label (device);
		gtk_object_set_data_full (GTK_OBJECT (menu_item), "label", g_strdup (device), (GtkDestroyNotify)g_free);
		if (i == 0) {
			gtk_object_set_data (GTK_OBJECT (druid_page), "active_item", menu_item);
		}
		gtk_signal_connect (GTK_OBJECT (menu_item), "activate",
				    GTK_SIGNAL_FUNC (menu_activate),
				    druid_page);
		gtk_widget_show (menu_item);
		gtk_menu_append (GTK_MENU (menu), menu_item);
	}
	gtk_option_menu_set_menu (GTK_OPTION_MENU (omenu), menu);
	gtk_option_menu_set_history (GTK_OPTION_MENU (omenu), 0);
	gtk_widget_show (menu);
}


static GtkWidget *
create_modem_select_page (GtkWidget* druid, Account *account)
{
	GtkWidget *druid_page;
        GtkWidget *toplevel;
	GtkWidget *align;
	GtkWidget *vbox, *vbox2;
	GtkWidget *hbox;
	GtkWidget *label;
	GtkWidget *omenu;
	GtkWidget *menu;

        toplevel = gtk_widget_get_toplevel(druid);

	druid_page = create_druid_standard (_("Select Modem"));
	druid_flow.modem_select_page = druid_page;

	vbox = GNOME_DRUID_PAGE_STANDARD (druid_page)->vbox;
	gtk_box_set_spacing (GTK_BOX (vbox), 0);
	gtk_container_set_border_width (GTK_CONTAINER (vbox), GNOME_PAD);
	gtk_signal_connect (GTK_OBJECT (druid_page), "next", GTK_SIGNAL_FUNC (modem_select_page_next_callback), account);
	gtk_signal_connect (GTK_OBJECT (druid_page), "prepare", GTK_SIGNAL_FUNC (prepare_modem_select_page_callback), account);
	gtk_signal_connect (GTK_OBJECT (druid_page), "back", GTK_SIGNAL_FUNC (generic_back_callback), NULL);

	align = gtk_alignment_new (0.0, 0.0, 0.0, 0.0);
	gtk_box_pack_start (GTK_BOX (vbox), align, FALSE, FALSE, 0);
	vbox2 = gtk_vbox_new (FALSE, GNOME_PAD_SMALL);
	gtk_container_add (GTK_CONTAINER (align), vbox2);
	label = gtk_label_new (_("There are several modems on your system.  Please choose one to use as a default when creating your dialup accounts:"));
	gtk_label_set_line_wrap (GTK_LABEL (label), TRUE);
	gtk_label_set_justify (GTK_LABEL (label), GTK_JUSTIFY_LEFT);
	gtk_box_pack_start (GTK_BOX (vbox2), label, FALSE, FALSE, 0);


	hbox = gtk_hbox_new (FALSE, GNOME_PAD_SMALL);
	label = gtk_label_new (_("Devices"));
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);

	omenu = gtk_option_menu_new ();
	gtk_object_set_data (GTK_OBJECT (druid_page), "omenu", omenu);
	gtk_box_pack_start (GTK_BOX (hbox), omenu, TRUE, TRUE, 0);
	gtk_box_pack_start (GTK_BOX (vbox2), hbox, FALSE, FALSE, 0);

	gtk_widget_show_all (vbox2);
	return druid_page;
}

/* Name page */
static void
name_page_prepare_callback (GtkWidget *widget, GtkWidget *druid, gpointer data)
{
	GtkWidget *entry;

	entry = GTK_WIDGET (gtk_object_get_data (GTK_OBJECT (widget), "name"));
	gtk_widget_grab_focus (entry);
}

/* Name page */
static gboolean
name_page_next_callback (GtkWidget *widget, GtkWidget *druid, gpointer data)
{
	GtkWidget *entry;
	gchar *text;
	gchar *dialer_text;
	gchar *temp;
	Account *account = (Account *)data;
	PhoneNumber *number = account->number;

	entry = GTK_WIDGET (gtk_object_get_data (GTK_OBJECT (widget), "name"));
	text = gtk_entry_get_text (GTK_ENTRY (entry));
	if (text == NULL || *text == '\000') {
		warning_dialog (_("You must enter a name for your connection."), GTK_WINDOW (gtk_widget_get_toplevel (druid)));
		return TRUE;
	}
	/* Check against duplicate entries */
	account_set_name (account, text);

	dialer_text = g_strdup_printf ("Dialer %s", text);
	if ((*cfg)[dialer_text] != NULL) {
		temp = g_strdup_printf (_("You already have an account with the name %s.\n\nPlease enter a different name for your connection."), text);
		warning_dialog (temp, GTK_WINDOW (gtk_widget_get_toplevel (druid)));
		g_free (temp);
		g_free (dialer_text);
		return TRUE;
	}
	g_free (dialer_text);

	entry = GTK_WIDGET (gtk_object_get_data (GTK_OBJECT (widget), "prefix"));
	text = gtk_entry_get_text (GTK_ENTRY (entry));
	if (!valid_phone_number_string (text)) {
		temp = g_strdup_printf (_("The number\n\"%s\"\nis not a valid number for your prefix."), text);
		warning_dialog (temp, GTK_WINDOW (gtk_widget_get_toplevel (druid)));
		g_free (temp);
		return 1;
	}
	phone_number_set_prefix (number, text);

	entry = GTK_WIDGET (gtk_object_get_data (GTK_OBJECT (widget), "area_code"));
	text = gtk_entry_get_text (GTK_ENTRY (entry));
	if (!valid_phone_number_string (text)) {
		temp = g_strdup_printf (_("The number\n\"%s\"\nis not a valid number for your area code."), text);
		warning_dialog (temp, GTK_WINDOW (gtk_widget_get_toplevel (druid)));
		g_free (temp);
		return 1;
	}
	phone_number_set_areacode (number, text);

	entry = GTK_WIDGET (gtk_object_get_data (GTK_OBJECT (widget), "number"));
	text = gtk_entry_get_text (GTK_ENTRY (entry));

	if (text == NULL || *text == '\000') {
	        warning_dialog (_("You must enter your connection's phone-number."), GTK_WINDOW (gtk_widget_get_toplevel (druid)));
		return 1;
	}

	if (!valid_phone_number_string (text)) {
		temp = g_strdup_printf (_("The number\n\"%s\"\nis not a valid number for your connection's phone-number."), text);
		warning_dialog (temp, GTK_WINDOW (gtk_widget_get_toplevel (druid)));
		g_free (temp);
		return 1;
	}
	phone_number_set_number (number, text);

	return 0;
}

static GtkWidget *
create_name_page (GtkWidget* druid, Account *account)
{
	GtkWidget *druid_page;
	GtkWidget *vbox, *vbox2;
	GtkWidget *hbox;
	GtkWidget *entry;
	GtkWidget *label;
	GtkWidget *align;
        GtkWidget *toplevel;

        toplevel = gtk_widget_get_toplevel(druid);

	druid_page = create_druid_standard (_("Phone number and name"));
	druid_flow.name_page = druid_page;

	vbox = GNOME_DRUID_PAGE_STANDARD (druid_page)->vbox;
	gtk_box_set_spacing (GTK_BOX (vbox), GNOME_PAD_BIG);
	gtk_container_set_border_width (GTK_CONTAINER (vbox), GNOME_PAD);
	gtk_signal_connect (GTK_OBJECT (druid_page), "next", (GtkSignalFunc) name_page_next_callback, account);
	gtk_signal_connect (GTK_OBJECT (druid_page), "prepare", (GtkSignalFunc) name_page_prepare_callback, account);
	gtk_signal_connect (GTK_OBJECT (druid_page), "back", GTK_SIGNAL_FUNC (generic_back_callback), NULL);

	/* name */
	align = gtk_alignment_new (0.0, 0.0, 0.0, 0.0);
	vbox2 = gtk_vbox_new (FALSE, GNOME_PAD_SMALL);
	gtk_box_pack_start (GTK_BOX (vbox), align, FALSE, FALSE, 0);
	gtk_container_add (GTK_CONTAINER (align), vbox2);
	hbox = gtk_hbox_new (FALSE, 0);
	label = gtk_label_new (_("Select a name for this internet connection."));
	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
	gtk_box_pack_start (GTK_BOX (hbox), label, TRUE, TRUE, 0);
	gtk_box_pack_start (GTK_BOX (vbox2), hbox, FALSE, FALSE, 0);

	label = gtk_label_new (_("Account name:"));
	hbox = gtk_hbox_new (FALSE, GNOME_PAD_SMALL);
	gtk_box_pack_start (GTK_BOX (hbox), label, TRUE, FALSE, 0);
	gtk_box_pack_start (GTK_BOX (vbox2), hbox, FALSE, FALSE, 0);
	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
	entry = gtk_entry_new ();
        rp3_editable_enters(GTK_WINDOW(toplevel), GTK_EDITABLE(entry));
	gtk_object_set_data (GTK_OBJECT (druid_page), "name", entry);
	gnome_widget_add_help (entry, _("Write a short description for your connection here.  This is what you'll refer to it by in the main dialog window."));
	rp3_set_tooltip (entry, _("Write a short description for your connection here.  This is what you'll refer to it by in the main dialog window."));
	gtk_box_pack_start (GTK_BOX (hbox), entry, FALSE, FALSE, 0);
	gtk_box_pack_start (GTK_BOX (vbox), gtk_hseparator_new (), FALSE, FALSE, 0);

	/* rank */
	align = gtk_alignment_new (0.0, 0.0, 0.0, 0.0);
	vbox2 = gtk_vbox_new (FALSE, GNOME_PAD_SMALL);
	gtk_box_pack_start (GTK_BOX (vbox), align, FALSE, FALSE, 0);
	gtk_container_add (GTK_CONTAINER (align), vbox2);
	hbox = gtk_hbox_new (FALSE, 0);
	label = gtk_label_new (_("Enter the internet provider's phone number.\nThe prefix and area-code are optional."));
	gtk_label_set_justify (GTK_LABEL (label), GTK_JUSTIFY_LEFT);
	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
	gtk_box_pack_start (GTK_BOX (hbox), label, TRUE, TRUE, 0);
	gtk_box_pack_start (GTK_BOX (vbox2), hbox, FALSE, FALSE, 0);

	hbox = gtk_hbox_new (FALSE, GNOME_PAD_SMALL);
	gtk_box_pack_start (GTK_BOX (vbox2), hbox, FALSE, FALSE, 0);
	label = gtk_label_new (_("Prefix"));
	gtk_box_pack_start (GTK_BOX (hbox), label, TRUE, FALSE, 0);
	entry = gtk_entry_new ();
        rp3_editable_enters(GTK_WINDOW(toplevel), GTK_EDITABLE(entry));
	gtk_object_set_data (GTK_OBJECT (druid_page), "prefix", entry);
	gtk_widget_set_usize (entry, 35, -1);
	gnome_widget_add_help (entry, _("Enter any numbers you need to dial before the others.  For example, if you need to reach an outside line, or disable call waiting, put the necessary numbers here."));
	rp3_set_tooltip (entry, _("Enter any numbers you need to dial before the others.  For example, if you need to reach an outside line, or disable call waiting, put the necessary numbers here."));
	gtk_box_pack_start (GTK_BOX (hbox), entry, FALSE, FALSE, 0);
	label = gtk_label_new (_(" Area/Country code"));
	gtk_box_pack_start (GTK_BOX (hbox), label, TRUE, FALSE, 0);
	entry = gtk_entry_new ();
        rp3_editable_enters(GTK_WINDOW(toplevel), GTK_EDITABLE(entry));
	gtk_object_set_data (GTK_OBJECT (druid_page), "area_code", entry);
	gtk_widget_set_usize (entry, 50, -1);
	gnome_widget_add_help (entry, _("Enter the area/city/country code needed to reach your internet provider"));
	rp3_set_tooltip (entry, _("Enter the area/city/country code needed to reach your internet provider"));
	gtk_box_pack_start (GTK_BOX (hbox), entry, FALSE, FALSE, 0);

	label = gtk_label_new (_(" Phone number"));
	gtk_box_pack_start (GTK_BOX (hbox), label, TRUE, FALSE, 0);
	entry = gtk_entry_new ();
        rp3_editable_enters(GTK_WINDOW(toplevel), GTK_EDITABLE(entry));
	gtk_object_set_data (GTK_OBJECT (druid_page), "number", entry);
	gnome_widget_add_help (entry, _("Enter the local phone number for your internet provider here"));
	rp3_set_tooltip (entry, _("Enter the local phone number for your internet provider here"));
	gtk_box_pack_start (GTK_BOX (hbox), entry, FALSE, FALSE, 0);

	return druid_page;
}

static gint
password_page_next_callback (GtkWidget *druid_page, GtkWidget *druid, gpointer data)
{
	GtkWidget *entry;
	gchar *text;
	gchar *temp;
	Account *account = (Account *)data;

	entry = GTK_WIDGET (gtk_object_get_data (GTK_OBJECT (druid_page), "user_name"));
	text = gtk_entry_get_text (GTK_ENTRY (entry));
	if (text == NULL || *text == '\000') {
		warning_dialog (_("You must enter a user name for your connection."), GTK_WINDOW (gtk_widget_get_toplevel (druid)));
		return 1;
	}

        account_set_username (account, text);
	entry = GTK_WIDGET (gtk_object_get_data (GTK_OBJECT (druid_page), "password"));
	text = gtk_entry_get_text (GTK_ENTRY (entry));
	if (text == NULL || *text == '\000') {
		warning_dialog (_("You must enter a password for your connection."), GTK_WINDOW (gtk_widget_get_toplevel (druid)));
		return 1;
	}

        account_set_password (account, text);
	return 0;
}

static void
password_page_prepare_callback (GtkWidget *druid_page, GtkWidget *druid, gpointer data)
{
	GtkWidget *entry;

	entry = GTK_WIDGET (gtk_object_get_data (GTK_OBJECT (druid_page), "user_name"));
	gtk_widget_grab_focus (entry);
}

static GtkWidget *
create_password_page (GtkWidget* druid, Account *account)
{
	GtkWidget *druid_page;
	GtkWidget *vbox, *vbox2;
	GtkWidget *hbox;
	GtkWidget *entry;
	GtkWidget *label;
	GtkWidget *align;
	GtkWidget *table;
        GtkWidget *toplevel;

        toplevel = gtk_widget_get_toplevel(druid);

	druid_page = create_druid_standard (_("User name and password"));
	gtk_signal_connect (GTK_OBJECT (druid_page), "prepare", (GtkSignalFunc) password_page_prepare_callback, account);
	gtk_signal_connect (GTK_OBJECT (druid_page), "next", (GtkSignalFunc) password_page_next_callback, account);

	vbox = GNOME_DRUID_PAGE_STANDARD (druid_page)->vbox;
	gtk_box_set_spacing (GTK_BOX (vbox), GNOME_PAD_BIG);
	gtk_container_set_border_width (GTK_CONTAINER (vbox), GNOME_PAD);
	table = gtk_table_new (5, 2, FALSE);
	gtk_table_set_row_spacings (GTK_TABLE (table), GNOME_PAD_SMALL);
	gtk_table_set_col_spacings (GTK_TABLE (table), GNOME_PAD);

	/* user name */
	align = gtk_alignment_new (0.0, 0.0, 0.0, 0.0);
	vbox2 = gtk_vbox_new (FALSE, GNOME_PAD_SMALL);
	gtk_box_pack_start (GTK_BOX (vbox), align, FALSE, FALSE, 0);
	gtk_container_add (GTK_CONTAINER (align), vbox2);

	label = gtk_label_new (_("Enter the user name for this account."));
	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
	gtk_table_attach (GTK_TABLE (table), label,
			  0, 2, 0, 1, GTK_FILL, GTK_FILL, 0, 0);

	gtk_box_pack_start (GTK_BOX (vbox2), table, FALSE, FALSE, 0);
	label = gtk_label_new (_("User name:"));
	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
	gtk_table_attach (GTK_TABLE (table), label,
			  0, 1, 1, 2, GTK_FILL, GTK_FILL, 0, 0);

	entry = gtk_entry_new ();
        rp3_editable_enters(GTK_WINDOW(toplevel), GTK_EDITABLE(entry));
	gtk_object_set_data (GTK_OBJECT (druid_page), "user_name", entry);
	gnome_widget_add_help (entry, _("Enter the user name that your internet provider gave you here."));
	rp3_set_tooltip (entry, _("Enter the user name that your internet provider gave you here."));
	gtk_table_attach (GTK_TABLE (table), entry,
			  1, 2, 1, 2, GTK_FILL, GTK_FILL, 0, 0);

	/* Gratuituous spacing */
	hbox = gtk_hbox_new (FALSE, 0);
	gtk_container_set_border_width (GTK_CONTAINER (hbox), GNOME_PAD);
	gtk_table_attach (GTK_TABLE (table), hbox,
			  0, 2, 2, 3, GTK_FILL, GTK_FILL, 0, 0);

	label = gtk_label_new (_("Enter the password for this account."));
	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
	gtk_table_attach (GTK_TABLE (table), label,
			  0, 2, 3, 4, GTK_FILL, GTK_FILL, 0, 0);

	label = gtk_label_new (_("Password:"));
	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
	gtk_table_attach (GTK_TABLE (table), label,
			  0, 1, 4, 5, GTK_FILL, GTK_FILL, 0, 0);

	entry = gtk_entry_new ();
        rp3_editable_enters(GTK_WINDOW(toplevel), GTK_EDITABLE(entry));
	gtk_entry_set_visibility (GTK_ENTRY (entry), FALSE);
	gtk_object_set_data (GTK_OBJECT (druid_page), "password", entry);
	gnome_widget_add_help (entry, _("Enter the password for your account.  If you do not know it, please contact your Internet service provider."));
	rp3_set_tooltip (entry, _("Enter the password for your account.  If you do not know it, please contact your Internet service provider."));
	gtk_table_attach (GTK_TABLE (table), entry,
			  1, 2, 4, 5, GTK_FILL, GTK_FILL, 0, 0);

	return druid_page;
}

static int
special_page_next_callback (GtkWidget *druid_page, GtkWidget *druid, gpointer data)
{
	GtkWidget *clist;
	Account *account = (Account *)data;

	clist = GTK_WIDGET (gtk_object_get_data (GTK_OBJECT (druid_page), "clist"));

	if ((GTK_CLIST (clist)->selection) && (GPOINTER_TO_INT (GTK_CLIST (clist)->selection->data) != 0))
		account->stupidmode = TRUE;
	else
		account->stupidmode = FALSE;
	return 0;
}

static GtkWidget *
create_special_page (GtkWidget* druid, Account *account)
{
	GtkWidget *druid_page;
	GtkWidget *label;
	GtkWidget *vbox, *vbox2;
	GtkWidget *hbox;
	GtkWidget *clist;
	GtkWidget *align;
	GtkWidget *frame;
	gchar *text[1];
	gint row;
	
	druid_page = create_druid_standard (_("Other Options"));
	gtk_signal_connect (GTK_OBJECT (druid_page), "next", (GtkSignalFunc) special_page_next_callback, account);
	vbox = GNOME_DRUID_PAGE_STANDARD (druid_page)->vbox;
	gtk_container_set_border_width (GTK_CONTAINER (vbox), GNOME_PAD);

	align = gtk_alignment_new (0.0, 0.0, 0.0, 0.0);
	vbox2 = gtk_vbox_new (FALSE, GNOME_PAD);
	gtk_box_pack_start (GTK_BOX (vbox), align, FALSE, FALSE, 0);
	gtk_container_add (GTK_CONTAINER (align), vbox2);
	hbox = gtk_hbox_new (FALSE, 0);
	label = gtk_label_new (_("If your internet provider is listed below, please select it.  If you don't see yours listed, \"Normal ISP\" is fine."));
	gtk_label_set_justify (GTK_LABEL (label), GTK_JUSTIFY_LEFT);
	gtk_label_set_line_wrap (GTK_LABEL (label), TRUE);
	gtk_box_pack_start (GTK_BOX (hbox), label, TRUE, TRUE, 0);
	gtk_box_pack_start (GTK_BOX (vbox2), hbox, FALSE, FALSE, 0);

	frame = gtk_frame_new (_("Account List"));
	clist = gtk_clist_new (1);
	gtk_object_set_data (GTK_OBJECT (druid_page), "clist", clist);
	gtk_clist_set_selection_mode (GTK_CLIST (clist), GTK_SELECTION_BROWSE);
	text[0] = _("AT&T Global Network Services");
	gtk_clist_insert (GTK_CLIST (clist), 0, text);

	text[0] = _("Normal ISP");
	gtk_clist_insert (GTK_CLIST (clist), 0, text);
	gtk_clist_select_row (GTK_CLIST (clist), 0, 0);
	gtk_box_pack_start (GTK_BOX (vbox2), frame, FALSE, FALSE, 0);
	hbox = gtk_hbox_new (FALSE, 0);
	gtk_container_add (GTK_CONTAINER (frame), hbox);
	gtk_container_set_border_width (GTK_CONTAINER (hbox), GNOME_PAD_SMALL);
	gtk_box_pack_start (GTK_BOX (hbox), clist, TRUE, TRUE, 0);
	return druid_page;
}

static void
finish_prepare_callback (GtkWidget *druid_page, GtkWidget *druid, gpointer data)
{
	gchar *text;
	Account *account = (Account *)data;

	text = g_strdup_printf (_("You have selected the following information:\n\n"\
				  "     Account name: %s\n"\
				  "     User name: %s\n"\
				  "     Phone number: %s\n\n\n"\
				  "Press \"Finish\" to create this account."),
				account->name,
				account->username,
				account->number->complete);

	gnome_druid_page_finish_set_text (GNOME_DRUID_PAGE_FINISH (druid_page),
					  text);
}

static void
finish_finish_callback (GtkWidget *druid_page, GtkWidget *druid, gpointer data)
{
	const gchar *text;
	Account *account = (Account *)data;
        GtkWidget* mainwin;
	account_set_modem_sect(account, "Dialer Defaults");

        mainwin = GTK_WIDGET(gtk_object_get_data(GTK_OBJECT(druid), "mainwin"));

        gtk_widget_destroy(gtk_widget_get_toplevel (druid));
	delete (cfg);

	rp3_mainwin_add_account(mainwin, account);
}

static GtkWidget *
create_finish_page (Account *account)
{
	GtkWidget *druid_page = NULL;
	GdkColor color;
	GnomeCanvasItem *item;
	static GdkImlibImage *image = NULL;

	if (image == NULL)
		image = gdk_imlib_load_image ("/usr/share/pixmaps/rp3/rp3-top.png");

	druid_page = gnome_druid_page_finish_new_with_vals (_("Create the account"), NULL, logo, watermark);
	gtk_signal_connect (GTK_OBJECT (druid_page), "prepare", GTK_SIGNAL_FUNC (finish_prepare_callback), account);
	gtk_signal_connect_after (GTK_OBJECT (druid_page), "finish", GTK_SIGNAL_FUNC (finish_finish_callback), account);
	color.red = 71*255;
	color.green = 107*255;
	color.blue = 179*255;
	gnome_druid_page_finish_set_bg_color (GNOME_DRUID_PAGE_FINISH (druid_page),
					      &color);

	item = gnome_canvas_item_new (gnome_canvas_root (GNOME_CANVAS (GNOME_DRUID_PAGE_FINISH (druid_page)->canvas)),
				      gnome_canvas_image_get_type (),
				      "image", image,
				      "x", 0.0,
				      "y", 0.0,
				      "anchor", GTK_ANCHOR_NORTH_WEST,
				      "width", (gfloat) 462,
				      "height", (gfloat) 67,
				      NULL);
	gnome_canvas_item_raise_to_top (GNOME_DRUID_PAGE_FINISH (druid_page)->title_item);
	return druid_page;
}

GtkWidget*
add_druid_new (GtkWidget* mainwin)
{
	GtkWidget *window;
	GtkWidget *druid;
	GtkWidget *druid_page;
	Account *account;

	window = gtk_window_new (GTK_WINDOW_DIALOG);
        gtk_window_set_title(GTK_WINDOW(window), _("Add New Internet Connection"));
	druid = gnome_druid_new ();
	gtk_container_add (GTK_CONTAINER (window), druid);

        gtk_object_set_data(GTK_OBJECT(druid), "mainwin", mainwin);

	cfg = new WvConf ("/etc/wvdial.conf");

	if (logo == NULL)
		logo = gdk_imlib_load_image ("/usr/share/pixmaps/redhat/shadowman-48.png");

	if (watermark == NULL)
		watermark = gdk_imlib_load_image ("/usr/share/pixmaps/rp3/rp3-left.png");

	/* Create the account */
 	account = account_new (NULL);

	druid_page = create_intro_page (account);
	gnome_druid_append_page (GNOME_DRUID (druid),
				 GNOME_DRUID_PAGE (druid_page));
	gnome_druid_set_page (GNOME_DRUID (druid),
			      GNOME_DRUID_PAGE (druid_page));

	druid_page = create_modem_warning_page (druid, account);
	gnome_druid_append_page (GNOME_DRUID (druid),
				 GNOME_DRUID_PAGE (druid_page));

	druid_page = create_modem_setup_page (druid, account);
	gnome_druid_append_page (GNOME_DRUID (druid),
				 GNOME_DRUID_PAGE (druid_page));

	druid_page = create_add_extra_modem_page (druid, account);
	gnome_druid_append_page (GNOME_DRUID (druid),
				 GNOME_DRUID_PAGE (druid_page));

	druid_page = create_manual_modem_page (druid, account);
	gnome_druid_append_page (GNOME_DRUID (druid),
				 GNOME_DRUID_PAGE (druid_page));

	druid_page = create_modem_select_page (druid, account);
	gnome_druid_append_page (GNOME_DRUID (druid),
				 GNOME_DRUID_PAGE (druid_page));

	druid_page = create_name_page (druid, account);
	gnome_druid_append_page (GNOME_DRUID (druid),
				 GNOME_DRUID_PAGE (druid_page));

	druid_page = create_password_page (druid, account);
	gnome_druid_append_page (GNOME_DRUID (druid),
				 GNOME_DRUID_PAGE (druid_page));

	druid_page = create_special_page (druid, account);
	gnome_druid_append_page (GNOME_DRUID (druid),
				 GNOME_DRUID_PAGE (druid_page));

	druid_page = create_finish_page (account);
	gnome_druid_append_page (GNOME_DRUID (druid),
				 GNOME_DRUID_PAGE (druid_page));

	gtk_widget_show_all (druid);
	gtk_signal_connect (GTK_OBJECT (druid), "cancel", (GtkSignalFunc) druid_cancel_callback, window);
        return window;
}

