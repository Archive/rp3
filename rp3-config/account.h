/* account.h
 *
 * Copyright (C) 1999 Red Hat, Inc.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef RP3_ACCOUNT_H
#define RP3_ACCOUNT_H

struct _interface;

typedef struct _PhoneNumber PhoneNumber;

struct _PhoneNumber {
  gchar* dial_prefix; /* call waiting, etc. */
  gchar* area_code;   /* country code, area code */
  gchar* number;      /* local number */
  gchar* complete;    /* everything concatenated, always non-NULL */
};

PhoneNumber*  phone_number_new(void);
void          phone_number_destroy(PhoneNumber* pn);
void          phone_number_set_prefix(PhoneNumber* pn, const gchar* prefix);
void          phone_number_set_areacode(PhoneNumber* pn, const gchar* ac);
void          phone_number_set_number(PhoneNumber* pn, const gchar* n);

gboolean      valid_phone_number_string(const gchar* str);

typedef enum {
  AccountUnknown,
  AccountDown,
  AccountUp
} AccountStatus;

typedef struct _Account Account;

struct _Account {
  gchar* name;
  gchar* username;
  gchar* password;
  gchar* modem_sect;
  PhoneNumber* number;
  AccountStatus status;
  gchar* wvdial_sect;
  gboolean stupidmode;
  gboolean userctl;
  gboolean onboot;
  gboolean persist;
  struct _interface* interface;
};

Account* account_new       (struct _interface* interface);
void     account_set_name  (Account* a, const gchar* name);
void     account_set_username(Account* a, const gchar* name);
void     account_set_password(Account* a, const gchar* pw);
void     account_set_modem_sect(Account* a, const gchar* m);
void     account_set_stupidmode(Account* a, gboolean s);
/* doesn't copy the PhoneNumber */
void     account_set_number(Account* a, PhoneNumber* number);
void     account_destroy   (Account* a);
Account* account_clone     (Account* a, GSList *accounts);
Account* account_copy      (Account* a, GSList *accounts);

void     account_dial      (Account* a);
void     account_hangup    (Account* a);
gboolean account_save      (Account* a); /* true on success */
void     account_update    (Account* a);
GSList*  load_accounts     (void);

#endif


