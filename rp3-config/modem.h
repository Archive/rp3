/* modem.h
 *
 * Copyright (C) 1999 Red Hat, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef __MODEM_H__
#define __MODEM_H__
#include <gnome.h>
#include <wvconf.h>

typedef enum {
	CONF_NONEXISTENT,
	CONF_MODEM_FOUND,  /* No Default */
	CONF_MODEMS_FOUND, /* No Default, 2+ modems */
	CONF_MODEM_DEFAULT_FOUND, /* Default set, 1 modem */
	CONF_MODEMS_DEFAULT_FOUND /* Default set, 2+ modems */
} ConfStatus;

typedef struct _Modem Modem;
struct _Modem
{
	gchar *device;
	gchar *speed;
	gchar *section;
};

ConfStatus check_conf (WvConf *cfg);
void       create_modem_dialog (GtkWidget *parent, gchar *section);
/*copy section1 -> section2 */
void       copy_sections (WvConf *cfg, gchar *section1, gchar *section2);
GSList    *load_modems ();
void       modem_save (Modem *modem);
void       modem_destroy (Modem *modem);
#endif
