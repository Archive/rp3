/*
 * forkexec.c
 *
 * contains a function to do a fork, exec, wait without using
 * system() -- we want the PPID of the child process to be the
 * process that calls fork_exec()
 *
 * Copyright 1999 Red Hat, Inc.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

#include <stdio.h>
#include <sys/types.h>
#include <sys/resource.h>
#include <sys/wait.h>
#include <unistd.h>

#include "forkexec.h"

/* lighter and simpler than system() */
int
fork_exec(int wait, char *path, char *arg)
{
    pid_t child;
    int status;

    if (!(child = fork())) {
	/* child */
	execl(path, path, arg, 0);
	perror(path);
	/* Can't call exit(), because it flushes the X protocol stream */
	_exit (1);
    }

    if (!wait) return child;

    wait4 (child, &status, 0, NULL);
    if (WIFEXITED(status) && (WEXITSTATUS(status) == 0)) {
	return WEXITSTATUS(status);
    }
    return -1;
}

