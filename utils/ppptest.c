
#include <gtk/gtk.h>

#include "gtknetmon.h"

#include <stdlib.h>
#include <string.h>

static const guint tputs[] = 
{ 0, 1, 2, 3, 2, 1, 5, 3, 2, 7, 10, 4, 7, 15, 17, 12, 14, 5, 6, 9, 9, 9, 7, 6, 
  5, 4, 2, 1, 0, 0, 0, 0, 20, 4, 5, 6, 7, 9, 0, 12, 14, 12, 8, 3, 2, 1 };

static const guint n_tputs = sizeof(tputs)/sizeof(tputs[0]);
static guint sofar = 0;

static guint* 
next_throughput()
{
  guint* retval;
  guint count = 0;
  guint i = 0;

  retval = g_malloc(sizeof(guint)*sofar*2); /* *2 avoids segv in stupid test program not worth debugging */

  count = sofar / n_tputs;

  while (i < count)
    {
      memcpy(&retval[i*n_tputs], tputs, sizeof(tputs));
      ++i;
    }

  memcpy(&retval[i*n_tputs], tputs, sizeof(guint)*(sofar%n_tputs));

  ++sofar;

  return retval;
}

static gint 
delete_event_cb(GtkWidget* win, GdkEventAny* de, gpointer data)
{
  gtk_main_quit();
  return TRUE;
}

static gint
online_timeout(GtkNetmon* netmon)
{
  static int cost = 0;
  static char buf[64];
  gint dollars, cents;
  guint* tp;

  gtk_netmon_set_seconds_online(netmon, netmon->seconds_online + 1);
 
  /* Not localized right, don't copy. :-) */
  dollars = cost/100;
  cents = cost%100;

  g_snprintf(buf, 64, "$%d.%s%d", dollars, cents > 9 ? "" : "0", cents);

  gtk_netmon_set_cost(netmon, buf);

  ++cost;

  tp = next_throughput();

  gtk_netmon_set_throughput(netmon, tp, tp, sofar);

  g_free(tp);

  gtk_netmon_set_speed(netmon, "300 bps");
  gtk_netmon_set_transmitting(netmon, (rand()/(RAND_MAX/100)) > 50); 
  gtk_netmon_set_receiving(netmon, (rand()/(RAND_MAX/100)) > 50); 

  return TRUE;
}


static void
toggle_online(GtkNetmon* netmon)
{
  static guint timeout_id = 0;

  gtk_netmon_set_online(netmon, !netmon->online);

  if (netmon->online)
    {
      g_assert(timeout_id == 0);
      timeout_id = gtk_timeout_add(1000, (GtkFunction)online_timeout, netmon);
    }
  else
    {
      if (timeout_id != 0)
        gtk_timeout_remove(timeout_id);
      timeout_id = 0;
    }
}

static void 
connect_cb(GtkWidget* button, GtkWidget* window);

static GtkWidget* 
connect_button(GtkWidget* win)
{
  GtkWidget* button;

  button = gtk_button_new_with_label("Connect");
  gtk_widget_set_usize (button, 48, 48);
  gtk_signal_connect(GTK_OBJECT(button), "clicked",
                     connect_cb, win);

  gtk_widget_show(button);

  return button;
}

static void disconnect_cb(GtkWidget* netmon, GtkWidget* window)
{
  GtkWidget* button;

  toggle_online(GTK_NETMON(netmon));

  gtk_container_remove(GTK_CONTAINER(window), netmon);

  button = connect_button(window);

  gtk_container_add(GTK_CONTAINER(window), button);
}

static void 
connect_cb(GtkWidget* button, GtkWidget* window)
{
  GtkWidget* netmon = gtk_netmon_new(TRUE);

#if 0
  gtk_signal_connect(GTK_OBJECT(netmon), "disconnect",
                     GTK_SIGNAL_FUNC(disconnect_cb),
                     window);
#endif
  
  gtk_netmon_set_has_disconnect(GTK_NETMON(netmon), TRUE);

  gtk_netmon_set_orientation (GTK_NETMON (netmon), GTK_NETMON_VERTICAL);
  gtk_netmon_set_size (GTK_NETMON (netmon), 24);
  
  toggle_online(GTK_NETMON(netmon));

  gtk_container_remove(GTK_CONTAINER(window), button);

  gtk_widget_show(netmon);
  gtk_container_add(GTK_CONTAINER(window), netmon);
}

int
main(int argc, char** argv)
{
  GtkWidget* win;
  GtkWidget* button;

  gtk_init(&argc, &argv);

  win = gtk_window_new(GTK_WINDOW_TOPLEVEL);

  button = connect_button(win);

  gtk_container_add(GTK_CONTAINER(win), button);

  gtk_signal_connect(GTK_OBJECT(win), "delete_event", 
                     GTK_SIGNAL_FUNC(delete_event_cb), 
                     NULL);
    
  gtk_widget_show_all(win);

  gtk_main();

  return 0;
}
