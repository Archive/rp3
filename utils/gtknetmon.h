/*
 * NETMON mini-display widget
 *
 * Developed by Havoc Pennington <hp@pobox.com>
 *
 * Copyright (C) 1999 Red Hat, Inc. GNU General Public License
 */

#ifndef INC_GTK_NETMON_H
#define INC_GTK_NETMON_H

#include <gtk/gtkwidget.h>
#include <gtk/gtkbin.h>
#include <gtk/gtktooltips.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

typedef enum {
  GTK_NETMON_COST,
  GTK_NETMON_TIME
} GtkNetmonDisplayStyle;

typedef enum {
  GTK_NETMON_VERTICAL,
  GTK_NETMON_HORIZONTAL
} GtkNetmonOrientation;

#define GTK_NETMON(obj)          GTK_CHECK_CAST (obj, gtk_netmon_get_type (), GtkNetmon)
#define GTK_NETMON_CLASS(klass)  GTK_CHECK_CLASS_CAST (klass, gtk_netmon_get_type (), GtkNetmonClass)
#define GTK_IS_NETMON(obj)       GTK_CHECK_TYPE (obj, gtk_netmon_get_type ())

typedef struct _GtkNetmon       GtkNetmon;
typedef struct _GtkNetmonClass  GtkNetmonClass;

struct _GtkNetmon
{
  GtkBin bin;
  
  /*< private >*/

  guint seconds_online;
  gchar* cost;
  gchar* speed;
  guint* rxs_tput;
  guint* txs_tput;
  guint throughput_count;
  
  GtkNetmonDisplayStyle style;

  GdkFont* small_font;

  GdkColor red;
  GdkColor blue;

  GdkGC* red_gc;
  GdkGC* blue_gc;

  GdkPixmap* red_led_pixmap;
  GdkBitmap* red_led_mask;
  
  GdkPixmap* blue_led_pixmap;
  GdkPixmap* blue_led_mask;

  GdkPixmap* red_empty_led_pixmap;
  GdkBitmap* red_empty_led_mask;

  GdkPixmap* blue_empty_led_pixmap;
  GdkBitmap* blue_empty_led_mask;
  
  GdkPixmap* connected_led_pixmap;
  GdkPixmap* connected_led_mask;

  GdkPixmap* disconnected_led_pixmap;
  GdkPixmap* disconnected_led_mask;
  
  GdkPixmap* buffer;

  GdkRectangle graph_rect;
  GdkRectangle tx_rect;
  GdkRectangle rx_rect;
  GdkRectangle hangup_rect;
  GdkRectangle timecost_rect;
  GdkRectangle speed_rect;

  GtkNetmonOrientation orientation;
  gint panel_size;

  GtkTooltips *button_tip;
  
  /* keep all flags at the end to minimize padding */
  guint online : 1;
  guint tx     : 1;
  guint rx     : 1;
  guint disconnect_button : 1;
};

struct _GtkNetmonClass
{
  GtkBinClass parent_class;

  void (*clicked) (GtkNetmon* netmon);
};

guint           gtk_netmon_get_type           (void);
GtkWidget*      gtk_netmon_new                (gboolean with_disconnect);

void            gtk_netmon_set_seconds_online (GtkNetmon* netmon, guint secs);
/* Can set cost string to NULL */
void            gtk_netmon_set_cost           (GtkNetmon* netmon, const gchar* cost);
void            gtk_netmon_set_speed          (GtkNetmon* netmon, const gchar* speed);
/* args can be NULL, NULL, 0 for none */
/* the arrays should have the latest information at the end of the array */
void            gtk_netmon_set_throughput     (GtkNetmon* netmon, 
                                               const guint* rxs, 
                                               const guint* txs,
                                               guint count);

void            gtk_netmon_set_online         (GtkNetmon* netmon, gboolean online);
void            gtk_netmon_set_transmitting   (GtkNetmon* netmon, gboolean tx);
void            gtk_netmon_set_receiving      (GtkNetmon* netmon, gboolean rx);
void            gtk_netmon_set_display_style  (GtkNetmon* netmon, GtkNetmonDisplayStyle style);
void            gtk_netmon_set_has_disconnect (GtkNetmon* netmon, gboolean setting);

void            gtk_netmon_set_orientation    (GtkNetmon* netmon, GtkNetmonOrientation orientation);
void            gtk_netmon_set_size           (GtkNetmon* netmon, gint size);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __GTK_NETMON_H__ */
