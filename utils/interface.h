/* Red Hat PPP Dialer applet/application
 * Copyright 1999 Red Hat, Inc.
 *
 * Author: Michael K. Johnson <johnsonm@redhat.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Cambridge, MA 
 * 02139, USA.
 *
 */

#ifndef _INTERFACE_H
#define _INTERFACE_H

#ifdef __cplusplus
extern "C" {
#endif

#include <time.h>
#include <glib.h>

#include "shvar.h"

typedef enum {
    INTERFACE_UNKNOWN = 0,
    INTERFACE_DOWN = 1,
    INTERFACE_UP = 2
} interfaceStatus;

#define INTERFACE_TPUT_READINGS 1000

typedef struct _interface interface;
struct _interface {
    char		*interfaceName;		/* includes clone part */
    char		*physicalName;		/* true physical device name */
    char		*logicalName;		/* ifcfg-<logicalName> (no clone extention) */
    char		*friendlyName;		/* NAME="whatever" */
    GSList		*cloneList;
    shvarFile		*ifcfg;
    gboolean		manipulable;		/* !getuid() || USERCTL */
    interfaceStatus	status;
    time_t		started;
    float		pricePerSecond;
    int			priceUnit;
    float		initialCost;
    int			cumulativeCost;
    int			minimumSeconds;
    int			secondsPerTick;
    float		currentCost;
    int			bytesIn[INTERFACE_TPUT_READINGS];
    int			bytesOut[INTERFACE_TPUT_READINGS];
    int			dqBytesIn[INTERFACE_TPUT_READINGS];  /* De-quantized */
    int			dqBytesOut[INTERFACE_TPUT_READINGS];
    gint		rx;
    gint		tx;
    /* PRIVATE to interface.c */
    char		*terminatedName;
    size_t		tNameLen;
    unsigned long	rx_total;
    unsigned long	tx_total;
    unsigned long	rx_current;
    unsigned long	tx_current;
    guint		rx_delta;
    guint		tx_delta;
    guint		rx_max_delta;
    guint		tx_max_delta;
    gboolean		txrx_init;		/* start averaging? */
};

/* return a list of interfaces currently available on the * system (ppp first).
 * The interface * elements of the list may be interface_freed by the caller;
 * if they are not freed, they will be cached.
 * If none are available, return NULL.
 */
GList *
interface_list_generate(void);

/* return a list of interfaces currently available on the system (ppp first).
 * This acts like interface_list_generate except that the list does not
 * include clone devices.
 */
GList *
interface_no_clone_list_generate(void);

/* returns 0 if interface is not user-manipulable,
 * 1 if it is user-manipulable
 */
int
interface_manipulation_allowed(char *interfaceName);

/* Get the next ppp<n> logical name not yet assigned on the system.
 * Returns "ppp<n>" on success and NULL on failure.
 */
char *
interface_logical_ppp_name_new(void);

/* Get the next ppp<n>-clone<m> logical name not yet assigned on the system.
 * Returns "ppp<n>-clone<m>" on success and NULL on failure.
 */
char *
interface_ppp_clone_new(char *name);

/* Create a new set of ifcfg files and such and return a new interface
 * interfaceName is ifcfg-<interfaceName>
 * logicalName is ifcfg-<logicalName>[-<clone_part>]
 * friendlyName is NAME=<friendlyName> in ifcfg-<interfaceName>
 * Close the returned interface with interface_free()
 */
interface *
interface_create(char *interfaceName, char *logicalName, char *friendlyName);

/* Initialize an interface structure given a logical name.
 * Close the returned interface with interface_free()
 */
interface *
interface_new(char *interfaceName);

/* populate i->cloneList with the names of all the clone devices
 * assigned to this interface.
 */
void
interface_populate_clone_list(interface *i);

/* Free all data associated with an interface */
void
interface_free(interface *i);

/* update the status of the interface that is passed in. */
interfaceStatus
interface_status_update(interface *i);

/* trigger a change to the specified status */
pid_t
interface_status_change(interface *i, interfaceStatus s);

/* calculate the cost of the current connection */
float
interface_current_cost(interface *i);

/* update current tx/rx status */
void
interface_txrx_update(interface *i);

/* put one more entry on each of the bytesIn/bytesOut members */
void
interface_bytecount_update(interface *i);

#ifdef __cplusplus
}
#endif

#endif /* _INTERFACE_H */
