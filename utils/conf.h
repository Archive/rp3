/* Red Hat PPP Dialer applet/application
 * Copyright 1999 Red Hat, Inc.
 *
 * Author: Michael K. Johnson <johnsonm@redhat.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Cambridge, MA 
 * 02139, USA.
 *
 */

#ifndef _CONF_H
#define _CONF_H

#ifdef __cplusplus
extern "C" {
#endif

#include <gnome.h>

/* wrappers around gnome_config that are specific to this app; should
 * all be self-explanitory
 */

float
conf_get_float(char *section, char *key, float def);

void
conf_set_float(char *section, char *key, float value);

int
conf_get_int(char *section, char *key, int def);

void
conf_set_int(char *section, char *key, int value);

gboolean
conf_get_bool(char *section, char *key, gboolean def);

void
conf_set_bool(char *section, char *key, gboolean value);

char *
conf_get_string(char *section, char *key, char *def);

void
conf_set_string(char *section, char *key, char *value);

void
conf_sync(void);

#ifdef __cplusplus
}
#endif

#endif /* _CONF_H */
