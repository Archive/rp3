/* Red Hat PPP Dialer applet/application
 * Copyright 1999 Red Hat, Inc.
 *
 * Author: Michael K. Johnson <johnsonm@redhat.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Cambridge, MA 
 * 02139, USA.
 *
 */

#include <sys/types.h> /* must preceed regex.h :-( */
#include <regex.h>
#include <fcntl.h>
#include <glob.h>
#include <stdio.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <time.h>
#include <unistd.h>

#include <net/if.h>
#include <net/if_ppp.h>

#include <glib.h>
#include <gnome.h>

#include "interface.h"
#include "conf.h"

#include "forkexec.h"
#include "shvar.h"

static GHashTable *interfaceHash;
static time_t dirTime;

/* Open a shvarFile given the interface name.  FIXME: should be extended
 * to search using WVDIALSECT as well to be really slick. */
static shvarFile *
interface_shvarfiles_get(char *interfaceName, gboolean create) {
    shvarFile *ifcfg, *ifcfgParent = NULL;
    char *ifcfgName, *ifcfgParentName, *ifcfgParentDiff;
    static char ifcfgPrefix[] = "/etc/sysconfig/network-scripts/ifcfg-";

    g_assert(interfaceName);
    ifcfgName = alloca(sizeof(ifcfgPrefix)+strlen(interfaceName)+1);
    sprintf(ifcfgName, "%s%s", ifcfgPrefix, interfaceName);
    ifcfg = svNewFile(ifcfgName);
    if (!ifcfg && create) {
	ifcfg = svCreateFile(ifcfgName);
    }
    if (!ifcfg) {
	return NULL;
    }
    close(ifcfg->fd); ifcfg->fd = -1;

    /* Do we have a parent interface to inherit? */
    ifcfgParentDiff = strchr(interfaceName, '-');
    if (ifcfgParentDiff) {
        /* allocate more than enough memory... */
	ifcfgParentName = alloca(sizeof(ifcfgPrefix)+strlen(interfaceName)+1);
	strcpy(ifcfgParentName, ifcfgPrefix);
	strncat(ifcfgParentName, interfaceName, ifcfgParentDiff-interfaceName);
	ifcfgParent = svNewFile(ifcfgParentName);
	ifcfg->parent = ifcfgParent;
	close(ifcfg->parent->fd); ifcfg->parent->fd = -1;
    }

    return ifcfg;
}

static void
interface_shvarfiles_close(shvarFile *ifcfg) {
    if (ifcfg->parent) {
	svWriteFile(ifcfg->parent, 0644);
	svCloseFile(ifcfg->parent);
    }
    svWriteFile(ifcfg, 0644);
    svCloseFile(ifcfg);
}

static char *
interface_ppp_logical_to_physical(char *logicalName) {
    char *mapFileName;
    char buffer[20]; /* more than enough space for ppp<n> */
    char *p, *q;
    int f, n;
    char *physicalDevice = NULL;

    mapFileName = alloca (strlen(logicalName)+20);
    sprintf(mapFileName, "/var/run/ppp-%s.pid", logicalName);
    if ((f = open(mapFileName, O_RDONLY)) >= 0) {
	n = read(f, buffer, 20);
	if (n > 0) {
	    buffer[n] = '\0';
	    /* get past pid */
	    p = buffer; while (*p && *p != '\n') p++; p++;
	    /* get rid of \n */
	    q = p; while (*q && *q != '\n' && q < buffer+n) q++; *q = '\0';
	    if (*p) physicalDevice = strdup(p);
	}
	close(f);
    }

    return physicalDevice;
}

/* find the last modification time on /var/run/ppp-<logicalName>.pid */
static time_t
interface_file_time(interface *i) {
    char *fileName;
    struct stat buf;

    if (strncmp(i->logicalName, "ppp", 3)) return time(NULL);

    fileName = alloca(strlen(i->logicalName) + 18);
    g_assert(fileName);
    sprintf(fileName, "/var/run/ppp-%s.pid", i->logicalName);
    stat(fileName, &buf);
    return buf.st_mtime;
}



/* returns 0 if interface is not user-manipulable,
 * 1 if it is user-manipulable
 */
int
interface_manipulation_allowed(char *interfaceName){
    int ret = 0;
    shvarFile *ifcfg;

    if (!getuid()) return 1; /* root can manipulate anything */

    ifcfg = interface_shvarfiles_get(interfaceName, FALSE);
    if (!ifcfg) return 0;
    ret = svTrueValue(ifcfg, "USERCTL", 0);

    svCloseFile(ifcfg);
    return ret;
}


/* return a list of interfaces currently available on the system (ppp first).
 * The interface * elements of the list may be interface_freed by the caller;
 * if they are not freed, they will be cached.
 * If none are available, return NULL.
 */
GList *
interface_list_generate(void) {
    regex_t p;
    glob_t globbuf;
    GList *interfaceList = NULL;
    GList *pppList = NULL;
    int i;
    char *name;
    struct stat statbuf;
    interface *tmp;

    stat("/etc/sysconfig/network-scripts", &statbuf);
    dirTime = statbuf.st_mtime;

    regcomp(&p, "(.rpm(save|orig)$)|(\\.(orig|bak)$)|(~$)",
	    REG_NOSUB|REG_EXTENDED|REG_ICASE);
    glob("/etc/sysconfig/network-scripts/ifcfg-*", 0, NULL, &globbuf);
    for (i = 0; i < globbuf.gl_pathc; i++) {
	name = strrchr(globbuf.gl_pathv[i], '/');
	name = strchr(name, '-');
	if (!name) continue;
	name++;
	if (!regexec(&p, name, 0, NULL, 0)) continue;
	if (!name[0]) continue;
	tmp = interface_new(name);
	if (tmp) {
	    if (!strncmp(name, "ppp", 3))
	        pppList = g_list_append(pppList, tmp);
	    else
	        interfaceList = g_list_append(interfaceList, tmp);
	}
    }

    globfree(&globbuf);
    regfree(&p);
    interfaceList = g_list_concat(pppList, interfaceList);
    return interfaceList;
}


/* return a list of interfaces currently available on the * system (ppp first).
 * This acts like interface_list_generate except that the list does not
 * include clone devices.
 */
GList *
interface_no_clone_list_generate(void) {
    GList *il = NULL, *interfaceList = NULL, *result = NULL;

    interfaceList = interface_list_generate();

    for (il = interfaceList; il; il = il->next) {
	interface *i = il->data;
	if (!strcmp(i->interfaceName, i->logicalName)) {
	    /* add non-clone to list */
	    result = g_list_append(result, i);
	}
    }

    g_list_free(interfaceList);

    return result;
}


/* Get the next ppp<n> logical name not yet assigned on the system.
 * Returns "ppp<n>" on success and NULL on failure.
 */
char *
interface_logical_ppp_name_new(void) {
    int inum;
    char *file, *ret;
    struct stat statbuf;

    ret = malloc(8); /* enough for "ppp1023\0" -- more than we need */
    g_assert(ret);
    file = alloca(50); /* /etc/sysconfig/network-scripts/ifcfg-ppp1234 + some */
    g_assert(file);
    for (inum = 0; inum < 255; inum++) {
	sprintf(file, "/etc/sysconfig/network-scripts/ifcfg-ppp%d", inum);
	if (stat(file, &statbuf) != 0) {
	    sprintf(ret, "ppp%d", inum);
	    return ret;
	}
    }
    return NULL;
}

/* Get the next ppp<n>-clone<m> logical name not yet assigned on the system.
 * Returns "ppp<n>-clone<m>" on success and NULL on failure.
 */
char *
interface_ppp_clone_new(char *name) {
    int inum;
    char *file, *ret;
    struct stat statbuf;

    ret = malloc(20); /* enough for "ppp1023-clone1234\0" -- more than we need */
    g_assert(ret);
    file = alloca(60); /* /etc/sysconfig/network-scripts/ifcfg-ppp1234 + some */
    g_assert(file);
    for (inum = 0; inum < 255; inum++) {
	sprintf(file, "/etc/sysconfig/network-scripts/ifcfg-%s-clone%d", name, inum);
	if (stat(file, &statbuf) != 0) {
	    sprintf(ret, "%s-clone%d", name, inum);
	    return ret;
	}
    }
    return NULL;
}


/* Create a new set of ifcfg files and such and return a new interface
 * interfaceName is ifcfg-<interfaceName>
 * logicalName is ifcfg-<logicalName>[-<clone_part>]
 * friendlyName is NAME=<friendlyName> in ifcfg-<interfaceName>
 * Close the returned interface with interface_free()
 */
interface *
interface_create(char *interfaceName, char *logicalName, char *friendlyName) {
    interface *i;

    if (!interfaceHash)
	interfaceHash = g_hash_table_new(g_str_hash, g_str_equal);
    i = g_hash_table_lookup(interfaceHash, interfaceName);
    g_assert (!i); /* never create an interface that already exists */

    i = calloc(sizeof(interface), 1);
    i->ifcfg = interface_shvarfiles_get(interfaceName, TRUE);
    g_assert(i->ifcfg);
    i->interfaceName = strdup(interfaceName);
    i->logicalName = strdup(logicalName);
    svSetValue(i->ifcfg, "DEVICE", logicalName);
    if (friendlyName) {
	i->friendlyName = strdup(friendlyName);
	svSetValue(i->ifcfg, "NAME", friendlyName);
    } else {
	i->friendlyName = strdup(interfaceName);
    }
    i->priceUnit = conf_get_int(interfaceName, "priceUnit", 60*60);
    i->pricePerSecond = conf_get_float(interfaceName, "pricePerSecond", 0.0);
    i->initialCost = conf_get_float(interfaceName, "initialCost", 0.0);
    i->cumulativeCost = conf_get_bool(interfaceName, "cumulativeCost", FALSE);
    i->minimumSeconds = conf_get_int(interfaceName, "minimumSeconds", 1);
    i->secondsPerTick = conf_get_int(interfaceName, "secondsPerTick", 1);
    i->currentCost = conf_get_float(interfaceName, "currentCost", 0.0);

    interface_status_update(i);

    g_hash_table_insert(interfaceHash, strdup(interfaceName), i);

    return i;
}

/* Initialize an interface structure given a logical name.
 * Close the returned interface with interface_free()
 */
interface *
interface_new(char *interfaceName) {
    interface *i;

    g_assert(interfaceName);

    if (!interfaceHash)
	interfaceHash = g_hash_table_new(g_str_hash, g_str_equal);
    i = g_hash_table_lookup(interfaceHash, interfaceName);
    if (i) return i;

    i = calloc(sizeof(interface), 1);
    i->ifcfg = interface_shvarfiles_get(interfaceName, TRUE);
    if (!i->ifcfg) {
	free(i);
	return NULL;
    }
    i->interfaceName = strdup(interfaceName);
    i->logicalName = svGetValue(i->ifcfg, "DEVICE");
    /* some people appear to have broken ifcfg files.  :-( */
    if (!i->logicalName) i->logicalName = strdup(i->interfaceName);
    i->friendlyName = svGetValue(i->ifcfg, "NAME");
    if (!i->friendlyName) i->friendlyName = strdup(i->interfaceName);
    if (getuid()) {
	i->manipulable = svTrueValue(i->ifcfg, "USERCTL", 0);
    } else {
	i->manipulable = 1;
    }
    i->priceUnit = conf_get_int(interfaceName, "priceUnit", 60*60);
    i->pricePerSecond = conf_get_float(interfaceName, "pricePerSecond", 0.0);
    i->initialCost = conf_get_float(interfaceName, "initialCost", 0.0);
    i->cumulativeCost = conf_get_bool(interfaceName, "cumulativeCost", FALSE);
    i->minimumSeconds = conf_get_int(interfaceName, "minimumSeconds", 1);
    i->secondsPerTick = conf_get_int(interfaceName, "secondsPerTick", 1);
    i->currentCost = conf_get_float(interfaceName, "currentCost", 0.0);

    interface_status_update(i);

    g_hash_table_insert(interfaceHash, strdup(interfaceName), i);

    return i;
}

/* populate i->cloneList with the names of all the clone devices
 * assigned to this interface.
 */
void
interface_populate_clone_list(interface *i) {
    GList *interfaceList, *il;
    struct stat statbuf;

    if (i->cloneList) {
	stat("/etc/sysconfig/network-scripts", &statbuf);
	if (statbuf.st_mtime != dirTime) {
	    g_slist_free(i->cloneList);
	    i->cloneList = NULL;
	} else {
	    return;
	}
    }

    interfaceList = interface_list_generate();
    for (il = interfaceList; il; il = il->next) {
	interface *c = il->data;

	if ((i != c) &&
	    (!strcmp(i->logicalName, c->logicalName))) {
	    i->cloneList = g_slist_append(i->cloneList, c);
	}
    }
    g_list_free(interfaceList);
}

/* Free all data associated with an interface */
void
interface_free(interface *i) {
    g_hash_table_remove(interfaceHash, i->interfaceName);
    if (!i) return;
    if (i->ifcfg) interface_shvarfiles_close(i->ifcfg);
    if (i->interfaceName) free (i->interfaceName);
    if (i->physicalName) free (i->physicalName);
    if (i->logicalName) free (i->logicalName);
    if (i->friendlyName) free (i->friendlyName);
    if (i->terminatedName) free (i->terminatedName);
    if (i->cloneList) g_slist_free(i->cloneList);
    free (i);
}


/* manage sockets for interface querying */
static int
do_query_socket(gboolean up) {
    static int sock = 0;
    static int refcount = 0;
    int pfs[] = {AF_INET, AF_IPX, AF_AX25, AF_APPLETALK, 0};
    int p = 0;

    if (up) refcount++;
    else refcount--;

    if (up && sock) return sock;
    if (!up) {
	if (!refcount && sock) {
	    close (sock);
	    sock = 0;
	}
	return 0;
    }

    /* At this point, we know we need to bring up a new socket */
    while (!sock && pfs[p]) {
	sock = socket(pfs[p++], SOCK_DGRAM, 0);
    }
    return sock;
}

/* update the status of the interface that is passed in. */
interfaceStatus
interface_status_update(interface *i) {
    int sock = 0;
    struct ifreq ifr;

    sock = do_query_socket(TRUE);

    if (!sock) {
	i->status = INTERFACE_DOWN;
	return i->status;
    }

    if (!strncmp("ppp", i->logicalName, 3)) {
	/* It is a PPP device, so we need to map it from logical to physical
	 * before we look it up.
	 */
	if (!i->physicalName)
	    i->physicalName = interface_ppp_logical_to_physical(i->logicalName);
	if (!i->physicalName) {
	    /* If we can't find a mapping, it's because it's down. */
	    i->status = INTERFACE_DOWN;
	    goto error; /* do not decrement refcount */
	}
    }

    /* only applies to non-ppp devices, so physical == logical */
    if (!i->physicalName) i->physicalName = strdup(i->logicalName);

    memset(&ifr, 0, sizeof(ifr));
    strncpy(ifr.ifr_name, i->physicalName, IFNAMSIZ);

    if (ioctl(sock, SIOCGIFFLAGS, &ifr) < 0) {
	i->status = INTERFACE_DOWN;
    } else if (ifr.ifr_flags & IFF_UP) {
	if (i->status != INTERFACE_UP) {
	    if (i->status == INTERFACE_UNKNOWN) {
		i->started = interface_file_time(i);
	    } else {
		i->started = time(NULL);
	    }
	    i->status = INTERFACE_UP;
	}
    } else {
	i->status = INTERFACE_DOWN;
    }

    do_query_socket(FALSE);

    if (i->status != INTERFACE_UP &&
        !strncmp("ppp", i->logicalName, 3) &&
	i->physicalName) {
	free(i->physicalName);
	i->physicalName = NULL;
    }

error:
    return i->status;
}

/* trigger a change to the specified status */
pid_t
interface_status_change(interface *i, interfaceStatus s) {
    pid_t ret;

    if (s == INTERFACE_UP) {
	i->txrx_init = FALSE; /* ignore initial spike next time */
	ret = fork_exec(0, "/sbin/ifup", i->interfaceName);
    } else /* s == INTERFACE_DOWN */ {
	ret = fork_exec(0, "/sbin/ifdown", i->interfaceName);
    }

    return ret;
}


/* calculate the cost of the current connection */
float
interface_current_cost(interface *i) {
    float cost = 0.0;
    int seconds, ticktime;

    cost = i->initialCost;
    seconds = difftime(time(NULL), i->started);

    if (seconds < i->minimumSeconds)
	seconds = i->minimumSeconds;

    /* avoid division by zero */
    if (i->secondsPerTick < 1) ticktime = 1;
    else ticktime = i->secondsPerTick;

    /* make seconds equal to the amount of time until the end of the next
     * accounting period.  This is a lot of work for nothing in the common
     * case when secondsPerTick is 1, but the math does work out correctly,
     * so this should work for all cases.
     */
    seconds = (seconds / ticktime + (seconds % ticktime ? 1 : 0)) * ticktime;

    cost += seconds * i->pricePerSecond;

    if (i->cumulativeCost)
	cost += i->currentCost;

    return cost;
}

/* update current tx/rx status */
void
interface_txrx_update(interface *i) {
    FILE *f;
#   define BUFLEN 200
    char buf[BUFLEN], *b = buf;

    g_assert(i);
    if (!i->physicalName) return; /* offline but function still queued */

    if (!i->terminatedName) {
	i->terminatedName = calloc(strlen(i->physicalName) + 2, 1);
	i->tNameLen = sprintf(i->terminatedName, "%s:", i->physicalName);
    }

    f = fopen("/proc/net/dev", "r");
    g_assert (f);
    fgets(buf, BUFLEN, f);	/* ignore header */
    fgets(buf, BUFLEN, f);

    if (!fgets(buf, BUFLEN, f)) {      /* get the first candidate line */
	fclose(f);
	return;
    }
    while (*b == ' ') b++;
    while (strncmp(b, i->terminatedName, i->tNameLen)) {
	if (!fgets(buf, BUFLEN, f)) {  /* get the next candidate line */
	    fclose(f);
	    return;
        }
	b = buf;
	while (*b == ' ') b++;
    }
    while (*b != ':') b++; b++;
    sscanf(b, "%lu %*d %*d %*d %*d %*d %*d %*d %lu",
	       &i->rx_total,                   &i->tx_total);
    fclose(f);

    if (!i->rx_current) i->rx_current = i->rx_total;
    if (!i->tx_current) i->tx_current = i->tx_total;

    /* FIXME: try to add some ?x_max_delta scaling factor? */
    i->rx = i->rx_total - (i->rx_current + i->rx_delta);
    i->tx = i->tx_total - (i->tx_current + i->tx_delta);
    if (i->rx < 0 ) i->rx = 0;
    if (i->tx < 0 ) i->tx = 0;
    /*
    fprintf(stderr, "rx %d tx %d dr %d dt %d\n",
	    i->rx_total, i->tx_total, i->rx, i->tx);
    */

    i->rx_delta = i->rx_total - i->rx_current;
    i->tx_delta = i->tx_total - i->tx_current;

    /* wait for initial spike in *_total to go out of the picture */
    if (i->txrx_init) {
	if (i->rx_delta > i->rx_max_delta) i->rx_max_delta = i->rx_delta;
	if (i->tx_delta > i->tx_max_delta) i->tx_max_delta = i->tx_delta;
    }
}

/* puts one more entry on each of the bytesIn/bytesOut members.
 * uses the data collected in txrx_update.  Assumes that txrx_update
 * is called much more frequently than bytecount_update.
 */
void
interface_bytecount_update(interface *i) {
    if (!i->txrx_init) {
	i->txrx_init = TRUE;
    } else {
	/* raw array */
	memmove(i->bytesIn, i->bytesIn+1, (INTERFACE_TPUT_READINGS-1)*sizeof(int));
	memmove(i->bytesOut, i->bytesOut+1, (INTERFACE_TPUT_READINGS-1)*sizeof(int));
	i->bytesIn[INTERFACE_TPUT_READINGS-1] = i->rx_delta;
	i->bytesOut[INTERFACE_TPUT_READINGS-1] = i->tx_delta;

	/* de-quantized array */
	memmove(i->dqBytesIn, i->dqBytesIn+1, (INTERFACE_TPUT_READINGS-2)*sizeof(int));
	memmove(i->dqBytesOut, i->dqBytesOut+1, (INTERFACE_TPUT_READINGS-2)*sizeof(int));
	i->dqBytesIn[INTERFACE_TPUT_READINGS-2] = 
	    ((i->bytesIn[INTERFACE_TPUT_READINGS-4]/2) +
	     i->bytesIn[INTERFACE_TPUT_READINGS-3] +
	     (i->bytesIn[INTERFACE_TPUT_READINGS-2]/2)) / 2;
	i->dqBytesOut[INTERFACE_TPUT_READINGS-2] = 
	    ((i->bytesOut[INTERFACE_TPUT_READINGS-4]/2) +
	     i->bytesOut[INTERFACE_TPUT_READINGS-3] +
	     (i->bytesOut[INTERFACE_TPUT_READINGS-2]/2)) / 2;
	i->dqBytesIn[INTERFACE_TPUT_READINGS-1] = 
	    (i->bytesIn[INTERFACE_TPUT_READINGS-3] +
	     i->bytesIn[INTERFACE_TPUT_READINGS-2]) / 2;
	i->dqBytesOut[INTERFACE_TPUT_READINGS-1] = 
	    (i->bytesOut[INTERFACE_TPUT_READINGS-3] +
	     i->bytesOut[INTERFACE_TPUT_READINGS-2]) / 2;
    }
    i->rx_delta = 0; i->tx_delta = 0;
    i->rx_current = i->rx_total;
    i->tx_current = i->tx_total;
    i->rx = 0;
    i->tx = 0;
}

