/*
 * shtest.c
 *
 * test shvar package
 *
 * Copyright 1999 Red Hat, Inc.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

#include <string.h>
#include <stdio.h>

#include "shvar.h"

void
testGetValue(shvarFile *s, char *key, char *expected) {
    char *tmp;

    tmp = svGetValue(s, key);
    if (expected) {
	if (!tmp || strcmp(tmp, expected))
	    printf("svGetValue \"%s\" result \"%s\" s/b \"%s\"\n", key, tmp, expected);
    } else {
	if (tmp)
	    printf("svGetValue \"%s\" result \"%s\" s/b NULL\n", key, tmp);
    }
    free(tmp);
}

void
testTrueValue(shvarFile *s, char *key, int def, int expected) {
    int res;

    res = svTrueValue(s, key, def);
    if (res != expected)
	printf("svTrueValue got unexpected result %d, s/b %d\n", res, expected);
}

int
main(void) {
    shvarFile *s, *p;

    /* basic adding */
    s = svNewFile("test.1");
    if (!s->fileName) printf ("missing fileName in shvarFile\n");
    if (s->fd != -1) printf ("test.1 must be read only\n");
    testGetValue(s, "BAZ", "baz");
    testGetValue(s, "BAR", "bar");
    testGetValue(s, "FOO", "foo");
    svSetValue(s, "BLAH", "blah");
    testGetValue(s, "BLAH", "blah");

    /* write the contents to a second file */
    s->fileName = strdup("test.2");
    if (s->fd != -1) {
	    close(s->fd);
	    s->fd = -1;
    }
    svWriteFile(s, 0644);
    svCloseFile(s);

    /* only writing when necessary, not otherwise */
    s = svNewFile("test.1");
    testGetValue(s, "BAZ", "baz");
    s->fileName = strdup("must.not.exist");
    if (s->fd != -1) {
	    close(s->fd);
	    s->fd = -1;
    }
    svWriteFile(s, 0644);
    svCloseFile(s);

    /* basic variable deletion */
    s = svNewFile("test.2");
    svSetValue(s, "BLAH", NULL);
    testGetValue(s, "BLAH", NULL);
    s->fileName = strdup("test.3");
    if (s->fd != -1) {
	    close(s->fd);
	    s->fd = -1;
    }
    svWriteFile(s, 0644);
    svCloseFile(s);

    /* basic variable changing */
    s = svNewFile("test.1");
    svSetValue(s, "BAZ", "baz2");
    testGetValue(s, "BAZ", "baz2");
    s->fileName = strdup("test.4");
    if (s->fd != -1) {
	    close(s->fd);
	    s->fd = -1;
    }
    svWriteFile(s, 0644);
    svCloseFile(s);

    /* test basic inheritance */
    p = svNewFile("test.2");
    s = svNewFile("test.1");
    s->parent = p;
    testGetValue(s, "BLAH", "blah");	/* inheritance */
    svSetValue(s, "BLAH", NULL);	/* override parent with BLAH= */
    svSetValue(s, "FOOF", "foof");	/* write in test.4 */
    svSetValue(s, "BEEP", "beep");	/* set up for deletion later */
    testGetValue(s, "BLAH", NULL);
    testGetValue(s, "FOOF", "foof");
    testGetValue(s, "BEEP", "beep");
    svSetValue(s, "BEEP", NULL);	/* simply delete in child */
    testGetValue(s, "BEEP", NULL);
    s->fileName = strdup("test.5");
    if (s->fd != -1) {
	    close(s->fd);
	    s->fd = -1;
    }
    svWriteFile(s, 0644);
    svCloseFile(s);
    svWriteFile(p, 0644);
    svCloseFile(p);

    /* test all inheritance table positions */
    /* FIXME */

    /* test reading and writing boolean information ... */
    s = svCreateFile("test.6");
    svSetValue(s, "FOO", "silly");
    svSetValue(s, "TRUE", "TrUe");
    svSetValue(s, "F", "f");
    svSetValue(s, "FALSE", "false");
    testTrueValue(s, "FOO", 1, 1);
    testTrueValue(s, "FOO", 0, 0);
    testTrueValue(s, "TRUE", 0, 1);
    testTrueValue(s, "TRUE", 1, 1);
    testTrueValue(s, "F", 0, 0);
    testTrueValue(s, "F", 1, 0);
    testTrueValue(s, "FALSE", 0, 0);
    testTrueValue(s, "FALSE", 1, 0);
    svWriteFile(s, 0644);
    svCloseFile(s);

    /* test escaping weird values */
    s = svCreateFile("test.7");
    svSetValue(s, "FUNKY", " (\t)this&is!a\\test#of^the%emergency$variable|escaping}system");
    testGetValue(s, "FUNKY", " (\t)this&is!a\\test#of^the%emergency$variable|escaping}system");
    svWriteFile(s, 0644);
    svCloseFile(s);

    /* test creation from scratch */
    s = svCreateFile("test.8");
    svSetValue(s, "THIS", "true");
    testGetValue(s, "THIS", "true");
    svWriteFile(s, 0644);
    svCloseFile(s);
    
    /* test setting to empty string being equivalent to NULL */
    s = svCreateFile("test.9");
    svSetValue(s, "THIS", "true");
    testGetValue(s, "THIS", "true");
    svSetValue(s, "THAT", "true");
    testGetValue(s, "THAT", "true");
    svSetValue(s, "THIS", NULL);
    testGetValue(s, "THIS", NULL);
    svSetValue(s, "THAT", "");
    testGetValue(s, "THAT", NULL);
    svWriteFile(s, 0644);
    svCloseFile(s);

    /* test opening non-existant files */
    s = svNewFile("test.10");
    if (s)
        printf("opened test.10, which shouldn't exist\n");

    return 0;
}
