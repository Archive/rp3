/*
 * forkexec.h
 *
 * contains a function to do a fork, exec, wait without using
 * system() -- we want the PPID of the child process to be the
 * process that calls fork_exec()
 *
 * Copyright 1999 Red Hat, Inc.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
#ifndef _FORKEXEC_H
#define _FORKEXEC_H

#include <glib.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/* lighter and simpler than system()
 * If wait, returns 0 on success, non-zero on failure.
 * If !wait, returns child pid.
 */
int
fork_exec(int wait, char *path, char *arg);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* ! _FORKEXEC_H */
