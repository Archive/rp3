/* Red Hat PPP Dialer applet/application
 * Copyright 1999 Red Hat, Inc.
 *
 * Author: Michael K. Johnson <johnsonm@redhat.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Cambridge, MA 
 * 02139, USA.
 *
 */

#ifndef _NETREPORT_H
#define _NETREPORT_H

#ifdef __cplusplus
extern "C" {
#endif

#include "forkexec.h"

/* Subscribe to netreport events.
 * This MUST be called from the process that wants to receive SIGIO
 * signals when network status changes happen.
 * Returns 0 on success, non-zero on failure.
 */
int
netreport_subscribe(void);

/* Unsubscribe to netreport events.
 * This MUST be called from the smae process that called _subscribe().
 * Returns 0 on success, non-zero on failure.
 */
int
netreport_unsubscribe(void);

#ifdef __cplusplus
}
#endif

#endif /* _INTERFACE_H */
