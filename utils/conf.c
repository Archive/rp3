/* Red Hat PPP Dialer applet/application
 * Copyright 1999 Red Hat, Inc.
 *
 * Author: Michael K. Johnson <johnsonm@redhat.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Cambridge, MA 
 * 02139, USA.
 *
 */

#include <config.h>
#include "conf.h"

/* wrappers around gnome_config that are specific to this app; should
 * all be self-explanitory.  We don't use *_with_default because we
 * don't really care whether the value was set in the first place.
 */

float
conf_get_float(char *section, char *key, float def) {
    float ret = def;
    gchar *configPath = g_strdup_printf("/%s/%s/%s=%f", PACKAGE, section, key,
		    			def);
    ret = gnome_config_get_float(configPath);
    g_free(configPath);
    return ret;
}

void
conf_set_float(char *section, char *key, float value) {
    gchar *configPath = g_strdup_printf("/%s/%s/%s", PACKAGE, section, key);
    gnome_config_set_float(configPath, value);
    g_free(configPath);
}

int
conf_get_int(char *section, char *key, int def) {
    int ret = def;
    gchar *configPath = g_strdup_printf("/%s/%s/%s=%d", PACKAGE, section, key,
		    			def);
    ret = gnome_config_get_int(configPath);
    g_free(configPath);
    return ret;
}

void
conf_set_int(char *section, char *key, int value) {
    gchar *configPath = g_strdup_printf("/%s/%s/%s", PACKAGE, section, key);
    gnome_config_set_int(configPath, value);
    g_free(configPath);
}

gboolean
conf_get_bool(char *section, char *key, gboolean def) {
    gboolean ret = def;
    gchar *configPath = g_strdup_printf("/%s/%s/%s=%s", PACKAGE, section, key,
				 	def ? "true" : "false");
    ret = gnome_config_get_bool(configPath);
    g_free(configPath);
    return ret;
}

void
conf_set_bool(char *section, char *key, gboolean value) {
    gchar *configPath = g_strdup_printf("/%s/%s/%s", PACKAGE, section, key);
    gnome_config_set_bool(configPath, value);
    g_free(configPath);
}

char *
conf_get_string(char *section, char *key, char *def) {
    char *ret = def;
    gchar *configPath = g_strdup_printf("/%s/%s/%s=%s", PACKAGE, section, key,
		   			def);
    ret = gnome_config_get_string(configPath);
    g_free(configPath);
    return ret;
}

void
conf_set_string(char *section, char *key, char *value) {
    gchar *configPath = g_strdup_printf("/%s/%s/%s", PACKAGE, section, key);
    gnome_config_set_string(configPath, value);
    g_free(configPath);
}

void
conf_sync(void) {
    gnome_config_sync();
}
