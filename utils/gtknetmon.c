/*
 * Network connection mini-display widget
 *
 * Developed by Havoc Pennington <hp@pobox.com>
 *
 * Copyright (C) 1999 Red Hat, Inc. GNU General Public License
 */

#include <config.h>

#include "gtknetmon.h"

#include <libgnome/gnome-defs.h>
#include <libgnome/gnome-i18n.h>
#include <gtk/gtkbutton.h>
#include <gtk/gtktooltips.h>
#include <gtk/gtksignal.h>
#include <gtk/gtkpixmap.h>
#include <math.h>
#include <string.h>

static char *red_led_xpm[] = {
/* columns rows colors chars-per-pixel */
"5 5 4 1",
"  c None",
". c #be0b10",
"X c #ff0619",
"o c #ff7078",
/* pixels */
" ooo ",
"oXXX.",
"oXXX.",
"oXXX.",
" ... "
};

static char *blue_led_xpm[] = {
/* columns rows colors chars-per-pixel */
"5 5 4 1",
"  c None",
"o      c #9FF004",
"X      c #64E004",
".      c #33D404",
#if 0
". c #2aaa65",
"X c #7fffd4",
"o c #cffff9",
#endif
/* pixels */
" ooo ",
"oXXX.",
"oXXX.",
"oXXX.",
" ... "
};

static char *red_empty_led_xpm[] = {
/* columns rows colors chars-per-pixel */
"5 5 4 1",
"  c None",
"o      c #580000",
"X      c #430204",
".      c #330204",
/* pixels */
" ooo ",
"oXXX.",
"oXXX.",
"oXXX.",
" ... "
};

static char *blue_empty_led_xpm[] = {
/* columns rows colors chars-per-pixel */
"5 5 4 1",
"  c None",
"o      c #005800",
"X      c #024304",
".      c #023304",
/* pixels */
" ooo ",
"oXXX.",
"oXXX.",
"oXXX.",
" ... "
};

#if 0
static char *disconnected_led_xpm[] = {
/* columns rows colors chars-per-pixel */
"12 12 7 1",
"       c None",
".      c #020204",
"+      c #023304",
"@      c #023B04",
"#      c #024B04",
"$      c #024304",
"%      c #025204",
"    ....    ",
"  ..%###..  ",
" .%%###$$$. ",
" .%###$$$@. ",
".%###$$$@@@.",
".###$$$@@@+.",
".##$$$@@@++.",
".#$$$@@@+++.",
" .$$@@@+++. ",
" .$@@@++++. ",
"  ..@+++..  ",
"    ....    "
};

static char *connected_led_xpm[] = {
/* columns rows colors chars-per-pixel */
"12 12 7 1",
"       c None",
".      c #020204",
"+      c #33D404",
"@      c #81E904",
"#      c #4DDB04",
"$      c #9FF004",
"%      c #64E004",
"    ....    ",
"  ..$$@@..  ",
" .$$$@@@@%. ",
" .$$@@@@%%. ",
".$$@@@@%%##.",
".$@@@@%%###.",
".@@@@%%###+.",
".@@@%%###++.",
" .@%%###++. ",
" .%%###+++. ",
"  ..##++..  ",
"    ....    "
};
#endif

static char * connected_led_xpm[] = {
"11 10 6 1",
"       c None",
".      c #9FF004",
"+      c #81E904",
"@      c #64E004",
"#      c #4DDB04",
"$      c #33D404",
"   ....    ",
" ...++++@  ",
" ..++++@@  ",
"..++++@@## ",
".++++@@##$ ",
".+++@@###$ ",
".++@@###$$ ",
" +@@###$$  ",
" @@###$$$  ",
"   $$$$    "};

static char * disconnected_led_xpm[] = {
"11 10 6 1",
"       c None",
".      c #005800",
"+      c #024B04",
"@      c #024304",
"#      c #023B04",
"$      c #023304",
"   ....    ",
" ..+++@@@  ",
" .+++@@@#  ",
".+++@@@##$ ",
".++@@@###$ ",
".+@@@###$$ ",
".@@@###$$$ ",
" @@###$$$  ",
" @###$$$$  ",
"   $$$$    "};

static void   gtk_netmon_class_init    (GtkNetmonClass  *klass);
static void   gtk_netmon_init          (GtkNetmon       *netmon);


/* GtkObject functions */
static void   gtk_netmon_destroy       (GtkObject   *object);

static void   gtk_netmon_finalize      (GtkObject* object);

/* GtkWidget functions */

static gint   gtk_netmon_event (GtkWidget          *widget,
                                GdkEvent           *event);

static void gtk_netmon_realize        (GtkWidget        *widget);
static void gtk_netmon_unrealize      (GtkWidget        *widget);
static void gtk_netmon_size_request   (GtkWidget        *widget,
                                       GtkRequisition   *requisition);
static void gtk_netmon_size_allocate  (GtkWidget        *widget,
                                       GtkAllocation    *allocation);

static void gtk_netmon_draw           (GtkWidget        *widget,
                                       GdkRectangle     *area);
static gint gtk_netmon_expose         (GtkWidget        *widget,
                                       GdkEventExpose   *event);

/* GtkNetmon-specific functions */

static void gtk_netmon_paint          (GtkNetmon            *netmon,
                                       GdkRectangle     *area);

static GtkBinClass *parent_class = NULL;

enum {
  CLICKED,
  LAST_SIGNAL
};

static guint netmon_signals[LAST_SIGNAL] = { 0 };

guint
gtk_netmon_get_type (void)
{
  static guint netmon_type = 0;

  if (!netmon_type)
    {
      static const GtkTypeInfo netmon_info =
      {
        "GtkNetmon",
        sizeof (GtkNetmon),
        sizeof (GtkNetmonClass),
        (GtkClassInitFunc) gtk_netmon_class_init,
        (GtkObjectInitFunc) gtk_netmon_init,
        /* reserved_1 */ NULL,
        /* reserved_2 */ NULL,
        (GtkClassInitFunc) NULL,
      };

      netmon_type = gtk_type_unique (gtk_bin_get_type (), &netmon_info);
    }

  return netmon_type;
}

static void
gtk_netmon_class_init (GtkNetmonClass *klass)
{
  GtkObjectClass *object_class;
  GtkWidgetClass *widget_class;

  object_class = (GtkObjectClass*) klass;
  widget_class = (GtkWidgetClass*) klass;

  parent_class = gtk_type_class (gtk_bin_get_type ());

  object_class->destroy = gtk_netmon_destroy;
  object_class->finalize = gtk_netmon_finalize;

  widget_class->realize = gtk_netmon_realize;
  widget_class->unrealize = gtk_netmon_unrealize;

  widget_class->size_request = gtk_netmon_size_request;
  
  widget_class->size_allocate = gtk_netmon_size_allocate;
  
  widget_class->draw = gtk_netmon_draw;         

  widget_class->event = gtk_netmon_event;
  
  widget_class->expose_event = gtk_netmon_expose;

  netmon_signals[CLICKED] =
    gtk_signal_new ("clicked",
                    GTK_RUN_FIRST,
                    object_class->type,
                    GTK_SIGNAL_OFFSET (GtkNetmonClass, clicked),
                    gtk_marshal_NONE__NONE,
		    GTK_TYPE_NONE, 0);

  gtk_object_class_add_signals (object_class, netmon_signals, LAST_SIGNAL);
}

static void
emit_clicked_cb(GtkWidget* button, GtkNetmon* netmon)
{
  gtk_signal_emit(GTK_OBJECT(netmon), netmon_signals[CLICKED]);
}

static void
gtk_netmon_init (GtkNetmon *netmon)
{
  GTK_WIDGET_UNSET_FLAGS (GTK_WIDGET(netmon), GTK_NO_WINDOW);
  netmon->seconds_online = 0;
  netmon->cost = NULL;
  netmon->speed = NULL;
  netmon->rxs_tput = NULL;
  netmon->txs_tput = NULL;
  netmon->throughput_count = 0;
  netmon->online = FALSE;
  netmon->tx = FALSE;
  netmon->rx = FALSE;
  netmon->style = GTK_NETMON_TIME;

  netmon->disconnect_button = FALSE;

  netmon->orientation = GTK_NETMON_HORIZONTAL;
  netmon->panel_size = 48;

  netmon->button_tip = gtk_tooltips_new ();
  gtk_object_ref (GTK_OBJECT (netmon->button_tip));
  gtk_object_sink (GTK_OBJECT (netmon->button_tip));
}

GtkWidget*
gtk_netmon_new (gboolean with_disconnect)
{
  GtkNetmon *netmon;

  netmon = gtk_type_new (gtk_netmon_get_type ());

  gtk_netmon_set_has_disconnect(netmon, with_disconnect);
     
  return GTK_WIDGET (netmon);
}

/* 
 * GtkObject functions 
 */

static void   
gtk_netmon_destroy       (GtkObject   *object)
{
  GtkNetmon* netmon;

  g_return_if_fail(object != NULL);
  g_return_if_fail(GTK_IS_NETMON(object));

  netmon = GTK_NETMON(object);

  /* Chain up */
  if (GTK_OBJECT_CLASS(parent_class)->destroy)
    (* GTK_OBJECT_CLASS(parent_class)->destroy) (object);
}

static void   
gtk_netmon_finalize      (GtkObject* object)
{
  GtkNetmon* netmon;

  g_return_if_fail(object != NULL);
  g_return_if_fail(GTK_IS_NETMON(object));

  netmon = GTK_NETMON(object);
  
  if (netmon->rxs_tput)
    g_free(netmon->rxs_tput);

  if (netmon->txs_tput)
    g_free(netmon->txs_tput);

  if (netmon->cost)
    g_free(netmon->cost);

  if (netmon->speed)
    g_free(netmon->speed);

  if (netmon->button_tip)
    {
      gtk_object_unref (GTK_OBJECT (netmon->button_tip));
    }
  
  /* Chain up */
  if (GTK_OBJECT_CLASS(parent_class)->finalize)
    (* GTK_OBJECT_CLASS(parent_class)->finalize) (object);
}

/* 
 * GtkWidget functions 
 */

static gint   
gtk_netmon_event (GtkWidget        *widget,
                  GdkEvent         *event)
{
  GtkNetmon* netmon;

  g_return_val_if_fail(widget != NULL, FALSE);
  g_return_val_if_fail(GTK_IS_NETMON(widget), FALSE);

  netmon = GTK_NETMON(widget);

  return FALSE; /* We no longer emit clicked unless you
                 * click the actual button
                 */
  
  switch (event->type)
    {
    case GDK_BUTTON_PRESS:
      if (event->button.button == 1)
        {
          gtk_signal_emit(GTK_OBJECT(widget), netmon_signals[CLICKED]);
          return TRUE;
        }
      break;
    default:
      break;
    }

  return FALSE;
}


static void
update_button_state (GtkNetmon* netmon)
{
  if (GTK_BIN (netmon)->child)
    {
      GtkWidget *button = GTK_BIN (netmon)->child;
      GtkWidget *pixmap = GTK_BIN (button)->child;
      
      if (netmon->online)
        {
          gtk_tooltips_set_tip (netmon->button_tip, button, _("Disconnect"), NULL);

          gtk_pixmap_set (GTK_PIXMAP (pixmap),
                          netmon->connected_led_pixmap,
                          netmon->connected_led_mask);
        }
      else
        {
          gtk_tooltips_set_tip (netmon->button_tip, button, _("Connect"), NULL);

          gtk_pixmap_set (GTK_PIXMAP (pixmap),
                          netmon->disconnected_led_pixmap,
                          netmon->disconnected_led_mask);
        }
    }
}

static void 
gtk_netmon_realize        (GtkWidget        *widget)
{
  GdkWindowAttr attributes;
  gint attributes_mask;
  GtkNetmon* netmon;
  GdkColormap* cmap;

  g_return_if_fail(widget != NULL);
  g_return_if_fail(GTK_IS_NETMON(widget));

  netmon = GTK_NETMON(widget);

  /* Set realized flag */

  GTK_WIDGET_SET_FLAGS (widget, GTK_REALIZED);

  /* Main widget window */

  attributes.window_type = GDK_WINDOW_CHILD;
  attributes.x = widget->allocation.x;
  attributes.y = widget->allocation.y;
  attributes.width = widget->allocation.width;
  attributes.height = widget->allocation.height;
  attributes.wclass = GDK_INPUT_OUTPUT;
  attributes.visual = gtk_widget_get_visual (widget);
  attributes.colormap = gtk_widget_get_colormap (widget);
  attributes.event_mask = gtk_widget_get_events (widget) | GDK_EXPOSURE_MASK | GDK_BUTTON_PRESS_MASK | GDK_ENTER_NOTIFY_MASK | GDK_LEAVE_NOTIFY_MASK;

  attributes_mask = GDK_WA_X | GDK_WA_Y | GDK_WA_VISUAL | GDK_WA_COLORMAP;

  widget->window = gdk_window_new (gtk_widget_get_parent_window (widget),
                                   &attributes, attributes_mask);
  gdk_window_set_user_data (widget->window, widget);

  /* Style */

  widget->style = gtk_style_attach (widget->style, widget->window);

  gtk_style_set_background (widget->style, widget->window, GTK_STATE_NORMAL); 

  /*  gdk_window_set_background(widget->window, &widget->style->black); */

  /* Load fonts */

  netmon->small_font = gdk_font_load("-schumacher-clean-medium-r-normal-*-*-80-*-*-c-*-iso8859-1");

  if (netmon->small_font == NULL)
    {
      g_warning("Couldn't load fonts for NETMON display");
      /* This will be unusable, but won't segv */
      netmon->small_font = widget->style->font;
      gdk_font_ref(netmon->small_font);
    }

  /* Alloc colors */
  cmap = gtk_widget_get_colormap(widget);
  
  netmon->red.red = 65535;
  netmon->red.green = 10000;
  netmon->red.blue = 10000;
  gdk_colormap_alloc_color(cmap, &netmon->red, FALSE, TRUE);

  gdk_color_parse ("#64E004", &netmon->blue);
  gdk_colormap_alloc_color(cmap, &netmon->blue, FALSE, TRUE);

  /* GC's */

  netmon->red_gc = gdk_gc_new(widget->window);
  gdk_gc_set_foreground(netmon->red_gc, &netmon->red);

  netmon->blue_gc = gdk_gc_new(widget->window);
  gdk_gc_set_foreground(netmon->blue_gc, &netmon->blue);

  netmon->red_led_pixmap = gdk_pixmap_create_from_xpm_d(widget->window,
                                                        &netmon->red_led_mask,
                                                        NULL,
                                                        red_led_xpm);

  netmon->blue_led_pixmap = gdk_pixmap_create_from_xpm_d(widget->window,
                                                         &netmon->blue_led_mask,
                                                         NULL,
                                                         blue_led_xpm);

  netmon->red_empty_led_pixmap = gdk_pixmap_create_from_xpm_d(widget->window,
                                                              &netmon->red_empty_led_mask,
                                                              NULL,
                                                              red_empty_led_xpm);


  netmon->blue_empty_led_pixmap = gdk_pixmap_create_from_xpm_d(widget->window,
                                                              &netmon->blue_empty_led_mask,
                                                              NULL,
                                                              blue_empty_led_xpm);
  
  netmon->connected_led_pixmap = gdk_pixmap_create_from_xpm_d (widget->window,
                                                               &netmon->connected_led_mask,
                                                               NULL,
                                                               connected_led_xpm);

  netmon->disconnected_led_pixmap = gdk_pixmap_create_from_xpm_d (widget->window,
                                                                  &netmon->disconnected_led_mask,
                                                                  NULL,
                                                                  disconnected_led_xpm);

  update_button_state (netmon);
}

static void 
gtk_netmon_unrealize (GtkWidget        *widget)
{
  GtkNetmon* netmon;
  GdkColormap* cmap;

  g_return_if_fail(widget != NULL);
  g_return_if_fail(GTK_IS_NETMON(widget));

  netmon = GTK_NETMON(widget);

  /* Hide all windows */

  if (GTK_WIDGET_MAPPED (widget))
    gtk_widget_unmap (widget);
  
  GTK_WIDGET_UNSET_FLAGS (widget, GTK_MAPPED);

  gdk_font_unref(netmon->small_font);

  gdk_gc_unref(netmon->red_gc);
  gdk_gc_unref(netmon->blue_gc);

  cmap = gtk_widget_get_colormap(widget);

  gdk_colormap_free_colors(cmap, &netmon->red, 1);
  gdk_colormap_free_colors(cmap, &netmon->blue, 1);

  gdk_pixmap_unref(netmon->red_led_pixmap);
  gdk_bitmap_unref(netmon->red_led_mask);

  gdk_pixmap_unref(netmon->blue_led_pixmap);
  gdk_bitmap_unref(netmon->blue_led_mask);

  gdk_pixmap_unref(netmon->red_empty_led_pixmap);
  gdk_bitmap_unref(netmon->red_empty_led_mask);

  gdk_pixmap_unref(netmon->blue_empty_led_pixmap);
  gdk_bitmap_unref(netmon->blue_empty_led_mask);
  
  gdk_pixmap_unref (netmon->connected_led_pixmap);
  gdk_bitmap_unref (netmon->connected_led_mask);

  gdk_pixmap_unref (netmon->disconnected_led_pixmap);
  gdk_bitmap_unref (netmon->disconnected_led_mask);
  
  gdk_pixmap_unref(netmon->buffer);
  netmon->buffer = NULL;

  /* This destroys widget->window and unsets the realized flag
   */
  if (GTK_WIDGET_CLASS(parent_class)->unrealize)
    (* GTK_WIDGET_CLASS(parent_class)->unrealize) (widget);
}

static void
get_requisition (GtkRequisition *requisition,
                 GtkNetmonOrientation orient,
                 gint size)
{
  switch (orient)
    {
    case GTK_NETMON_VERTICAL:
      requisition->width = size;
      requisition->height = 48;
      break;

    case GTK_NETMON_HORIZONTAL:
      if (size < 48)
        requisition->width = 48 * 2;
      else
        requisition->width = 48;
      
      requisition->height = size;
      break;

    default:
      g_assert_not_reached ();
      break;
    }
}

static void 
gtk_netmon_size_request   (GtkWidget        *widget,
                           GtkRequisition   *requisition)
{
  GtkNetmon *netmon;
  
  g_return_if_fail(widget != NULL);
  g_return_if_fail(GTK_IS_NETMON(widget));
  
  netmon = GTK_NETMON (widget);
  
  get_requisition (requisition, netmon->orientation, netmon->panel_size);
  
  if (GTK_NETMON(widget)->disconnect_button)
    {
      /* maintain invariant that all widgets get a size request */
      GtkRequisition dummy;
      gtk_widget_size_request(GTK_BIN(widget)->child, &dummy);
    }
}

static void 
compute_rects_normal_layout (GtkNetmon *netmon)
{
  GtkWidget *widget;
  GdkRectangle graph_rect;
  GdkRectangle tx_rect;
  GdkRectangle rx_rect;
  GdkRectangle hangup_rect;
  GdkRectangle timecost_rect;
  GdkRectangle speed_rect;

  static const gint border = 2;
  static const gint diameter = 5;
  
  widget = GTK_WIDGET (netmon);
  
  tx_rect.x = border;
  tx_rect.y = border*2;
  tx_rect.width = diameter;
  tx_rect.height = diameter;
  
  rx_rect.x = border;
  rx_rect.y = tx_rect.y + tx_rect.height + border;
  rx_rect.width = diameter;
  rx_rect.height = diameter;

  speed_rect.height = 12;
  speed_rect.y = widget->allocation.height - speed_rect.height;
  speed_rect.x = border;
  speed_rect.width = widget->allocation.width - border*2;

  timecost_rect = speed_rect;
  timecost_rect.y -= speed_rect.height;

  hangup_rect.width = widget->allocation.width / 3;
  hangup_rect.height = widget->allocation.height - speed_rect.height - 
    timecost_rect.height - border*2 + 1;
  hangup_rect.y = border;
  hangup_rect.x = widget->allocation.width - border - hangup_rect.width;

  graph_rect.x = border + diameter + border;
  graph_rect.y = border;
  graph_rect.height = hangup_rect.height;
  graph_rect.width = speed_rect.width - diameter - border;

  if (netmon->disconnect_button)
    graph_rect.width -= (hangup_rect.width + border);

  netmon->tx_rect = tx_rect;
  netmon->rx_rect = rx_rect;
  netmon->graph_rect = graph_rect;
  netmon->hangup_rect = hangup_rect;
  netmon->timecost_rect = timecost_rect;
  netmon->speed_rect = speed_rect;
}

static void
compute_rects_flat_layout (GtkNetmon *netmon)
{
  GtkWidget *widget;
  GdkRectangle graph_rect;
  GdkRectangle tx_rect;
  GdkRectangle rx_rect;
  GdkRectangle hangup_rect;
  GdkRectangle timecost_rect;
  GdkRectangle speed_rect;

  static const gint border = 2;
  static const gint diameter = 5;

  widget = GTK_WIDGET (netmon);
    
  tx_rect.x = border;
  tx_rect.y = border*2;
  tx_rect.width = diameter;
  tx_rect.height = diameter;
  
  rx_rect.x = border;
  rx_rect.y = tx_rect.y + tx_rect.height + border;
  rx_rect.width = diameter;
  rx_rect.height = diameter;

  speed_rect.height = widget->allocation.height / 2;
  speed_rect.y = widget->allocation.height - speed_rect.height;
  speed_rect.width = widget->allocation.width / 2 - border*2;
  speed_rect.x = widget->allocation.width - speed_rect.width - border;
  
  timecost_rect = speed_rect;
  timecost_rect.y -= speed_rect.height - 1;
  
  hangup_rect.width = widget->allocation.width / 6 + 1;
  hangup_rect.height = widget->allocation.height - border * 2;
  hangup_rect.y = border;
  hangup_rect.x = widget->allocation.width - border - hangup_rect.width - speed_rect.width - border;

  graph_rect.x = border + diameter + border;
  graph_rect.y = border;
  graph_rect.height = hangup_rect.height;
  graph_rect.width = speed_rect.width - diameter - border;

  if (netmon->disconnect_button)
    graph_rect.width -= (hangup_rect.width + border);

  netmon->tx_rect = tx_rect;
  netmon->rx_rect = rx_rect;
  netmon->graph_rect = graph_rect;
  netmon->hangup_rect = hangup_rect;
  netmon->timecost_rect = timecost_rect;
  netmon->speed_rect = speed_rect;
}

static void
compute_rects_micro_layout (GtkNetmon *netmon)
{
  GtkWidget *widget;
  GdkRectangle graph_rect;
  GdkRectangle tx_rect;
  GdkRectangle rx_rect;
  GdkRectangle hangup_rect;
  GdkRectangle timecost_rect;
  GdkRectangle speed_rect;

  static const gint border = 2;
  static const gint diameter = 5;

  widget = GTK_WIDGET (netmon);
  
  tx_rect.x = border;
  tx_rect.y = border*2;
  tx_rect.width = diameter;
  tx_rect.height = diameter;
  
  rx_rect.x = border;
  rx_rect.y = tx_rect.y + tx_rect.height + border;
  rx_rect.width = diameter;
  rx_rect.height = diameter;

  speed_rect.height = 0;
  speed_rect.y = 0;
  speed_rect.x = 0;
  speed_rect.width = 0;

  timecost_rect = speed_rect;

  graph_rect.x = border;
  graph_rect.y = widget->allocation.height / 2 + border;
  graph_rect.height = widget->allocation.height / 2 - border * 2;
  graph_rect.width = widget->allocation.width - border * 2;

  hangup_rect.width = widget->allocation.width - border*3 - tx_rect.width;
  hangup_rect.height = widget->allocation.height - graph_rect.height - border*4 + 1;
  hangup_rect.y = border;
  hangup_rect.x = widget->allocation.width - border - hangup_rect.width;
  
  netmon->tx_rect = tx_rect;
  netmon->rx_rect = rx_rect;
  netmon->graph_rect = graph_rect;
  netmon->hangup_rect = hangup_rect;
  netmon->timecost_rect = timecost_rect;
  netmon->speed_rect = speed_rect;
}


static void 
gtk_netmon_size_allocate  (GtkWidget        *widget,
                           GtkAllocation    *allocation)
{
  GtkNetmon* netmon;

  g_return_if_fail(widget != NULL);
  g_return_if_fail(GTK_IS_NETMON(widget));
  
  netmon = GTK_NETMON(widget);

  widget->allocation = *allocation;

  if (GTK_WIDGET_REALIZED (widget))
    {
      gdk_window_move_resize (widget->window,
                              allocation->x, 
                              allocation->y,
                              allocation->width, 
                              allocation->height);

      if (netmon->buffer != NULL)
        gdk_pixmap_unref(netmon->buffer);

      netmon->buffer = gdk_pixmap_new(widget->window, allocation->width, 
                                      allocation->height, -1);

      g_assert(netmon->buffer != NULL);
    }

  if (netmon->panel_size >= 48)
    compute_rects_normal_layout (netmon);
  else if (netmon->panel_size < 48 &&
           netmon->orientation == GTK_NETMON_VERTICAL)
    compute_rects_micro_layout (netmon);
  else
    compute_rects_flat_layout (netmon);

  if (GTK_BIN (netmon)->child && GTK_WIDGET_VISIBLE (GTK_BIN (netmon)->child))
    {
      GtkAllocation child_allocation;

      child_allocation.x = netmon->hangup_rect.x;
      child_allocation.y = netmon->hangup_rect.y;
      
      child_allocation.width = MAX(1, netmon->hangup_rect.width);
      child_allocation.height = MAX(1, netmon->hangup_rect.height);

      gtk_widget_size_allocate (GTK_BIN (netmon)->child, &child_allocation);
    }
}

static void 
gtk_netmon_draw           (GtkWidget        *widget,
                           GdkRectangle     *area)
{
  GtkNetmon* netmon;

  g_return_if_fail(widget != NULL);
  g_return_if_fail(GTK_IS_NETMON(widget));

  netmon = GTK_NETMON(widget);

  gtk_netmon_paint(netmon, area);

  if (GTK_WIDGET_CLASS(parent_class)->draw)
    (* GTK_WIDGET_CLASS(parent_class)->draw) (widget, area);
}

static gint 
gtk_netmon_expose         (GtkWidget        *widget,
                           GdkEventExpose   *event)
{  
  gtk_netmon_paint(GTK_NETMON(widget), &event->area);

  if (GTK_WIDGET_CLASS(parent_class)->expose_event)
    (* GTK_WIDGET_CLASS(parent_class)->expose_event) (widget, event);

  return TRUE;
}

/* 
 * GtkNetmon-specific functions 
 */

/* this paint function is where all the work happens */
static void 
gtk_netmon_paint          (GtkNetmon            *netmon,
                           GdkRectangle     *area)
{
  GtkWidget* widget = GTK_WIDGET(netmon);
  const gint xthick = widget->style->klass->xthickness;
  const gint ythick = widget->style->klass->ythickness;

  GdkRectangle graph_rect = netmon->graph_rect;
  GdkRectangle tx_rect = netmon->tx_rect;
  GdkRectangle rx_rect = netmon->rx_rect;
  GdkRectangle timecost_rect = netmon->timecost_rect;
  GdkRectangle speed_rect = netmon->speed_rect;
  gchar time_str[64];
  gint hrs;
  gint mins;
  gint timecost_ascent;
  gint timecost_descent;
  gint speed_ascent;
  gint speed_descent;

  g_return_if_fail(netmon != NULL);
  g_return_if_fail(GTK_IS_NETMON(netmon));

  if (!GTK_WIDGET_DRAWABLE (widget))
    return;

  if (netmon->buffer == NULL)
    {
      g_assert(widget->window != NULL); /* we're drawable */
      netmon->buffer = gdk_pixmap_new(widget->window, 
                                      widget->allocation.width, 
                                      widget->allocation.height, -1);
    }
  
  /* work around pixmap theme... */
  gdk_draw_rectangle(netmon->buffer,
                     widget->style->bg_gc[widget->state],
                     TRUE,
                     area->x, area->y, area->width, area->height);

  /* non-broken themes should draw something in this case... */
  gtk_paint_flat_box(widget->style, netmon->buffer,
                     widget->state,
                     GTK_SHADOW_NONE,
                     area,
                     widget,
                     NULL,
                     area->x, area->y,
                     area->width, area->height);

  hrs = netmon->seconds_online / 3600;
  mins = (netmon->seconds_online % 3600) / 60;

  g_snprintf(time_str, 64, _("%d:%s%d"), hrs, 
             mins > 9 ? "" : "0",
             mins);

  {
    gdk_gc_set_clip_origin(widget->style->white_gc, tx_rect.x, tx_rect.y);

    if (netmon->tx)
      {
        gdk_gc_set_clip_mask(widget->style->white_gc, netmon->red_led_mask);
        
        gdk_draw_pixmap(netmon->buffer, widget->style->white_gc,
                        netmon->red_led_pixmap, 0, 0, tx_rect.x, tx_rect.y,
                        tx_rect.width, tx_rect.height);
      }
    else
      {
        gdk_gc_set_clip_mask(widget->style->white_gc, netmon->red_empty_led_mask);
        
        gdk_draw_pixmap(netmon->buffer, widget->style->white_gc,
                        netmon->red_empty_led_pixmap, 0, 0, tx_rect.x, tx_rect.y,
                        tx_rect.width, tx_rect.height);
      }

    gdk_gc_set_clip_mask(widget->style->white_gc, NULL);
  }

  {
    gdk_gc_set_clip_origin(widget->style->white_gc, rx_rect.x, rx_rect.y);

    if (netmon->rx)
      {
        gdk_gc_set_clip_mask(widget->style->white_gc, netmon->blue_led_mask);
        
        gdk_draw_pixmap(netmon->buffer, widget->style->white_gc,
                        netmon->blue_led_pixmap, 0, 0, rx_rect.x, rx_rect.y,
                        rx_rect.width, rx_rect.height);
      }
    else
      {
        gdk_gc_set_clip_mask(widget->style->white_gc, netmon->blue_empty_led_mask);
        
        gdk_draw_pixmap(netmon->buffer, widget->style->white_gc,
                        netmon->blue_empty_led_pixmap, 0, 0, rx_rect.x, rx_rect.y,
                        rx_rect.width, rx_rect.height);
      }

    gdk_gc_set_clip_mask(widget->style->white_gc, NULL);
  }

  if (graph_rect.width > 0 && graph_rect.height > 0)
    {
      /* Draw graph */
      gint pixel = 0;
      float rx_pixel_per_throughput = 0.0;
      float tx_pixel_per_throughput = 0.0;
      gint i = 0;
      /* reasonable smallest range for bps for an internet connection */
      guint max_rx_tp = 4000;
      guint max_tx_tp = 4000;

      gdk_gc_set_clip_rectangle(widget->style->black_gc,
                                &graph_rect);

      gdk_draw_rectangle(netmon->buffer, widget->style->black_gc,
                         TRUE, graph_rect.x, graph_rect.y, 
                         graph_rect.width, graph_rect.height);

      gdk_gc_set_clip_rectangle(widget->style->black_gc,
                                NULL);

      if (netmon->rxs_tput && netmon->txs_tput && netmon->throughput_count > 0)
        {
          while (i < netmon->throughput_count)
            {
              max_rx_tp = MAX(max_rx_tp, netmon->rxs_tput[i]);
              max_tx_tp = MAX(max_tx_tp, netmon->txs_tput[i]);
            
              ++i;
            }

          rx_pixel_per_throughput = (graph_rect.height / 2.0) / log(max_rx_tp+2);
          tx_pixel_per_throughput = (graph_rect.height / 2.0) / log(max_tx_tp+2);

          gdk_gc_set_clip_rectangle(netmon->blue_gc, &graph_rect);
          gdk_gc_set_clip_rectangle(netmon->red_gc, &graph_rect);

          pixel = graph_rect.x + graph_rect.width;
          i = netmon->throughput_count - 1;
          while (i >= 0 && pixel >= 0)
            {
              gint rx_h = (gint) (log(netmon->rxs_tput[i]+2)*rx_pixel_per_throughput);
              gint tx_h = (gint) (log(netmon->txs_tput[i]+2)*tx_pixel_per_throughput);
              /*
                if (i == netmon->throughput_count - 1)
                g_print("rx_h %d fm rxs_tput %d tx_h %d fm txs_tput %d\n",
                rx_h, netmon->rxs_tput[i], tx_h, netmon->txs_tput[i]);
              */

              if (netmon->rxs_tput[i] > 2)
                gdk_draw_line(netmon->buffer, netmon->blue_gc,
                              pixel, graph_rect.y+graph_rect.height-1, 
                              pixel, graph_rect.y+graph_rect.height-rx_h-1);

              if (netmon->txs_tput[i] > 2)
                gdk_draw_line(netmon->buffer, netmon->red_gc,
                              pixel, graph_rect.y+(graph_rect.height/2)-tx_h-1, 
                              pixel, graph_rect.y+(graph_rect.height/2)-1);

              --pixel;

              --i;
            }
        }

      gtk_paint_shadow (widget->style, netmon->buffer,
                        GTK_STATE_NORMAL, GTK_SHADOW_ETCHED_OUT,
                        &graph_rect, 
                        widget, "frame", /* we aren't a frame but would like to use its theme stuff */
                        graph_rect.x, graph_rect.y,
                        graph_rect.width, graph_rect.height);
      
    }


  if (timecost_rect.width > 0 && timecost_rect.height > 0)
    {
      const gchar* timecost_str = NULL;

      switch (netmon->style)
        {
        case GTK_NETMON_TIME:
          timecost_str = time_str;
          break;      
        case GTK_NETMON_COST:
          timecost_str = netmon->cost;
          break;
        default:
          g_assert_not_reached();
          break;
        }

      gdk_gc_set_clip_rectangle(widget->style->black_gc,
                                &timecost_rect);

      gdk_gc_set_clip_rectangle(widget->style->white_gc,
                                &timecost_rect);

      gdk_draw_rectangle(netmon->buffer, widget->style->black_gc,
                         TRUE, timecost_rect.x, timecost_rect.y, 
                         timecost_rect.width, timecost_rect.height);

      if (timecost_str)
        {
          gint lbearing, rbearing;

          gdk_string_extents(netmon->small_font, timecost_str,
                             &lbearing, &rbearing, NULL, 
                             &timecost_ascent, &timecost_descent);
       
          gdk_draw_string(netmon->buffer, netmon->small_font, widget->style->white_gc,
                          timecost_rect.x + timecost_rect.width - xthick - 1 -
                          lbearing - rbearing,
                          timecost_rect.y + timecost_ascent + ythick,
                          timecost_str);
        }

      gdk_gc_set_clip_rectangle(widget->style->black_gc,
                                NULL);

      gdk_gc_set_clip_rectangle(widget->style->white_gc,
                                NULL);

#if 0
      gtk_paint_shadow (widget->style, netmon->buffer,
                        GTK_STATE_NORMAL, GTK_SHADOW_IN,
                        &timecost_rect, 
                        widget, "frame", /* we aren't a frame but would like to use its theme stuff */
                        timecost_rect.x, timecost_rect.y,
                        timecost_rect.width, timecost_rect.height);
#endif
    }

  if (speed_rect.width > 0 && speed_rect.height > 0)    
    {
      gdk_gc_set_clip_rectangle(widget->style->black_gc,
                                &speed_rect);

      gdk_gc_set_clip_rectangle(widget->style->white_gc,
                                &speed_rect);

      gdk_draw_rectangle(netmon->buffer, widget->style->black_gc,
                         TRUE, speed_rect.x, speed_rect.y, 
                         speed_rect.width, speed_rect.height);

      if (netmon->speed)
        {
          gint lbearing, rbearing;

          gdk_string_extents(netmon->small_font, netmon->speed,
                             &lbearing, &rbearing, NULL, 
                             &speed_ascent, &speed_descent);
       
          gdk_draw_string(netmon->buffer, netmon->small_font, widget->style->white_gc,
                          speed_rect.x + speed_rect.width - xthick - 1 -
                          lbearing - rbearing,
                          speed_rect.y + speed_ascent,
                          netmon->speed);
        }

      gdk_gc_set_clip_rectangle(widget->style->black_gc,
                                NULL);

      gdk_gc_set_clip_rectangle(widget->style->white_gc,
                                NULL);

#if 0
      gtk_paint_shadow (widget->style, netmon->buffer,
                        GTK_STATE_NORMAL, GTK_SHADOW_IN,
                        &speed_rect, 
                        widget, "frame", /* we aren't a frame but would like to use its theme stuff */
                        speed_rect.x, speed_rect.y,
                        speed_rect.width, speed_rect.height);
#endif
    }

  if ( (timecost_rect.width > 0 && timecost_rect.height > 0) ||
       (speed_rect.width > 0 && speed_rect.height > 0) )
    {
      GdkRectangle text_rect = {timecost_rect.x, timecost_rect.y, 
                                timecost_rect.width, 
                                speed_rect.y + speed_rect.height - timecost_rect.y };

      gtk_paint_shadow (widget->style, netmon->buffer,
                        GTK_STATE_NORMAL, GTK_SHADOW_ETCHED_OUT,
                        &text_rect, 
                        widget, "frame", /* we aren't a frame but would like to use its theme stuff */
                        text_rect.x, text_rect.y,
                        text_rect.width, text_rect.height);
    }
  

  if (GTK_WIDGET_HAS_FOCUS (widget))
    {
      gtk_paint_focus (widget->style, netmon->buffer,
                       area, widget, "netmon",
                       widget->allocation.x, widget->allocation.y, 
                       widget->allocation.width-1, widget->allocation.height-1);
    }

  gdk_gc_set_clip_rectangle(widget->style->black_gc, area);

  gdk_draw_pixmap(widget->window, widget->style->black_gc,
                  netmon->buffer, area->x, area->y, area->x, area->y,
                  area->width, area->height);

  gdk_gc_set_clip_rectangle(widget->style->black_gc, NULL);
  
}

void            
gtk_netmon_set_seconds_online (GtkNetmon* netmon, guint secs)
{
  g_return_if_fail(netmon != NULL);
  g_return_if_fail(GTK_IS_NETMON(netmon));

  netmon->seconds_online = secs;
  
  gtk_widget_queue_draw(GTK_WIDGET(netmon));
}

void
gtk_netmon_set_cost           (GtkNetmon* netmon, const gchar* cost)
{
  g_return_if_fail(netmon != NULL);
  g_return_if_fail(GTK_IS_NETMON(netmon));

  if (netmon->cost != NULL)
    g_free(netmon->cost);

  netmon->cost = cost ? g_strdup(cost) : NULL;

  gtk_widget_queue_draw(GTK_WIDGET(netmon));
}

void
gtk_netmon_set_speed          (GtkNetmon* netmon, const gchar* speed)
{
  g_return_if_fail(netmon != NULL);
  g_return_if_fail(GTK_IS_NETMON(netmon));


  if (netmon->speed != NULL)
    g_free(netmon->speed);

  netmon->speed = speed ? g_strdup(speed) : NULL;

  gtk_widget_queue_draw(GTK_WIDGET(netmon));
}

void            
gtk_netmon_set_throughput     (GtkNetmon* netmon, 
                               const guint* rxs, 
                               const guint* txs,
                               guint count)
{
  g_return_if_fail(netmon != NULL);
  g_return_if_fail(GTK_IS_NETMON(netmon));

  if (count != netmon->throughput_count)
    {
      if (netmon->rxs_tput)
        g_free(netmon->rxs_tput);
      if (netmon->txs_tput)
        g_free(netmon->txs_tput);

      netmon->rxs_tput = g_new(guint, count);
      netmon->txs_tput = g_new(guint, count);      
    }

  if (rxs != NULL)
    {
      g_return_if_fail(count > 0);
      g_return_if_fail(txs != NULL);

      netmon->throughput_count = count;
      
      memcpy(netmon->rxs_tput, rxs, count*sizeof(guint));
      memcpy(netmon->txs_tput, txs, count*sizeof(guint));
    }
  else
    {
      netmon->rxs_tput = NULL;
      netmon->txs_tput = NULL;
      netmon->throughput_count = 0;
    }

  gtk_widget_queue_draw(GTK_WIDGET(netmon));
}

void
gtk_netmon_set_online         (GtkNetmon* netmon, gboolean online)
{
  g_return_if_fail(netmon != NULL);
  g_return_if_fail(GTK_IS_NETMON(netmon));

  netmon->online = online;

  if (!netmon->online)
    {
      gtk_netmon_set_transmitting (netmon, FALSE);
      gtk_netmon_set_receiving (netmon, FALSE);
    }
  
  update_button_state (netmon);
  
  gtk_widget_queue_draw(GTK_WIDGET(netmon));
}

void
gtk_netmon_set_transmitting   (GtkNetmon* netmon, gboolean tx)
{
  g_return_if_fail(netmon != NULL);
  g_return_if_fail(GTK_IS_NETMON(netmon));

  if (tx) netmon->tx = TRUE;
  else netmon->tx=FALSE;

  gtk_widget_queue_draw(GTK_WIDGET(netmon));
}

void
gtk_netmon_set_receiving      (GtkNetmon* netmon, gboolean rx)
{
  g_return_if_fail(netmon != NULL);
  g_return_if_fail(GTK_IS_NETMON(netmon));

  if (rx) netmon->rx = TRUE;
  else netmon->rx=FALSE;

  gtk_widget_queue_draw(GTK_WIDGET(netmon));
}

void            
gtk_netmon_set_display_style  (GtkNetmon* netmon, GtkNetmonDisplayStyle style)
{
  g_return_if_fail(netmon != NULL);
  g_return_if_fail(GTK_IS_NETMON(netmon));

  netmon->style = style;

  gtk_widget_queue_draw(GTK_WIDGET(netmon));
}

void
gtk_netmon_set_has_disconnect (GtkNetmon* netmon, gboolean setting)
{
  if (!(netmon->disconnect_button) == !(setting))
    return;

  netmon->disconnect_button = setting ? TRUE : FALSE;

  if (netmon->disconnect_button)
    {
      GtkWidget* button;

      g_return_if_fail(GTK_BIN(netmon)->child == NULL);

      button = gtk_button_new ();

      gtk_container_add (GTK_CONTAINER (button),
                         gtk_widget_new (GTK_TYPE_PIXMAP, NULL));
      
      gtk_container_add(GTK_CONTAINER(netmon), button);
      
      gtk_signal_connect(GTK_OBJECT(button), "clicked",
                         GTK_SIGNAL_FUNC(emit_clicked_cb),
                         netmon);
      
      update_button_state (netmon);
      
      gtk_widget_show_all(button);
    }
  else
    {
      gtk_container_remove(GTK_CONTAINER(netmon), GTK_BIN(netmon)->child);
      GTK_BIN(netmon)->child = NULL;
      /* button is implicitly destroyed */
    }

  gtk_widget_queue_resize(GTK_WIDGET(netmon));
}

void
gtk_netmon_set_orientation (GtkNetmon* netmon,
                            GtkNetmonOrientation orientation)
{
  if (netmon->orientation == orientation)
    return;

  netmon->orientation = orientation;

  gtk_widget_queue_resize (GTK_WIDGET (netmon));
}

void
gtk_netmon_set_size (GtkNetmon* netmon,
                     gint size)
{
  if (netmon->panel_size == size)
    return;

  netmon->panel_size = size;

  gtk_widget_queue_resize (GTK_WIDGET (netmon));
}

