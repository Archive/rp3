Summary: The Red Hat graphical PPP management tool.
Name: rp3
Version: 1.1.10
Release: 1
License: GPL
Group: Applications/System
Source: rp3-%{version}.tar.gz
Requires: ppp >= 2.3.9, initscripts >= 5.40, wvdial >= 1.40-5, usermode >= 1.13, /etc/pam.d/system-auth
Obsoletes: usernet
BuildRoot: %{_tmppath}/%{name}-root

%description
The rp3 program provides an easy-to-use interface for configuring
PPP connections, and for activating and monitoring any type of
network configuration.  rp3 includes a GNOME panel applet,
a more highly functional replacement for usernet, and a graphical
configuration tool that takes you through the process of setting up
a PPP connection from start to finish, one step at a time.

%prep
%setup -q

%build
%configure --sysconfdir=/etc
make

%install
[ "$RPM_BUILD_ROOT" != "/" ] && rm -rf $RPM_BUILD_ROOT
%{makeinstall}
%find_lang %name

%clean
[ "$RPM_BUILD_ROOT" != "/" ] && rm -rf $RPM_BUILD_ROOT

%files -f %{name}.lang
%defattr(-,root,root)
%{_bindir}/rp3
%{_bindir}/usernet
%{_bindir}/rp3-config
%{_sbindir}/rp3-config
%{_datadir}/gnome/help/*/*/*
%{_datadir}/applets/*/*
%{_datadir}/pixmaps/rp3
%config %{_sysconfdir}/X11/applnk/*/*
%config %{_sysconfdir}/CORBA/servers/*
%config(noreplace) /etc/security/console.apps/*
%config(noreplace) /etc/pam.d/*
%config %{_libdir}/desktop-links/*

%changelog
* Fri Apr  6 2001 Nalin Dahyabhai <nalin@redhat.com>
- don't attempt to query a widget after it's already been destroyed (#34531)

* Tue Apr  3 2001 Nalin Dahyabhai <nalin@redhat.com>
- generate the list of non-clone interfaces correctly (#34531)

* Mon Feb 26 2001 Nalin Dahyabhai <nalin@redhat.com>
- attempt to handle non-readable ifcfg files more correctly by assuming
  that we can't manipulate them (#29048)

* Fri Feb 23 2001 Trond Eivind Glomsr�d <teg@redhat.com>
- langify

* Mon Feb 12 2001 Nalin Dahyabhai <nalin@redhat.com>
- time.h vs. sys/time.h sorted out by Tim Waugh
- pull in latest translations

* Wed Feb  7 2001 Nalin Dahyabhai <nalin@redhat.com>
- fix segfault on non-readable ifcfg files (#24535, #25060)
- fix a compile problem on newer glibc: use <time.h> instead of <sys/time.h>
- modify local copy of dave-html.dsl to work with the new SGML environment
  in Fisher

* Tue Dec 12 2000 Nalin Dahyabhai <nalin@redhat.com>
- Use FHS macros everywhere.
- Add patch for Czech translations from Milan Kerslager (#21733)
- Default demand-dial to OFF.  It's completely counter-intuitive.
- Fix the progress-bar update bug, again, I think.

* Wed Aug 30 2000 Nalin Dahyabhai <nalin@redhat.com>
- Add Swedish (sv) translations in rp3*.desktop and usernet.desktop (#15330)
- Add more translations from CVS.

* Wed Aug 23 2000 Jonathan Blandford <jrb@redhat.com>
- Re-add lost patch to make rp3 install in the desktop.  
  Also, up the release.

* Mon Aug 23 2000 Nalin Dahyabhai <nalin@redhat.com>
- save PAP information from the configuration dialog (#16640)

* Mon Aug 21 2000 Nalin Dahyabhai <nalin@redhat.com>
- save PAP information before we exit if we're in chat mode (#16640)

* Mon Aug  7 2000 Nalin Dahyabhai <nalin@redhat.com>
- require a new-enough initscripts to make sure demand-dialing works
- exit more gracefully if no interface is specified (#15087)

* Fri Aug  4 2000 Nalin Dahyabhai <nalin@redhat.com>
- fix detection of whether demand mode is enabled or not (#15282,#15389)

* Thu Aug  3 2000 Nalin Dahyabhai <nalin@redhat.com>
- quote all shell metacharacters and control operators (#15211)
- add option to control PEERDNS (#15206)

* Mon Jul 31 2000 Jonathan Blandford <jrb@redhat.com>
- mark static string as N_ instead of _.

* Wed Jul 12 2000 Havoc Pennington <hp@redhat.com>
- Hack UI to work with new panel (multiple panel sizes)

* Mon Jul 10 2000 Nalin Dahyabhai <nalin@redhat.com>
- change tooltip in main dialog window to say "Hang up" instead of "Dial" when
  the connection is up (#12798)
- add pt_BR to the list of languages (thanks to Gustavo Maciel Dias Vieira)

* Tue Jul  2 2000 Jakub Jelinek <jakub@redhat.com>
- Rebuild with new C++

* Mon Jun 19 2000 Nalin Dahyabhai <nalin@redhat.com>
- finish support for demand-dialing
- add fixes from Kjartan Maraas for missing config.h includes which mess up i18n

* Fri Jun 16 2000 Nalin Dahyabhai <nalin@redhat.com>
- integrate the beginnings of support for demand-dialing
- resync with wvdial package
- don't assume we have < 100 serial devices (you never know)

* Thu Jun  8 2000 Nalin Dahyabhai <nalin@redhat.com>
- integrate German translations from Kai Lahmann

* Thu Jun  1 2000 Nalin Dahyabhai <nalin@redhat.com>
- modify PAM setup to use system-auth

* Wed May 17 2000 Michael K. Johnson <johnsonm@redhat.com>
- Version 1.1
- Cumulative cost estimation
- Several small fixes, including documentation

* Thu Apr 20 2000 Nalin Dahyabhai <nalin@redhat.com>
- fix shvar test suite to match shvar

* Tue Apr 18 2000 Nalin Dahyabhai <nalin@redhat.com>
- fix from Svante Signell for compiling with newer C++ compilers
- other fixes for newer version of GCC

* Thu Apr 13 2000 Nalin Dahyabhai <nalin@redhat.com>
- make sure that too much information from wvdial doesn't crash rp3-config

* Wed Feb  9 2000 Nalin Dahyabhai <nalin@redhat.com>
- fix new crashing bug in rp3-config/adddruid.cc

* Tue Feb  8 2000 Nalin Dahyabhai <nalin@redhat.com>
- fix too-many-open-files bug in utils/interface.c
- make all files under /etc config files

* Tue Feb  1 2000 Nalin Dahyabhai <nalin@redhat.com>
- properly save and interpret confirmStart and confirmStop
- fix the session manager "save yourself" wait problem
- clean up configuration file handling
- update sysconfig files when we update wvdial.conf

* Thu Jan 27 2000 Nalin Dahyabhai <nalin@redhat.com>
- integrate wvdial-1.41 and fix the "call" problem with pppd

* Tue Jan 18 2000 Nalin Dahyabhai <nalin@redhat.com>
- rewrite logger dialog to get rid of warnings and other problems

* Wed Dec 08 1999 Michael K. Johnson <johnsonm@redhat.com>
- control persistence
- delete wvdial sections when deleting account so names can be reused
- fixed various segvs
- bring clone devices up and down correctly
- unsigned/signed conversion fix for graph display

* Sat Nov 13 1999 Michael K. Johnson <johnsonm@redhat.com>
- fixed SIGILL on alpha
- made more robust when encountering bad data files

* Mon Nov 01 1999 Michael K. Johnson <johnsonm@redhat.com>
- segfault when adding new modem fixed
- danish translation
- fix dialog box flashing
- ignore non-rp3 configurations in rp3-config
- fixed SIGFPE on sparc, alpha

* Sat Sep 25 1999 Michael K. Johnson <johnsonm@redhat.com>
- Version 1.0
- moved menu files to applnk directories
- norwegian translation
- --swallow

* Thu Sep 23 1999 Michael K. Johnson <johnsonm@redhat.com>
- volume setting option
- terminate on close
- onboot syntax fixed
- yet another stupid mode fix
- debug connection works even if ppp comes up successfully
- usernet/rp3-config device confusion problem fixed

* Wed Sep 22 1999 Michael K. Johnson <johnsonm@redhat.com>
- help tied into rp3-config
- corrected "Stupid mode" and USERCTL interpretation
- if iface is down but manipulatble when chosen interactively at start time,
  offer to start it
- changed default size of rp3 application, allow larger graph
- rp3-config desktop entry

* Mon Sep 20 1999 Michael K. Johnson <johnsonm@redhat.com>
- require latest usermode for consolehelper that sets $HOME
- DNS management bugs fixed
- USERCTL set right
- Stupid mode last bug stomped (famous last words...)

* Sun Sep 19 1999 Michael K. Johnson <johnsonm@redhat.com>
- no longer segfaults if rp3's Preferences->Apply is clicked more
  than once
- wvdial's "Stupid mode" is now set correctly

* Fri Sep 17 1999 Michael K. Johnson <johnsonm@redhat.com>
- New version 0.14
- rp3-config can deal with multiple modems better, has advanced
  options for further configurability
- added dependency on newer version of wvdial so that we get the
  "Inherits" implementation in wvdial.conf

* Tue Sep 14 1999 Michael K. Johnson <johnsonm@redhat.com>
- added desktop entry and wmconfig entry for usernet
- added the help files to the filelist
- added usernet desktop and (legacy) wmconfig files

* Mon Sep 13 1999 Michael K. Johnson <johnsonm@redhat.com>
- version 0.13 creates all tested account types properly
- rp3 now runs child processes asynchronously when possible,
  allowing rp3 to handle expose events.
- requires new initscripts and wvdial for PAP/CHAP to work.
- fixed fd leaks
- rp3-config can now log debugging dialing and save it to a file

* Fri Sep 10 1999 Michael K. Johnson <johnsonm@redhat.com>
- version 0.12 has fully working panel applet that saves session
  properly.

* Wed Sep 08 1999 Michael K. Johnson <johnsonm@redhat.com>
- version 0.10 requires even newer initscripts so that ifdown is
  also synchronous (modulo timeouts) and squashes the annoying
  multi-dialog-popup bug

* Tue Sep 07 1999 Michael K. Johnson <johnsonm@redhat.com>
- version 0.9 should really work.  Requires even newer initscripts
  than before so that ifup ppp<n> is synchronous -- that is, it will
  not return until the device is up or the operation has failed.

* Fri Aug 27 1999 Michael K. Johnson <johnsonm@redhat.com>
- built initial package with real contents

* Thu Aug 19 1999 Michael K. Johnson <johnsonm@redhat.com>
- created stub package for documentation purposes
