#
# rules.mk:  1999 04 10
#
# Copyright (C) 1998, 1999 by Avery Pennarun <apenwarr@worldvisions.ca>.
#   Use, distribute, modify, and redistribute freely.  (But if you're nice,
#   you'll send all your changes back to me.)
#
# This is a complicated-looking set of Makefile rules that should make your
# own Makefiles simpler, by providing for several useful features (like
# autodependencies and a 'clean' target) without any extra effort.
#
# It will only work with GNU make.
#

STRIP=strip --remove-section=.note --remove-section=.comment
#STRIP=echo

#
# Define DEBUG flag if not already set.
#
ifeq ($(DEBUG),)
DEBUG=1
endif

#
# Point to the electric fence library, assuming DEBUG=1.
#
ifeq ($(DEBUG),1)
EFENCE=-lefence
else
EFENCE=
endif

#
# C compilation flags (depends on DEBUG setting)
#
CPPFLAGS = $(CPPOPTS)
CFLAGS = $(COPTS) -Wall -D_BSD_SOURCE -D_GNU_SOURCE
CXXFLAGS = $(CXXOPTS)
LDFLAGS = $(LDOPTS)

ifeq ($(DEBUG),1)
CFLAGS += -g -Wall -DDEBUG=1
CXXFLAGS +=
LDFLAGS += -g
else
CFLAGS += -g $(RPM_OPT_FLAGS) -DDEBUG=0 -DNDEBUG
#CFLAGS += -fomit-frame-pointer  # really evil
#CXXFLAGS += -fno-implement-inlines  # causes trouble with egcs 1.0
#CXXFLAGS += -fno-rtti -fno-exceptions # Breaks apps that don't use these flags
#LDFLAGS += -g
endif

# we need a default rule, since the 'include' below causes trouble
default: all

# any rule that depends on FORCE will always run
.PHONY: FORCE
FORCE:

%.gz: FORCE %
	@rm -f $@
	gzip -f $*
	@ls -l $@

ALLDIRS = $(XPATH)
#VPATH = $(shell echo $(ALLDIRS) | sed 's/[ 	][ 	]*/:/g')
INCFLAGS = $(addprefix -I,$(ALLDIRS))

#
# Typical compilation rules.
#
override _R_CFLAGS=$(CPPFLAGS) $(CFLAGS) $(INCFLAGS) -MMD
override _R_CXXFLAGS=$(CPPFLAGS) $(CFLAGS) $(CXXFLAGS) $(INCFLAGS) -MMD
export _R_CFLAGS
export _R_CXXFLAGS

%.o: %.c
	@rm -f .$*.d $@
	$(CC) $(_R_CFLAGS) -c $<
	@mv $*.d .$*.d

%.o: %.cc
	@rm -f .$*.d $@
	$(CXX) $(_R_CXXFLAGS) -c $<
	@mv $*.d .$*.d

../%.a:
	@echo "Library $@ does not exist!"; exit 1

# we need to do some magic in order to make a .a file that contains another
# .a file.  Otherwise this would be easy...
%.a:
	rm -f $@
	@set -e; \
	SIMPLE_OBJS=$$(ls $^ | cat | sed 's/^.*\.[^o]$$//'); \
	AR_FILES=$$(ls $^ | cat | sed 's/^.*\.[^a]$$//'); \
	echo ar q $@ $$SIMPLE_OBJS; \
	ar q $@ $$SIMPLE_OBJS; \
	for d in $$AR_FILES; do \
		echo "ar q $@ $$d (magic library merge)"; \
		BASE=$$(basename $$d); \
		for e in $$(ar t $$d); do \
			OBJNAME="ar_$${BASE}_$$e"; \
			ar p $$d $$e >$$OBJNAME; \
			ar q $@ $$OBJNAME; \
			rm -f $$OBJNAME; \
		done; \
	done
	ranlib $@

%: %.o
	$(CC) $(LDFLAGS) -o $@ $^ $(LIBS) $($*-LIBS)

# Force objects to be built before final binaries	
$(addsuffix .o,$(basename $(wildcard *.c) $(wildcard *.cc))):

#
# Automatically generate header dependencies for .c and .cc files.  The
# dependencies are stored in the file ".depend"
#

depfiles_sf = $(wildcard .*.d)

ifneq ($(depfiles_sf),)
-include $(depfiles_sf)
endif


#
# A macro for compiling subdirectories listed in the SUBDIRS variable.
# Tries to make the target ($@) in each subdir, unless the target is called
# "subdirs" in which case it makes "all" in each subdir.
#
define subdirs
	@OLDDIR="$$PWD"; set -e; \
	for d in __fx__ ${SUBDIRS}; do \
		if [ "$$d" = "__fx__" ]; then continue; fi; \
		cd "$$d"; \
		echo ; \
		echo "--> Making $(subst subdirs,all,$@) in $$PWD..."; \
		${MAKE} --no-print-directory $(subst subdirs,all,$@); \
		cd "$$OLDDIR"; \
	done
	@echo
	@echo "--> Back in $$PWD..."
endef

subdirs:
	$(subdirs)


#
# Auto-clean rule.  Feel free to append to this in your own directory, by
# defining your own "clean" rule.
#
clean: FORCE cleanrule

cleanrule: FORCE
	rm -f *~ *.o *.a *.d .*.d .depend

#
# Make 'tags' file using the ctags program - useful for editing
#
#tags: $(shell find -name '*.cc' -o -name '*.[ch]')
#	@echo '(creating "tags")'
#	@if [ -x /usr/bin/ctags ]; then /usr/bin/ctags $^; fi
