.TH WVDIAL 1 "August 1998" Worldvisions
.\" NAME should be all caps, SECTION should be 1-8, maybe w/ subsection
.\" other parms are allowed: see man(7), man(1)

.SH NAME
wvdial \- PPP dialer with built-in intelligence.

.SH SYNOPSIS
.B wvdial
.I "--help | --version | section..."

.SH DESCRIPTION
.B wvdial
is an intelligent PPP dialer, which means that it dials a modem and
starts PPP in order to connect to the Internet.  It is something like the
.BR chat (8)
program, except that it uses heuristics to guess how to dial and log
into your server rather than forcing you to write a login script.
.PP
When
.B wvdial
starts, it first loads its configuration from
.IR /etc/wvdial.conf ,
which contains basic information about the modem port, speed, and init
string, along with information about your Internet Service Provider (ISP),
such as the phone number, your username, and your password.
.PP
Then it initializes your modem and dials the server and waits for a 
connection (a CONNECT string from the modem).  It understands and responds 
to typical connection problems (like BUSY and NO DIALTONE).
.PP
Any time after connecting, 
.B wvdial
will start PPP if it sees a PPP sequence from the server.  Otherwise,
it tries to convince the server to start PPP by doing the following:
.PP
.HP
\ - responding to any login/password prompts it sees;
.br
.HP
\ - interpreting "choose one of the following"-style menus;
.br
.HP
\ - eventually, sending the word "ppp" (a common terminal server command).
.PP
If all of this fails,
.B wvdial
just runs
.BR pppd (8)
and hopes for the best.

.SH OPTIONS
Three options are recognized by
.BR wvdial .
.TP
.I "\-\-chat"
Run wvdial as a chat replacement from within pppd, instead of the more
normal method of having wvdial negotiate the connection and then call
pppd.
.TP
.I "\-\-help"
Prints a short message describing how to use
.B wvdial
and exits.
.TP
.I "\-\-version"
Displays
.BR wvdial 's
version number and exits.
.PP
.B wvdial
is normally run without command line options, in which case it reads
its configuration from the
.I "[Dialer Defaults]"
section of
.IR /etc/wvdial.conf .
(The configuration file is described in more detail later.)
.PP
One or more sections of
.I
/etc/wvdial.conf
may be specified on the command line.  Settings in these sections will
override settings in
.IR "[Dialer Defaults]" .
.TP
For example, the command:
.B wvdial phone2
.PP
will read default options from the
.I "[Dialer Defaults]"
section, then override any or all of the options with those found in the
.I "[Dialer phone2]"
section.
.PP
If more than one section is specified, they are processed in the order they
are given.  Each section will override all the sections that came before it.
.TP
For example, the command:
.B wvdial phone2 pulse shh
.PP
will read default options from the
.I "[Dialer Defaults]"
section, then override any or all of the options with those found in the
.I "[Dialer phone2]"
section, followed by the
.I "[Dialer pulse]"
section, and lastly the
.I "[Dialer shh]"
section.
.PP
Using this method, it is possible to easily configure
.B wvdial
to switch between different internet providers, modem init strings,
account names, and so on without specifying the same configuration
information over and over.

.SH CONFIGURATION FILE
The configuration file 
.I /etc/wvdial.conf
is in Windows "ini" file format, with
.B sections
named in square brackets and a number of
.B variable = value
pairs within each section.

Here is a sample configuration file:
.PP
.RS
[Dialer Defaults]
.br
Modem = /dev/ttyS2
.br
Baud = 57600
.br
Init = ATZ
.br
Init2 = AT S11=50
.br
Phone = 555-4242
.br
Username = apenwarr
.br
Password = my-password
.br
.br

[Dialer phone2]
.br
Phone = 555-4243
.br
.br

[Dialer shh]
.br
Init3 = ATM0
.br
.br

[Dialer pulse]
.br
Dial Command = ATDP
.RE
.PP
The sample configuration file above contains all of the options
necessary to run the two sample command lines given above.  Here is a
complete list of settings that
.B wvdial
understands:
.TP
.I Inherits
Normally, all sections inherit from the
.I "[Dialer Defaults]"
section.  You can make a section explicitly inherit from another
section by naming it in a 
.I Inherits = InheritedSection
line.
.TP
.I Modem
The location of the device that
.B wvdial
should use as your modem.  The default is
.BR /dev/modem .
.TP
.I Baud
The speed at which
.B wvdial
will communicate with your modem.  The default is 57600 baud.
.TP
.I "Init1 ... Init9"
.B wvdial
can use up to nine initialization strings to set up your modem.  Before 
dialing, these strings are sent to the modem in numerical order.  These are
particularly useful when specifying multiple sections.  See above for an
example that uses Init3 to turn the modem's speaker off.  The default is 
"ATZ" for Init1.
.TP
.I Phone
The phone number you want
.B wvdial
to dial.
.TP
.I Area Code
.B wvdial
inserts the area code between the dial prefix and the phone number.
In general, you will probably wish to leave this out for dialing
from home and put into a
.I Area Code
string any additional characters you may need to dial remotely.
In most of North America, your
.I Phone
entry will be 7 digits (555-1234) and your
.I Area Code
will be
.B 4
digits (1-818).  In local areas with multiple area codes, your
.I Phone
entry will be 10 digits (818-555-1234) and your
.I Area Code
will be 1 digit (1).  In most of europe, your
.I Phone
entry will not include your city code, and you might want several
different sections with different "\fIArea Code\fP" entries; one
with your city code, another with your country code and city code,
or whatever else you need to do.
.TP
.I Dial Prefix
.B wvdial
will insert this string after the dial command and before the phone number
(including area code, if any).
For example, to disable call waiting (in North America, anyway) set
this to "*70".  Or in a hotel, you might have to use "9" to get an outside
line.  A "," character is automatically appended to the dial prefix, which
causes a slight pause in dialing.
.TP
.I Dial Command
.B wvdial
will use this string to tell the modem to dial.  The default is "ATDT".
.TP
.I Login
You must set this to the username you use at your ISP.
.TP
.I Login Prompt
If your ISP has an unusual login procedure that
.B wvdial
is unable to figure out, you can use this option to specify a
login prompt.  When this prompt is received from your ISP, 
.B wvdial
will send the
.B Login
string.
.TP
.I Password
You must set this to the password you use at your ISP.
.TP
.I Password Prompt
If your ISP has an unusual login procedure that
.B wvdial
is unable to figure out, you can use this option to specify a
password prompt.   When this prompt is received from you ISP,
.B wvdial
will send the
.B Password
string.
.TP
.I PPPD Path
If your system has pppd somewhere other than
.BR "/usr/sbin/pppd" ,
you will need to set this option.
.TP
.I Force Address
This option only applies if you have a static IP address at your ISP, and
even then you probably don't need it.  Some ISP's don't send the IP address
as part of the PPP negotiation.  This option forces pppd to use the address
you give it.
.TP
.I Remote Name
If your ISP uses PAP or CHAP authentication, you might need to change this
to your ISP's authentication name.  In most cases, however, it's safe to use 
the default value, "*".
.TP
.I Carrier Check
.B wvdial
checks your modem during the connection process to ensure that it is actually
online.  If you have a weird modem that insists its carrier line is always
down, you can disable the carrier check by setting this option to "no".
.TP
.I Stupid Mode
When
.B wvdial
is in Stupid Mode, it does not attempt to interpret any prompts from the
terminal server.  It starts pppd immediately after the modem connects.
Apparently there are ISP's that actually give you a login prompt, but
work only if you start PPP, rather than logging in.  Go figure.  Stupid
Mode is (naturally) disabled by default.
.TP
.I New PPPD
If you have pppd version 2.3.0 or newer, you'll need to enable this, because
new versions require a file 
.BR /etc/ppp/peers/wvdial .
Enabling this options tells pppd to look for this file.
.TP
.I Default Reply
When
.B wvdial
detects a prompt, and it hasn't seen any clues that indicate what it should
send as a response to the prompt, it defaults to sending "ppp".  Sometimes
this is inadequate.  Use this option to override
.BR wvdial 's
default prompt response.
.TP
.I Auto Reconnect
If enabled,
.B wvdial
will attempt to automatically reestablish a connection if you are
randomly disconnected by the other side.
This option is "on" by default.
.PP
The
.BR wvdialconf (1)
program can be used to detect your modem and fill in the Modem, Baud,
and Init/Init2 options automatically.

.SH BUGS
"Intelligent" programs are frustrating when they don't work right.
This version of
.B wvdial
has only minimal support for disabling or overriding its "intelligence",
with the "Stupid Mode", "Login Prompt", and "Password Prompt" options.
So, in general if you have a nice ISP, it will probably work,
and if you have a weird ISP, it might not.
.PP
Still, it's not much good if it doesn't work for you, right?  Don't be fooled
by the fact that
.B wvdial
finally made it to version 1.00; it could well contain many bugs and 
misfeatures.  Let us know if you have problems by sending e-mail to
.BR <wvdial@worldvisions.ca> .
.PP
Also, there is now a mailing list for discussion about
.BR wvdial .
If you are having problems, or have anything else to say, send e-mail to
.BR <wvdial-list@worldvisions.ca> .
.PP
You may encounter some error messages if you don't have write access to
.B /etc/ppp/pap-secrets
and
.BR /etc/ppp/chap-secrets .
Unfortunately, there's really no nice way around this yet.
.SH FILES
.TP
/etc/wvdial.conf
Configuration file which contains modem, dialing, and login
information.
.TP
/dev/ttyS*
Serial port devices.
.TP
/etc/ppp/peers/wvdial
Required for correct authentication in pppd version 2.3.0 or newer.
.TP
/etc/ppp/{pap,chap}-secrets
Contains a list of usernames and passwords used by pppd for authentication.
.B wvdial
maintains this list automatically.

.SH AUTHORS
Dave Coombs and Avery Pennarun for Worldvisions Computer Technology, as
part of the Worldvisions Weaver project.

.SH SEE ALSO
.BR wvdialconf (1),
.BR pppd (8),
.BR chat (8)
