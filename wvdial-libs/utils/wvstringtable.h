/*
 * Worldvisions Weaver Software:
 *   Copyright (C) 1997-1999 Worldvisions Computer Technology, Inc.
 *
 * WvStrings are used a lot more often than WvStringTables, so the Table need
 * not be defined most of the time.  Include this file if you need it.
 *
 */

#ifndef __WVSTRINGTABLE_H
#define __WVSTRINGTABLE_H

#include "wvstring.h"
#include "wvhashtable.h"

DeclareWvTable(WvString);

#endif // __WVSTRINGTABLE_H
