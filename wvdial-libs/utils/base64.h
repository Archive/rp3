/*
 * Worldvisions Weaver Software:
 *   Copyright (C) 1997, 1998, 1999 Worldvisions Computer Technology, Inc.
 *
 * Functions for encoding and decoding strings in MIME's Base64 notation.
 *
 */

#ifndef __BASE64_H
#define __BASE64_h

extern char * base64_encode( char * str );
extern char * base64_decode( char * str );

#endif __BASE64_H
