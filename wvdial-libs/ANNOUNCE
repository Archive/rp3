
Introducing WvDial
==================

After eight months of testing, bugfixes, and feedback from our fearless and
patient users, Worldvisions WvDial has finally made it to version 1.00!

WvDial is a programming library and command-line tool that dials your modem
to connect to an ISP, logs in, and runs pppd.  You've heard of that before
-- but unlike every other Linux solution we know of, WvDial completely
avoids the need for chat scripts.  It was designed to make the (previously
rather complicated) configuration of Linux dial-up networking as simple as
phone number, username, password.

WvDial is and will remain totally free (under the GNU Library General Public
License, or LGPL), and we hope that all Linux distributions will try to
integrate it with their systems.


Features
========

WvDial 1.00 can:

    - automatically detect your internal or external modem on any of
	your serial ports, figure out its maximum baud rate, and choose
	a valid initialization string.
	
    - dial your internet provider and log in automatically, without
	a handmade login script. (This is based on the knowledge that just
	about every modern ISP either starts PPP immediately and uses PAP,
	or has a standard login/password prompt.)
	
    - support any number of different dialin accounts with one simple
	configuration file.

    - guess at the command prompt you get from some ISP's, and try to
	respond automatically.  It even reads command menus!

    - configure and run pppd automatically using PAP or CHAP without the
	need to manually change or even understand the pap-secrets file.

And from what people have told us, it actually works!


Great!  Where can I get it?
===========================

The WvDial 1.00 release is available both as source code and as a Debian
package.  You can get either or both from our web page:

		http://www.worldvisions.ca/wvdial/
		
The Debian package will soon also be arriving at ftp.debian.org in the
unstable "slink" distribution.


Credits
=======

WvDial and its supporting libraries were written by Dave Coombs and
Avery Pennarun of Worldvisions Computer Technology, Inc. as part of the
Worldvisions Weaver project.

Many thanks to all the people who endured our pre-release versions!

You can contact us at:
		wvdial@worldvisions.ca

