/*
 * Worldvisions Weaver Software:
 *   Copyright (C) 1998, 1999 Worldvisions Computer Technology, Inc.
 *
 * Version numbers for the different parts of Weaver included here.
 *
 */
#ifndef __WVVER_H
#define __WVVER_H

// *_VER is a numerical version number for use ONLY in comparisons and
// internal data structures.
// *_VER_STRING is a string for use ONLY in display strings.
//
// Never mix and match them!  WEAVER_VER_STRING may be any
//  valid string, eg "4.5 beta 6"
//

#define WEAVER_VER		0x00022100
#define WEAVER_VER_STRING	"2.21"

#define WVDIAL_VER		0x00014100
#define WVDIAL_VER_STRING	"1.41"

#define FASTFORWARD_VER		0x00010100
#define FASTFORWARD_VER_STRING	"1.01"

#define TUNNELV_VER		0x00020000
#define TUNNELV_VER_STRING	"2.00"

#endif __WVDEFS_H
