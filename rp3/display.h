/* Red Hat PPP Dialer applet/application
 * Copyright 1999 Red Hat, Inc.
 *
 * Author: Michael K. Johnson <johnsonm@redhat.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Cambridge, MA 
 * 02139, USA.
 *
 */

#ifndef _DISPLAY_H
#define _DISPLAY_H

#include <gtk/gtk.h>
#include <gtknetmon.h>

#include "interface.h"

/* display a warning message */
void
display_warning(char *warning);

/* When we create the netmon widget corresponding to this device,
 * ask whether we should bring the interface up.
 * Only sets one interface -- we should not create a storm of
 * questions when the program starts.
 */
void
display_set_interface_startup_query(interface *i);

/* create the gtk widget set corresponding to the named interface */
GtkWidget *
display_interface_create(interface *i);

int
display_wait(pid_t child, GtkWidget *d, interface *i, interfaceStatus status);
void
display_unwait(pid_t child);

void
display_set_status(GtkWidget *netmon);
void
display_set_status_by_interface(GtkWidget *ignore, interface *i);

/* change the netmon->interface binding */
void
display_interface_set(GtkWidget *netmon, interface *i);

void
display_set_orientation (GtkWidget *netmon, GtkNetmonOrientation orientation);

void
display_set_panel_size (GtkWidget *netmon, gint size);


#endif /* _DISPLAY_H */
