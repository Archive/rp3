/* Red Hat PPP Dialer applet/application
 * Copyright 1999 Red Hat, Inc.
 *
 * Author: Michael K. Johnson <johnsonm@redhat.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Cambridge, MA 
 * 02139, USA.
 *
 */

#include <config.h>
#include <errno.h>
#include <time.h>
#include <signal.h>
#define _GNU_SOURCE
#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include <gnome.h>

#include "gtknetmon.h"
#include "conf.h"
#include "interface.h"
#include "netreport.h"
#include "appapplet.h"
#include "properties.h"

#include "display.h"

typedef struct _appinfo {
    interface		*i;
    interfaceStatus	nextStatus;
    int			txrx_cb_tag;
    int			speed_cb_tag;
    int			time_cb_tag;
} appinfo;


static GSList *interfaceList;
static GHashTable *childHash;
static int sigio_notify_pipe[3];
static int sigchld_notify_pipe[3];
static interface *autoStart = NULL;



/* display a warning message */
void
display_warning(char *warning) {
    GtkWidget *d;

    d = appapplet_gnome_warning_dialog(warning);
    gnome_dialog_run_and_close(GNOME_DIALOG(d));
}



static gint
display_txrx_update(gpointer data) {
    GtkWidget *netmon = data;
    appinfo *app;

    app = gtk_object_get_user_data(GTK_OBJECT(netmon));
    g_assert(app);

    interface_txrx_update(app->i);
    gtk_netmon_set_transmitting(GTK_NETMON(netmon), app->i->tx);
    gtk_netmon_set_receiving(GTK_NETMON(netmon), app->i->rx);

    return TRUE;
}

static gint
display_speed_update(gpointer data) {
    GtkWidget *netmon = data;
    appinfo *app;
    int speed, speedin = 0, speedout = 0, n;
    float display = 0.0;
    char spdisplay[50]; /* way bigger than we need */

    app = gtk_object_get_user_data(GTK_OBJECT(netmon));
    g_assert(app);

    /* Use a sliding window of the last 6 seconds to calculate the
     * current transceive rate.  I'm told that network sniffers
     * tend to use a range of 6-9, so 6 should be reasonable.
     * -- Hmm, 6 is too much, particularly for the PPP connections
     * that rp3 is primarily used for.
     *
     * Use input or output, whichever gives a higher number, since
     * the user most likely cares about the higher number, and we
     * barely have space for one number, let alone two.
     */
#   define WINDOW 2
    for (n = INTERFACE_TPUT_READINGS - 1; n >= INTERFACE_TPUT_READINGS - WINDOW; n--) {
	speedin  += app->i->bytesIn[n];
	speedout += app->i->bytesOut[n];
    }
    speed = MAX(speedin, speedout);
    speed /= WINDOW;
    if (speed < 0) speed = 0;

    if (speed > 1024*1024) {
	display = speed / (1024.0*1024.0);
	sprintf(spdisplay, "%.1f M", display);
    } else if (speed > 1024) {
	display = speed / 1024.0;
	sprintf(spdisplay, "%.1f K", display);
    } else {
	sprintf(spdisplay, "%d B", speed);
    }
    gtk_netmon_set_speed(GTK_NETMON(netmon), spdisplay);

    return TRUE;
}

/* update time AND graph display */
static gint
display_time_update(gpointer data) {
    GtkWidget *netmon = data;
    appinfo *app;

    app = gtk_object_get_user_data(GTK_OBJECT(netmon));
    g_assert(app);

    interface_bytecount_update(app->i);
    gtk_netmon_set_throughput(GTK_NETMON(netmon), app->i->dqBytesIn,
	app->i->dqBytesOut, INTERFACE_TPUT_READINGS);

    /* update cost or time? */
    if (conf_get_bool(app->i->interfaceName, "watchCost", FALSE)) {
	char cost[50];
	char *currencySymbol;

	currencySymbol = conf_get_string("default", "currencySymbol", _("$"));
	snprintf(cost, 49, _("%s%.2f"),
	    currencySymbol,
	    interface_current_cost(app->i));
	g_free(currencySymbol);
	gtk_netmon_set_cost(GTK_NETMON(netmon), cost);
	gtk_netmon_set_display_style(GTK_NETMON(netmon), GTK_NETMON_COST);
    } else {
	gtk_netmon_set_seconds_online(GTK_NETMON(netmon),
	    difftime(time(NULL), app->i->started));
	gtk_netmon_set_display_style(GTK_NETMON(netmon), GTK_NETMON_TIME);
    }

    return TRUE;
}

static void
display_status_warning(interfaceStatus intendedStatus) {
    if (intendedStatus == INTERFACE_UP)
	display_warning(_("Failed to activate interface"));
    else
        if (intendedStatus == INTERFACE_DOWN)
	    display_warning(_("Failed to deactivate interface"));
        else
	    display_warning(_("Don't know what to do with interface"));
}



typedef struct _waitInfo {
    GtkWidget *d;
    interface *i;
    interfaceStatus status;
} waitInfo;

static void
display_maybe_warn(interface *i, interfaceStatus wanted) {
    interface_status_update(i);
    if (i->status != wanted)
	display_status_warning(wanted);
}

int
display_wait(pid_t child, GtkWidget *d, interface *i, interfaceStatus status) {
    waitInfo *w;

    /* wait asynchronously first in case child has already returned */
    if (waitpid(child, NULL, WNOHANG) == child) {
	if (i)
	    display_maybe_warn(i, status);
	return 0;
    }

    w = g_malloc0(sizeof(waitInfo));
    w->d = d;
    w->i = i;
    w->status = status;
    g_hash_table_insert(childHash, GINT_TO_POINTER(child), w);
    return 1;
}

void
display_unwait(pid_t child) {
    waitInfo *w;

    w = g_hash_table_lookup(childHash, GINT_TO_POINTER(child));
    if (w != NULL) {
        g_free(w);
        g_hash_table_remove(childHash, GINT_TO_POINTER(child));
    }
}

static void
display_sigchld_real_handler(gpointer data, gint source, GdkInputCondition condition) {
    pid_t child;
    waitInfo *w;
    char b;

    /* read and throw away the "1" we wrote in the signal handler */
    read(sigchld_notify_pipe[0], &b, 1);

    child = waitpid(-1, NULL, WNOHANG);
    if ((child == 0) || (child == -1)) return;

    /* if not a child we are waiting for, return */
    w = g_hash_table_lookup(childHash, GINT_TO_POINTER(child));
    if (!w) return;

    if (w->i) {
	gtk_widget_destroy(w->d);
	display_maybe_warn(w->i, w->status);
    } else {
	display_unwait(child);
    }
}

static void
display_sigchld_handler(int ignored) {
    write(sigchld_notify_pipe[1], "1", 1);
}


static gint
display_progress_bar_update(gpointer p) {
    static float percentage = 0.0;

    if(GTK_IS_PROGRESS_BAR(p)) {
        gtk_progress_bar_update(GTK_PROGRESS_BAR(p), percentage);
    }
    percentage += 0.01;
    if (percentage >= 1.0) percentage = 0.0;

    return TRUE;
}

/* set a pointer to the value of the row data for the selected row */
static void
listbox_set_data(GtkCList *clist, gint row, gint column, GdkEvent *event,
		 gpointer *data)
{
	*data = gtk_clist_get_row_data(clist, row);
}

static void
display_set_status_by_app(appinfo *app, int force_ask) {
    char *message = NULL;
    size_t len;
    GtkWidget *d, *l, *p;
    GtkWidget *chooseScroll = NULL, *chooseBox = NULL;
    int button;
    interfaceStatus status;
    pid_t child, try_child;
    interface *i = NULL;

    status = app->nextStatus;
    len = strlen(app->i->friendlyName);

    /* do we need to ask? */
    switch (status) {
    case INTERFACE_UP:
	if (app->i->cloneList) {
	    int row;
	    GSList *cl;

	    /* 200 should be enough for any translation, I hope... */
	    message = alloca(len + 200);
	    snprintf(message, len+199, _("Choose a connection for %s:"),
		     app->i->friendlyName);

	    chooseScroll = gtk_scrolled_window_new(NULL, NULL);
	    gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(chooseScroll),
		GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
	    chooseBox = gtk_clist_new(1);
	    gtk_clist_freeze(GTK_CLIST(chooseBox));
	    gtk_clist_set_selection_mode(GTK_CLIST(chooseBox), GTK_SELECTION_BROWSE);
	    gtk_clist_set_shadow_type(GTK_CLIST(chooseBox), GTK_SHADOW_IN);
	    /* hack!!! */
	    gtk_clist_set_column_width(GTK_CLIST(chooseBox), 0, 200);

	    /* present non-clone version */
	    row = gtk_clist_append(GTK_CLIST(chooseBox), &app->i->friendlyName);
	    gtk_clist_set_row_data(GTK_CLIST(chooseBox), row, app->i);
	    /* present clones */
	    for (cl = app->i->cloneList; cl; cl = cl->next) {
		interface *c = cl->data;
		row = gtk_clist_append(GTK_CLIST(chooseBox), &c->friendlyName);
		gtk_clist_set_row_data(GTK_CLIST(chooseBox), row, c);
	    }
	    gtk_clist_select_row(GTK_CLIST(chooseBox), 0, 0);

	    gtk_scrolled_window_add_with_viewport(GTK_SCROLLED_WINDOW(chooseScroll), chooseBox);
	    gtk_clist_thaw(GTK_CLIST(chooseBox));

	} else if (force_ask || conf_get_bool(app->i->logicalName, "confirmStart", TRUE)) {
	    /* 200 should be enough for any translation, I hope... */
	    message = alloca(len + 200);
	    snprintf(message, len+199, _("Start interface %s?"),
		     app->i->friendlyName);
	}
	break;

    case INTERFACE_DOWN:
	if (force_ask || conf_get_bool(app->i->logicalName, "confirmStop", TRUE)) {
	    /* 200 should be enough for any translation, I hope... */
	    message = alloca(len + 200);
	    snprintf(message, len+199, _("Stop interface %s?"),
		     app->i->friendlyName);
	}
	break;

    default:
	g_assert_not_reached();
	break;
    }

    if (message) {
	d = gnome_dialog_new (
	    _("Change connection status?"),
	    GNOME_STOCK_BUTTON_YES,
	    GNOME_STOCK_BUTTON_NO,
	    NULL);

	l = gtk_label_new (message); 
	gtk_box_pack_start (GTK_BOX (GNOME_DIALOG (d)->vbox), l, TRUE, TRUE, 0);
	if (chooseBox) {
	    gtk_signal_connect (GTK_OBJECT (chooseBox), "select-row",
			        GTK_SIGNAL_FUNC (listbox_set_data), &i);
	    gtk_clist_select_row (GTK_CLIST (chooseBox), 1, 0);
	    gtk_box_pack_start (GTK_BOX (GNOME_DIALOG (d)->vbox), chooseScroll, TRUE, TRUE, 0);
	}
	gtk_widget_show_all(d);
	appapplet_gnome_dialog_set_parent(GNOME_DIALOG(d));

	button = gnome_dialog_run_and_close (GNOME_DIALOG (d));

	switch (button) {
	    case 0:
		/* i has been set by a callback, either by the user or
		   the first time we selected a row after registering the
		   callback.  Either that or it's NULL. */
		if (i == NULL) {
		    i = app->i;
		}

		break;
	    case -1: /* User closed window with the mouse */
	    case 1:
		return;
		break;
	}

    } else {
	i = app->i;
    }


    if (status == INTERFACE_DOWN) {
	child = interface_status_change(i, status);
	do {
	    try_child = waitpid(child, NULL, 0);
	} while ((try_child < 0 && errno == EINTR) ||
		 (try_child && try_child != child));
	interface_status_update(app->i);
	if (app->i->status != status)
	    display_status_warning(status);

    } else {
	child = interface_status_change(i, status);

	d = gnome_dialog_new (
	    _("Waiting..."),
	    GNOME_STOCK_BUTTON_CANCEL,
	    NULL);

	if (display_wait(child, d, app->i, status)) {
	    int progress;

	    /* 200 should be enough for any translation, I hope... */
	    message = alloca(len + 200);
	    snprintf(message, len+199, _("Waiting to connect to %s"),
		     app->i->friendlyName);
	    l = gtk_label_new (message); 
	    gtk_box_pack_start (GTK_BOX (GNOME_DIALOG (d)->vbox), l, TRUE, TRUE, 0);
	    gtk_widget_show(l);
	    p = gtk_progress_bar_new();
	    gtk_progress_set_activity_mode(GTK_PROGRESS(p), TRUE);
	    progress = gtk_timeout_add(100, display_progress_bar_update, p);
	    gtk_box_pack_start (GTK_BOX (GNOME_DIALOG (d)->vbox), p, TRUE, TRUE, 0);
	    gtk_widget_show(l);
	    gtk_widget_show_all(d);
	    appapplet_gnome_dialog_set_parent(GNOME_DIALOG(d));
	    button = gnome_dialog_run_and_close (GNOME_DIALOG (d));

	    gtk_timeout_remove(progress);

	    display_unwait(child);
	    switch (button) {
	    case 0:
		/* cancel */
		child = interface_status_change(i, INTERFACE_DOWN);
		do {
		    try_child = waitpid(child, NULL, 0);
		} while ((try_child < 0 && errno == EINTR) ||
			 (try_child && try_child != child));
		break;
	    default:
		/* window closed by wm close, ESC, or ifup returned */
		break;
	    }
	} else {
	    gtk_widget_destroy(d);
	}
    }
}

void
display_set_status(GtkWidget *netmon) {
    appinfo *app;

    app = gtk_object_get_user_data(GTK_OBJECT(netmon));
    g_assert(app);

    display_set_status_by_app(app, 0);
}

void
display_set_status_by_interface(GtkWidget *ignore, interface *i) {
    display_set_status(appapplet_get_netmon(i));
}




static void
display_netmon_connected(GtkWidget *netmon) {
    appinfo *app;

    app = gtk_object_get_user_data(GTK_OBJECT(netmon));
    g_assert(app);

    appapplet_set_menu_status(app->i);
    if (app->i->manipulable) {
	appapplet_set_netmon_tooltip(netmon, app->i);
	if (app->nextStatus == INTERFACE_UNKNOWN)
	    gtk_signal_connect(GTK_OBJECT(netmon), "clicked",
		GTK_SIGNAL_FUNC(display_set_status), NULL);
	app->nextStatus = INTERFACE_DOWN;
    }

    if (app->txrx_cb_tag) gtk_timeout_remove(app->txrx_cb_tag);
    if (app->speed_cb_tag) gtk_timeout_remove(app->speed_cb_tag);
    if (app->time_cb_tag) gtk_timeout_remove(app->time_cb_tag);

    app->txrx_cb_tag = gtk_timeout_add(100, display_txrx_update, netmon);
    app->speed_cb_tag = gtk_timeout_add(2000, display_speed_update, netmon);
    app->time_cb_tag = gtk_timeout_add(1000, display_time_update, netmon);

    gtk_netmon_set_online(GTK_NETMON(netmon), TRUE);
}

static void
display_netmon_disconnected(GtkWidget *netmon) {
    appinfo *app;

    app = gtk_object_get_user_data(GTK_OBJECT(netmon));
    g_assert(app);

    gtk_netmon_set_speed(GTK_NETMON(netmon), "");
    gtk_netmon_set_seconds_online(GTK_NETMON(netmon), 0);

    appapplet_set_menu_status(app->i);
    if (app->i->manipulable) {
	appapplet_set_netmon_tooltip(netmon, app->i);
	if (app->nextStatus == INTERFACE_UNKNOWN)
	    gtk_signal_connect(GTK_OBJECT(netmon), "clicked",
		GTK_SIGNAL_FUNC(display_set_status), NULL);
	app->nextStatus = INTERFACE_UP;
    }

    if (app->txrx_cb_tag) gtk_timeout_remove(app->txrx_cb_tag);
    if (app->speed_cb_tag) gtk_timeout_remove(app->speed_cb_tag);
    if (app->time_cb_tag) gtk_timeout_remove(app->time_cb_tag);
    app->txrx_cb_tag = app->speed_cb_tag = app->time_cb_tag = 0;

    gtk_netmon_set_online(GTK_NETMON(netmon), FALSE);
}


static void
display_sigio_real_handler(gpointer data, gint source, GdkInputCondition condition) {
    GSList *il;
    char *b;

    /* read and throw away the "1" we wrote in the signal handler */
    read(sigio_notify_pipe[0], &b, 1);

    for (il = interfaceList; il; il = il->next) {
	GtkWidget *netmon = il->data;
	appinfo *app;

	app = gtk_object_get_user_data(GTK_OBJECT(netmon));
	g_assert(app);

	/* We only want to change signal connection state and
	 * possibly update saved costs if:
	 *   We just started and thus need to set up for the first time
	 *   We have not done this since we last changed status for this iface
	 * Conditionalizing on app->nextStatus works for this because it is
	 * only written in display_netmon(dis)?connected.  If this is
	 * ever changed, the invariants need to be re-analyzed.
	 */
	interface_status_update(app->i);
	if (app->nextStatus == INTERFACE_UNKNOWN ||
	    app->nextStatus == app->i->status) {
	    if (app->i->status == INTERFACE_UP) {
		display_netmon_connected(netmon);
	    } else {
		properties_update_saved_cost(app->i);
		display_netmon_disconnected(netmon);
	    }
	}
    }
}

static void
display_sigio_handler(int ignored) {
    write(sigio_notify_pipe[1], "1", 1);
}

/* clean up the app pointer on widget destroy */
static void
display_netmon_destroy(GtkWidget *netmon) {
    appinfo *app;

    app = gtk_object_get_user_data(GTK_OBJECT(netmon));
    g_assert(app);

    if (app->txrx_cb_tag) gtk_timeout_remove(app->txrx_cb_tag);
    if (app->speed_cb_tag) gtk_timeout_remove(app->speed_cb_tag);
    if (app->time_cb_tag) gtk_timeout_remove(app->time_cb_tag);

    interfaceList = g_slist_remove(interfaceList, netmon);
    if (!interfaceList) {
	netreport_unsubscribe();
    }
}


/* When we create the netmon widget corresponding to this device,
 * ask whether we should bring the interface up.
 * Only sets one interface -- we should not create a storm of
 * questions when the program starts.
 */
void
display_set_interface_startup_query(interface *i) {
    autoStart = i;
}

/* create the gtk widget set corresponding to the named interface */
GtkWidget *
display_interface_create(interface *i){
    GtkWidget *netmon;
    appinfo *app;

    app = g_malloc0(sizeof(appinfo));
    app->i = i;

    netmon = gtk_netmon_new(FALSE);
    gtk_netmon_set_has_disconnect (GTK_NETMON (netmon), i->manipulable);
    gtk_object_set_user_data(GTK_OBJECT(netmon), app);

    /* it is particularly important to show the current cost if
     * we are accumulating cost, but it's good to show currency
     * instead of time even when the value is 0.0...
     */
    if (conf_get_bool(i->interfaceName, "watchCost", FALSE)) {
	char cost[50];
	char *currencySymbol;

	currencySymbol = conf_get_string("default", "currencySymbol", _("$"));
	snprintf(cost, 49, _("%s%.2f"),
	    currencySymbol,
	    conf_get_float(i->interfaceName, "currentCost", 0.0));
	g_free(currencySymbol);
	gtk_netmon_set_cost(GTK_NETMON(netmon), cost);
	gtk_netmon_set_display_style(GTK_NETMON(netmon), GTK_NETMON_COST);
    }

    gtk_signal_connect(GTK_OBJECT(netmon), "destroy",
	GTK_SIGNAL_FUNC(display_netmon_destroy), NULL);

    if (i->status == INTERFACE_UP) {
	display_netmon_connected(netmon);
    } else {
	display_netmon_disconnected(netmon);
    }

    if (!interfaceList) {
	struct sigaction act;

	if (pipe(sigio_notify_pipe) < 0) {
	    display_warning(_("Internal error: could not open SIGIO pipe"));
	    return NULL; /* abort */
	}
	sigio_notify_pipe[2] = gdk_input_add(sigio_notify_pipe[0],
					     GDK_INPUT_READ,
					     display_sigio_real_handler,
					     NULL);
	netreport_subscribe();
	act.sa_handler = display_sigio_handler;
	act.sa_flags = SA_RESTART;
	sigaction(SIGIO, &act, NULL);

	childHash = g_hash_table_new(g_direct_hash, NULL);
	if (pipe(sigchld_notify_pipe) < 0) {
	    display_warning(_("Internal error: could not open SIGCHLD pipe"));
	    return NULL; /* abort */
	}
	sigchld_notify_pipe[2] = gdk_input_add(sigchld_notify_pipe[0],
					       GDK_INPUT_READ,
					       display_sigchld_real_handler,
					       NULL);
	act.sa_handler = display_sigchld_handler;
	act.sa_flags = SA_RESTART;
	sigaction(SIGCHLD, &act, NULL);
    }

    interfaceList = g_slist_prepend(interfaceList, netmon);

    if (autoStart) {
	display_set_status_by_app(app, 1);
    }

    return netmon;
}


/* change the netmon->interface binding */
void
display_interface_set(GtkWidget *netmon, interface *i) {
    appinfo *app;

    app = gtk_object_get_user_data(GTK_OBJECT(netmon));
    app->i = i;
    interface_status_update(i);
    appapplet_set_netmon(i, netmon);
    appapplet_set_menu_status(i);
    if (app->i->status == INTERFACE_UP) {
	display_netmon_connected(netmon);
    } else {
	display_netmon_disconnected(netmon);
    }
}


void
display_set_orientation (GtkWidget *netmon, GtkNetmonOrientation orientation)
{
  gtk_netmon_set_orientation (GTK_NETMON (netmon), orientation);
}

void
display_set_panel_size (GtkWidget *netmon, gint size)
{
  gtk_netmon_set_size (GTK_NETMON (netmon), size);
}

