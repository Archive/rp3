/* Red Hat PPP Dialer applet/application
 * Copyright 1999 Red Hat, Inc.
 *
 * Author: Michael K. Johnson <johnsonm@redhat.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Cambridge, MA 
 * 02139, USA.
 *
 */

/* wrap the differences between being an applet and being an application,
 * such as how to deal with tooltips
 */

#include <config.h>
#include <assert.h>
#include <stdarg.h>
#include <stdio.h>

#include <gtk/gtk.h>
#include <applet-widget.h>

#include "appapplet.h"
#include "display.h"
#include "properties.h"

#include "conf.h"

static AppletWidget *the_applet = NULL;
static GtkWidget *the_toplevel = NULL;
static GtkTooltips *the_tooltips = NULL;
static gboolean is_usernet = FALSE;
static GtkAccelGroup *the_accel = NULL;
static GtkWidget *last_netmon = NULL;
static GtkLabel *menu_connect = NULL;
static GtkLabel *menu_disconnect = NULL;

static GHashTable *netmonHash = NULL;
static GHashTable *conHash = NULL;
static GHashTable *disHash = NULL;

static char *the_interface_name = NULL;
static char *the_title = NULL;



void
appapplet_set_is_applet(AppletWidget *applet) {
    the_applet = applet;
    netmonHash = g_hash_table_new(g_direct_hash, NULL);
}

void
appapplet_set_is_app(GtkWidget *toplevel, char *title, gboolean usernet) {
    is_usernet = usernet;
    the_toplevel = toplevel;
    the_tooltips = gtk_tooltips_new();
    the_title = title;
    if (the_title)
	gtk_window_set_title(GTK_WINDOW(the_toplevel), the_title);
    netmonHash = g_hash_table_new(g_direct_hash, NULL);
    conHash = g_hash_table_new(g_direct_hash, NULL);
    disHash = g_hash_table_new(g_direct_hash, NULL);
}

void
appapplet_set_tooltip(char *format, ...) {
    va_list ap;
    /* do this simply: tooltips should be small anyway... */
    char tip[250];

    assert(the_applet || the_toplevel);

    va_start(ap, format);
    vsnprintf(tip, 249, format, ap);
    va_end(ap);

    if (the_applet) {
	applet_widget_set_tooltip(the_applet, tip);
    } else {
	gtk_tooltips_set_tip(the_tooltips, the_toplevel, tip, NULL);
    }
}

void
appapplet_set_widget_tooltip(GtkWidget *widget, char *format, ...) {
    va_list ap;
    char *tip = NULL;

    assert(the_applet || the_toplevel);

    if (format) {
	va_start(ap, format);
	/* do this simply: tooltips should be small anyway... */
	tip = alloca(250);
	vsnprintf(tip, 249, format, ap);
	va_end(ap);
    }

    if (the_applet) {
	applet_widget_set_widget_tooltip(the_applet, widget, tip);
    } else {
	gtk_tooltips_set_tip(the_tooltips, widget, tip, NULL);
    }
}



/* interface status management */

static void
appapplet_set_interface_status(GtkWidget *ignore, interface *i) {
    GtkWidget *netmon;

    netmon = appapplet_get_netmon(i);
    assert(netmon);
    display_set_status(netmon);
}

/* creates the appropriate callback in an applet
 * records the two menu items in an application, set sensitivity
 */
void
appapplet_set_menu_items(interface *i, GtkWidget *netmon,
			 GtkWidget *con, GtkWidget *dis) {
    appapplet_set_netmon(i, netmon);
    if (the_applet) {
	appapplet_set_menu_status(i);
    } else {
	g_hash_table_insert(conHash, netmon, con);
	g_hash_table_insert(disHash, netmon, dis);
	appapplet_set_menu_status(i);
    }
}

/* change the callback in an applet
 * set the correct menu item sensitive in an application
 */
void
appapplet_set_menu_status(interface *i) {
    if (the_applet) {
	char *message;
	char *stockButton;
	void *callback;

	if (!i->manipulable)
	    return;
	if (i->status == INTERFACE_DOWN) {
	    message = g_strdup_printf(_("Connect to %s"), i->friendlyName);
	    stockButton = GNOME_STOCK_MENU_UP;
	} else {
	    message = g_strdup_printf(_("Disconnect from %s"), i->friendlyName);
	    stockButton = GNOME_STOCK_MENU_DOWN;
	}
	callback = appapplet_set_interface_status;
	applet_widget_register_stock_callback(APPLET_WIDGET(the_applet),
	    "connection", stockButton, message, callback, i);
	g_free(message);

    } else if (the_toplevel) {
	GtkWidget *netmon;
	GtkWidget *con;
	GtkWidget *dis;

	netmon = appapplet_get_netmon(i);
	if (!netmon) return; /* if called before netmon created, will be called again */
	con = g_hash_table_lookup(conHash, netmon);
	dis = g_hash_table_lookup(disHash, netmon);

	if (i->manipulable) {
	    if (i->status == INTERFACE_DOWN) {
		gtk_widget_set_sensitive(con, TRUE);
		gtk_widget_set_sensitive(dis, FALSE);
	    } else {
		gtk_widget_set_sensitive(con, FALSE);
		gtk_widget_set_sensitive(dis, TRUE);
	    }
	} else {
	    gtk_widget_set_sensitive(con, FALSE);
	    gtk_widget_set_sensitive(dis, FALSE);
	}
    } /* else ignore */
}


/* Set the popup menu for an interface.
 * Call to re-set menus whenever necessary in single-display mode.
 */
void
appapplet_set_popup_menu(interface *i, GtkWidget *netmon,
			 GtkWidget *attach1, GtkWidget *attach2) {

    if (netmon) {
	if (!appapplet_get_netmon(i))
	    appapplet_set_netmon(i, netmon);
    } else {
	if (is_usernet) {
	    netmon = appapplet_get_netmon(i);
	} else {
	    netmon = last_netmon;
	}
    }

    if (the_toplevel) {
	GtkWidget *popupMenu;
	GnomeUIInfo popupMenuInfo[] = {
	    GNOMEUIINFO_MENU_ABOUT_ITEM(properties_about_show, NULL),
	    GNOMEUIINFO_MENU_PROPERTIES_ITEM(properties_dialog_show, i),
	    { GNOME_APP_UI_ITEM, _("Configure PPP..."), NULL,
	      properties_rp3config_show_nowait, NULL, NULL, GNOME_APP_PIXMAP_NONE,
	      0, 0, (GdkModifierType) 0, NULL },
	    GNOMEUIINFO_SEPARATOR,
	    { GNOME_APP_UI_ITEM, NULL, NULL,
	      display_set_status_by_interface, NULL, NULL,
	      GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_PIXMAP_UP,
	      0, (GdkModifierType) 0, NULL },
	    { GNOME_APP_UI_ITEM, NULL, NULL,
	      display_set_status_by_interface, NULL, NULL,
	      GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_PIXMAP_DOWN,
	      0, (GdkModifierType) 0, NULL },
	    GNOMEUIINFO_SEPARATOR,
	    GNOMEUIINFO_MENU_CLOSE_ITEM(gtk_main_quit, NULL),
	    GNOMEUIINFO_END
	};

	if (!is_usernet && !the_title)
	    gtk_window_set_title(GTK_WINDOW(the_toplevel), i->friendlyName);

	/* find any clones */
	interface_populate_clone_list(i);

	if (!attach1) {
	    /* we don't need to do the menu mangling for usernet */
	    if (is_usernet) return;
	}

	popupMenuInfo[4].label =
	    g_strdup_printf(_("Connect to %s%s"), i->friendlyName,
			    i->cloneList ? _(" or clone") : "");
	popupMenuInfo[5].label =
	    g_strdup_printf(_("Disconnect from %s%s"), i->friendlyName,
			    i->cloneList ? _(" or clone") : "");
	/* NOTE: callback data in GnomeUIInfo is IGNORED when
	 * gnome_popup_menu_attach is used; the callback data
	 * given to gnome_popup_menu_attach is used instead.  :-(
	 */

	if (menu_connect) {
	    gtk_label_set_text(menu_connect, popupMenuInfo[4].label);
	    gtk_label_set_text(menu_disconnect, popupMenuInfo[5].label);
	} else {
	    if (!the_accel) {
		the_accel = gtk_accel_group_new();
		gtk_accel_group_attach(the_accel, GTK_OBJECT(the_toplevel));
		popupMenu = gnome_popup_menu_new_with_accelgroup(popupMenuInfo, the_accel);
	    } else {
		popupMenu = gnome_popup_menu_new(popupMenuInfo);
	    }
	    gnome_popup_menu_attach(popupMenu, attach1, i);
	    if (attach2) gnome_popup_menu_attach(popupMenu, attach2, i);
	    appapplet_set_menu_items(i, netmon,
		popupMenuInfo[4].widget, popupMenuInfo[5].widget);

	    if (!is_usernet) {
		menu_connect = GTK_LABEL(GTK_BIN(popupMenuInfo[4].widget)->child);
		menu_disconnect = GTK_LABEL(GTK_BIN(popupMenuInfo[5].widget)->child);
	    }
	}
	g_free(popupMenuInfo[4].label);
	g_free(popupMenuInfo[5].label);

    } else {

	applet_widget_register_stock_callback(APPLET_WIDGET(the_applet),
	    "properties", GNOME_STOCK_MENU_PROP, _("Properties..."),
	    properties_dialog_show, i);
	applet_widget_register_stock_callback(APPLET_WIDGET(the_applet),
	    "configure", GNOME_STOCK_MENU_PROP, _("Configure PPP..."),
	    properties_rp3config_show_nowait, NULL);
	appapplet_set_menu_items(i, netmon, NULL, NULL);
	applet_widget_register_stock_callback(APPLET_WIDGET(the_applet),
	    "about", GNOME_STOCK_MENU_ABOUT, _("About..."),
	    properties_about_show, NULL);
    }
    appapplet_set_netmon_tooltip(netmon, i);
}


/* set the proper current tooltip state for a netmon widget */
void
appapplet_set_netmon_tooltip(GtkWidget *netmon, interface *i) {
    if (!netmon) return;

    if (i->manipulable) {
	if (i->status == INTERFACE_UP)
	    appapplet_set_widget_tooltip(netmon,
		_("Click to disconnect from %s"), i->friendlyName);
	else
	    appapplet_set_widget_tooltip(netmon,
		_("Click to connect to %s"), i->friendlyName);
    } else {
	appapplet_set_widget_tooltip(netmon, NULL);
    }
}



void
appapplet_set_netmon(interface *i, GtkWidget *netmon) {
    if (!g_hash_table_lookup(netmonHash, i)) {
	g_hash_table_insert(netmonHash, i, netmon);
    }
    last_netmon = netmon;
}

GtkWidget *
appapplet_get_netmon(interface *i) {
    return g_hash_table_lookup(netmonHash, i);
}


gboolean
appapplet_is_usernet(void) {
    return is_usernet;
}



void
appapplet_gnome_dialog_set_parent(GnomeDialog* dialog) {
    if (the_toplevel)
	gnome_dialog_set_parent(dialog, GTK_WINDOW(the_toplevel));
}

GtkWidget *
appapplet_gnome_warning_dialog(char* message) {
    if (the_toplevel)
	return gnome_warning_dialog_parented(message, GTK_WINDOW(the_toplevel));
    else
	return gnome_warning_dialog(message);
}






/* session management */


static int
session_save (GnomeClient *client, gint phase, GnomeSaveStyle save_style,
	      gint is_shutdown, GnomeInteractStyle interact_sytle,
	      gint is_fast, gpointer client_data) {
    gchar ** argv;
    guint argc;

    argv = g_malloc0(sizeof(gchar *) * 4);
    argc = 1;
    argv[0] = client_data;

    if (!is_usernet && the_interface_name) {
	argv[1] = "--interface";
	argv[2] = the_interface_name;
	argc = 3;
    }

    gnome_client_set_clone_command(client, argc, argv);
    gnome_client_set_restart_command(client, argc, argv);

    return TRUE;
}

static int
session_die (GnomeClient *client, gpointer client_data) {
    if (!is_usernet &&
        conf_get_bool(the_interface_name, "terminateOnClose", FALSE)) {
	interface *i;

	i = interface_new(the_interface_name); /* just gets a copy */
	if (i->manipulable) {
	    interface_status_update(i);
	    if (i->status == INTERFACE_UP)
		interface_status_change(i, INTERFACE_DOWN);
	}
    }
    gtk_main_quit();
    return TRUE;
}

static int
session_save_applet (AppletWidget *applet, char *privcfgpath, gpointer data) {
    
    gnome_config_push_prefix(privcfgpath);
    gnome_config_set_string("rp3/interfaceName", the_interface_name);
    gnome_config_sync();
    gnome_config_drop_all();
    gnome_config_pop_prefix();
    if (conf_get_bool(the_interface_name, "terminateOnClose", FALSE)) {
	interface *i;

	i = interface_new(the_interface_name); /* just gets a pointer to the existing structure */
	if (i->manipulable) {
	    interface_status_update(i);
	    if (i->status == INTERFACE_UP)
		interface_status_change(i, INTERFACE_DOWN);
	}
    }
    return FALSE;
}

void
appapplet_start_session_management(char **argv, char *interfaceName) {
    GnomeClient *client;

    appapplet_set_interface_name(interfaceName);

    if (!the_applet) {
	client = gnome_master_client();
	gtk_signal_connect (GTK_OBJECT(client), "save_yourself",
	    GTK_SIGNAL_FUNC(session_save), argv[0]);
	gtk_signal_connect (GTK_OBJECT(client), "die",
	    GTK_SIGNAL_FUNC(session_die), NULL);
    } else {
	gtk_signal_connect (GTK_OBJECT(the_applet), "save_session",
	    GTK_SIGNAL_FUNC(session_save_applet), NULL);
    }
}

void
appapplet_set_interface_name(char *interfaceName) {
    the_interface_name = interfaceName;
}

