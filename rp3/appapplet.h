/* Red Hat PPP Dialer applet/application
 * Copyright 1999 Red Hat, Inc.
 *
 * Author: Michael K. Johnson <johnsonm@redhat.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Cambridge, MA 
 * 02139, USA.
 *
 */

/* wrap the differences between being an applet and being an application,
 * such as how to deal with tooltips
 */

#ifndef _APPAPPLET_H
#define _APPAPPLET_H

#include <gtk/gtk.h>
#include <applet-widget.h>

#include "interface.h"

void
appapplet_set_is_applet(AppletWidget *applet);

void
appapplet_set_is_app(GtkWidget *toplevel, char *title, gboolean usernet);

void
appapplet_set_tooltip(char *format, ...);

void
appapplet_set_widget_tooltip(GtkWidget *widget, char *format, ...);

/* creates the appropriate callback in an applet
 * records the two menu items in an application, set sensitivity
 */
void
appapplet_set_menu_items(interface *i, GtkWidget *netmon,
			 GtkWidget *con, GtkWidget *dis);

/* change the callback in an applet
 * set the correct menu item sensitive in an application
 */
void
appapplet_set_menu_status(interface *i);

/* Set the popup menu for an interface.
 * Call to re-set menus whenever necessary in single-display mode.
 */
void
appapplet_set_popup_menu(interface *i, GtkWidget *netmon,
			 GtkWidget *attach1, GtkWidget *attach2);

/* set the proper current tooltip state for a netmon widget */
void
appapplet_set_netmon_tooltip(GtkWidget *netmon, interface *i);

void
appapplet_set_netmon(interface *i, GtkWidget *netmon);
GtkWidget *
appapplet_get_netmon(interface *i);

gboolean
appapplet_is_usernet(void);


GtkWidget *
appapplet_gnome_warning_dialog(char* message);

void
appapplet_gnome_dialog_set_parent(GnomeDialog* dialog);



/* session management stuff */
void
appapplet_start_session_management(char **argv, char *interfaceName);

void
appapplet_set_interface_name(char *interfaceName);

#endif /* _APPAPPLET_H */
