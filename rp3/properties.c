/* Red Hat PPP Dialer applet/application
 * Copyright 1999 Red Hat, Inc.
 *
 * Author: Michael K. Johnson <johnsonm@redhat.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Cambridge, MA 
 * 02139, USA.
 *
 */

#include <config.h>

#include <stdlib.h>
#include <signal.h>

#include <gnome.h>
#include <applet-widget.h>

#include "display.h"
#include "properties.h"
#include "appapplet.h"
#include "gtknetmon.h"

#include "interface.h"
#include "forkexec.h"
#include "conf.h"

/* a pipe to monitor for SIGCHLD.  The third element is the tag provided by
 * gdk_input_add */
static int sigchld_notify_pipe[3];

/* returns interface to use, given command-line hint
 * checks to make sure that chosenInterface exists and is user-manageable
 * if not, calls properties_interface_choose.
 * If this function returns NULL, no interface is available and no
 * interface was made available, and the program should either
 * exit or grey itself out or something like that.
 */
interface *
properties_interface_get(char **chosenInterface) {
    interface *i = NULL;

    if (chosenInterface && !*chosenInterface) {
	i = properties_interface_choose();
	if (!i) return NULL;
	*chosenInterface = i->logicalName;
	if (i->manipulable && i->status == INTERFACE_DOWN)
	    display_set_interface_startup_query(i);
    }

    return interface_new(chosenInterface ? *chosenInterface : NULL);
}

static void
list_box_return_row_data(GtkCList *clist, gint row, gint column, GdkEvent *ev,
			 gpointer *data)
{
    *data = gtk_clist_get_row_data(clist, row);
}

/* offers list of existing interfaces (if any) and the option
 * to set up a new interface.  Modal; does not return until
 * it has an interface or has officially given up (NULL).
 */
interface *
properties_interface_choose(void) {
    GtkWidget *d, *l, *chooseScroll, *chooseBox;
    int button;
    GList *interfaceList, *il;
    interface *ret = NULL;

    interfaceList = interface_list_generate(); /* always gets at least lo */
    if (interfaceList && interfaceList->next) {
	chooseScroll = gtk_scrolled_window_new(NULL, NULL);
	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(chooseScroll),
	    GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
	chooseBox = gtk_clist_new(1);
	gtk_clist_freeze(GTK_CLIST(chooseBox));
	gtk_clist_set_selection_mode(GTK_CLIST(chooseBox), GTK_SELECTION_BROWSE);
	gtk_clist_set_shadow_type(GTK_CLIST(chooseBox), GTK_SHADOW_IN);
	/* hack!!! */
	gtk_clist_set_column_width(GTK_CLIST(chooseBox), 0, 200);
	for (il = interfaceList; il; il = il->next) {
	    int row;
	    interface *i = il->data;
	    if((i != NULL) &&
	       (i->friendlyName != NULL) &&
	       strlen(i->friendlyName)) {
	        row = gtk_clist_append(GTK_CLIST(chooseBox), &i->friendlyName);
	    } else
	    if((i != NULL) &&
	       (i->interfaceName != NULL) &&
	       strlen(i->interfaceName)) {
	        row = gtk_clist_append(GTK_CLIST(chooseBox), &i->interfaceName);
	    } else {
	        continue;
	    }
	    gtk_clist_set_row_data(GTK_CLIST(chooseBox), row, i);
	}

	/* set up a signal handler for the select event, and then select the
	   first interface */
	gtk_signal_connect(GTK_OBJECT(chooseBox), "select-row",
			   GTK_SIGNAL_FUNC(list_box_return_row_data), &ret);
	gtk_clist_select_row(GTK_CLIST(chooseBox), 0, 0);

	gtk_scrolled_window_add_with_viewport(GTK_SCROLLED_WINDOW(chooseScroll), chooseBox);
	gtk_clist_thaw(GTK_CLIST(chooseBox));


	d = gnome_dialog_new (
	    _("Choose"),
	    GNOME_STOCK_BUTTON_OK,
	    GNOME_STOCK_BUTTON_CANCEL,
	    NULL);
	l = gtk_label_new(_("Choose an interface:"));
	gtk_box_pack_start (GTK_BOX (GNOME_DIALOG (d)->vbox), l, TRUE, TRUE, 0);
	gtk_box_pack_start (GTK_BOX (GNOME_DIALOG (d)->vbox), chooseScroll, TRUE, TRUE, 0);

	gtk_widget_show_all(d);
	gnome_dialog_close_hides(GNOME_DIALOG (d), TRUE);
	appapplet_gnome_dialog_set_parent(GNOME_DIALOG(d));
	button = gnome_dialog_run_and_close (GNOME_DIALOG (d));

	switch (button) {
	    case 0:
		/* ret was set by a callback */
		break;
	    case -1: /* User closed window with the mouse */
	    case 1:
		ret = NULL;
		break;
	}

    } else {
	/* no non-lo interfaces available!?!  Run rp3-config and try again
	 * if user has created an interface
	 */
	properties_rp3config_show();
	interfaceList = interface_list_generate();
	if (interfaceList) {
	    if (interfaceList->next)
		ret = properties_interface_choose();
	    else
		ret = interfaceList->data;
	} else {
	    ret = NULL;
	}
    }

    g_list_free(interfaceList);

    return ret;
}



/* properties dialog, including all helper functions and static data
 */
static GtkWidget *properties;
static GtkWidget *buttonName;
static GtkWidget *watchTime, *watchCost;
static GtkWidget *chooseOptionMenu, *chooseMenu;
static GtkWidget *confirmStart, *confirmStop;
static GtkWidget *terminateOnClose;
static GtkWidget *currencySymbol,
		 *currencyRateHour, *currencyRateMinute, *currencyRateSecond,
		 *currencyRate;
static GtkWidget *initialCost, *minimumSeconds, *secondsPerTick;
static GtkWidget *cumulativeCost, *currentCost;

static void
properties_interface_name_set(GtkWidget *widget, gpointer callbackData) {
    interface *i = callbackData;

    gtk_object_set_user_data(GTK_OBJECT(widget->parent), i);
    gnome_property_box_changed(GNOME_PROPERTY_BOX(properties));
}

static void
properties_dialog_apply(GtkWidget *dialog, gint page_num, gpointer data) {
    interface *i, *oldi;
    GtkWidget *netmon;

    if (page_num == -1) {
	return;
    }

    /* get all values and store them */
    oldi = gtk_object_get_user_data(GTK_OBJECT(properties));
    i = gtk_object_get_user_data(GTK_OBJECT(chooseMenu));
    if (!appapplet_is_usernet()) {
	/* don't modify netmon in usernet, where it all exists already */
	netmon = appapplet_get_netmon(oldi);
	appapplet_set_netmon(i, netmon);
	display_interface_set(netmon, i);
	appapplet_set_popup_menu(i, NULL, NULL, NULL);
    }
    appapplet_set_interface_name(i->interfaceName);
    conf_set_bool(i->interfaceName, "watchCost",
	(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON (watchCost))));
    conf_set_bool(i->interfaceName, "confirmStart",
	(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON (confirmStart))));
    conf_set_bool(i->interfaceName, "confirmStop",
	(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON (confirmStop))));
    conf_set_bool(i->interfaceName, "terminateOnClose",
	(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON (terminateOnClose))));

    conf_set_string("default", "currencySymbol",
	gtk_entry_get_text(GTK_ENTRY(currencySymbol)));
    if (GTK_TOGGLE_BUTTON (currencyRateHour)->active)
	i->priceUnit = 60*60;
    else if (GTK_TOGGLE_BUTTON (currencyRateMinute)->active)
	i->priceUnit = 60;
    else
	i->priceUnit = 1;
    conf_set_int(i->interfaceName, "PriceUnit", i->priceUnit);
    i->pricePerSecond =
	atof(gtk_entry_get_text(GTK_ENTRY(currencyRate))) / i->priceUnit;
    conf_set_float(i->interfaceName, "pricePerSecond", i->pricePerSecond);
    i->initialCost = atof(gtk_entry_get_text(GTK_ENTRY(initialCost)));
    conf_set_float(i->interfaceName, "initialCost", i->initialCost);
    i->secondsPerTick = atoi(gtk_entry_get_text(GTK_ENTRY(secondsPerTick)));
    if (i->secondsPerTick < 1)
	i->secondsPerTick = 1; /* avoid division by zero */
    conf_set_int(i->interfaceName, "secondsPerTick", i->secondsPerTick);
    i->minimumSeconds = atoi(gtk_entry_get_text(GTK_ENTRY(minimumSeconds)));
    conf_set_int(i->interfaceName, "minimumSeconds", i->minimumSeconds);
    i->cumulativeCost =
	gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON (cumulativeCost));
    conf_set_bool(i->interfaceName, "cumulativeCost", i->cumulativeCost);
    i->currentCost = atof(gtk_entry_get_text(GTK_ENTRY(currentCost)));
    conf_set_float(i->interfaceName, "currentCost", i->currentCost);

    if (!i->cumulativeCost) {
	/* this must come after the currentCost setting, because the cost
	* calculation relies on currentCost being zero of cumulativeCost
	* is not set
	*/
	i->currentCost = 0.0;
	conf_set_float(i->interfaceName, "currentCost", i->currentCost);
	conf_sync();
    }

    conf_sync();
}

static void
properties_dialog_help(GtkWidget *dialog, gint page_num, gpointer data) {
    static GnomeHelpMenuEntry help_ref = { "rp3", "s1-screens-properties.html" };
    gnome_help_display(NULL, &help_ref);
}

static void
properties_dialog_close(GtkWidget *dialog) {
    properties = NULL;
}


/* run rp3-config
 */
void
properties_rp3config_show_nowait(AppletWidget *applet, gpointer data) {
    pid_t child;

    child = fork_exec(0, "/usr/bin/rp3-config", NULL);
    display_wait(child, NULL, NULL, 0); /* clean up later */
}

static void
properties_quit_main(gpointer data, gint source, GdkInputCondition condition)
{
    char byte;
    gdk_input_remove(sigchld_notify_pipe[2]);
    read(source, &byte, 1);
    gtk_main_quit();
}

static void
properties_sigchld_handler(int signum)
{
    write(sigchld_notify_pipe[1], "1", 1);
}

void
properties_rp3config_show(void) {
    pid_t child;
    struct sigaction act;

    if(pipe(sigchld_notify_pipe) == -1) {
	return;
    }

    memset(&act, sizeof(act), 0);
    act.sa_handler = properties_sigchld_handler;
    sigaction(SIGCHLD, &act, NULL);

    sigchld_notify_pipe[2] = gdk_input_add(sigchld_notify_pipe[0],
		    			   GDK_INPUT_READ,
					   properties_quit_main,
					   NULL);

    child = fork_exec(0, "/usr/bin/rp3-config", NULL);

    gtk_main();
}

void
properties_dialog_show(AppletWidget *app, gpointer data) {
    int menuIndex = -1, n;
    GList *interfaceList, *item;
    interface *defaultInterface = data, *i = NULL;
    char stringValue[50]; /* big enough to put a float or an int in */
    GtkWidget *nameBox, *watchBox, *currencyBox;
    GtkWidget *basic, *costs;
    GtkWidget *watchLabel, *currencyRateLabel;
    GtkWidget *initialCostLabel, *minimumSecondsLabel;
    GtkWidget *secondsPerTickLabel, *currentCostLabel;
    GtkWidget *initialCostBox, *minimumSecondsBox;
    GtkWidget *secondsPerTickBox, *currentCostBox;
    char *currencySymbolString;

    if (properties) return;
    properties = gnome_property_box_new();
    gtk_window_set_title(GTK_WINDOW(properties), _("RP3 Properties"));

    interfaceList = interface_list_generate();
    while (!interfaceList) {
	properties_rp3config_show();
	interfaceList = interface_list_generate();
	if (!interfaceList) {
	    /* FIXME: need warning dialog to explain why we are dying,
	     * give option to re-run rp3-config or exit
	     */
	    exit (1);
	}
    }



    chooseMenu = gtk_menu_new();
    for (item=interfaceList, n=0; item; n++, item = item->next) {
	GtkWidget *menuItem;

	i = item->data;
	menuItem = gtk_menu_item_new_with_label(i->friendlyName);
	gtk_widget_show(menuItem);
	/* if not specified, first should be default */
	if (!defaultInterface) defaultInterface = i;
	if (defaultInterface == i) {
	    menuIndex = n;
	}
	gtk_menu_append (GTK_MENU(chooseMenu), menuItem);
	gtk_signal_connect (GTK_OBJECT (menuItem), "activate",
	    GTK_SIGNAL_FUNC (properties_interface_name_set), (gpointer) i);
    }

    if (menuIndex == -1) {
	/* apparantly, the data item refers to a non-manipulable interface */
	GtkWidget *menuItem;

	menuItem = gtk_menu_item_new_with_label(defaultInterface->friendlyName);
	gtk_widget_show(menuItem);
	gtk_menu_append (GTK_MENU(chooseMenu), menuItem);
	gtk_signal_connect (GTK_OBJECT (menuItem), "activate",
	    GTK_SIGNAL_FUNC (properties_interface_name_set),
	    (gpointer) defaultInterface);
	menuIndex = n;
    }


    /* Create all the widgets we'll be using */
    buttonName = gtk_label_new(_("Connection name: "));

    gtk_object_set_user_data(GTK_OBJECT(chooseMenu), defaultInterface);
    gtk_object_set_user_data(GTK_OBJECT(properties), defaultInterface);
    chooseOptionMenu = gtk_option_menu_new();
    gtk_option_menu_set_menu (GTK_OPTION_MENU(chooseOptionMenu), chooseMenu);
    gtk_option_menu_set_history(GTK_OPTION_MENU(chooseOptionMenu), menuIndex);

    confirmStart = gtk_check_button_new_with_label (_("Confirm starting connection"));
    confirmStop = gtk_check_button_new_with_label (_("Confirm stopping connection"));
    terminateOnClose = gtk_check_button_new_with_label (_("Terminate connection when session closes"));

    watchLabel = gtk_label_new (_("Count: "));
    watchTime = gtk_radio_button_new_with_label(NULL, _("time"));
    watchCost = gtk_radio_button_new_with_label(
	gtk_radio_button_group(GTK_RADIO_BUTTON(watchTime)), _("cost"));

    currencySymbol = gtk_entry_new_with_max_length(1);
    gtk_widget_set_usize(GTK_WIDGET(currencySymbol), 18, -1);
    currencyRateLabel = gtk_label_new(_("Cost per: "));
    currencyRateHour = gtk_radio_button_new_with_label(NULL, _("Hour"));
    currencyRateMinute = gtk_radio_button_new_with_label(gtk_radio_button_group (
	GTK_RADIO_BUTTON(currencyRateHour)), _("Minute"));
    currencyRateSecond = gtk_radio_button_new_with_label(gtk_radio_button_group (
	GTK_RADIO_BUTTON(currencyRateMinute)), _("Second"));
    gtk_radio_button_group (GTK_RADIO_BUTTON(currencyRateSecond));
    currencyRate = gtk_entry_new();

    initialCostLabel = gtk_label_new (_("Extra cost to start a connection: "));
    initialCost = gtk_entry_new();
    secondsPerTickLabel = gtk_label_new (_("Number of seconds in basic accounting unit: "));
    secondsPerTick = gtk_entry_new();
    minimumSecondsLabel = gtk_label_new (_("Minimum number of seconds charged: "));
    minimumSeconds = gtk_entry_new();
    cumulativeCost = gtk_check_button_new_with_label (_("Accumulate cost over multiple connections"));
    currentCost = gtk_entry_new();
    currentCostLabel = gtk_label_new (_("Cost accumulated from past connections:"));


    /* Fill in all those widgets */
    if (conf_get_bool(defaultInterface->interfaceName, "watchCost", FALSE)) {
	gtk_toggle_button_set_active( GTK_TOGGLE_BUTTON(watchCost), 1);
    } else {
	gtk_toggle_button_set_active( GTK_TOGGLE_BUTTON(watchTime), 1);
    }

    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(confirmStart),
	conf_get_bool(defaultInterface->interfaceName, "confirmStart", TRUE));
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(confirmStop),
	conf_get_bool(defaultInterface->interfaceName, "confirmStop", TRUE));
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(terminateOnClose),
	conf_get_bool(defaultInterface->interfaceName, "terminateOnClose", FALSE));

    currencySymbolString = conf_get_string("default", "currencySymbol", "$");
    gtk_entry_set_text(GTK_ENTRY(currencySymbol), currencySymbolString);
    g_free(currencySymbolString);
    defaultInterface->priceUnit = conf_get_int(defaultInterface->interfaceName, "PriceUnit", 60*60);
    switch (defaultInterface->priceUnit) {
    case 60*60:
	gtk_toggle_button_set_active( GTK_TOGGLE_BUTTON(currencyRateHour), 1);
	break;
    case 60:
	gtk_toggle_button_set_active( GTK_TOGGLE_BUTTON(currencyRateMinute), 1);
	break;
    default:
	gtk_toggle_button_set_active( GTK_TOGGLE_BUTTON(currencyRateSecond), 1);
	break;
    }
    sprintf(stringValue, "%.2f", defaultInterface->pricePerSecond * defaultInterface->priceUnit);
    gtk_entry_set_text(GTK_ENTRY(currencyRate), stringValue);

    sprintf(stringValue, "%.2f", defaultInterface->initialCost);
    gtk_entry_set_text(GTK_ENTRY(initialCost), stringValue);
    sprintf(stringValue, "%d", defaultInterface->secondsPerTick);
    gtk_entry_set_text(GTK_ENTRY(secondsPerTick), stringValue);
    sprintf(stringValue, "%d", defaultInterface->minimumSeconds);
    gtk_entry_set_text(GTK_ENTRY(minimumSeconds), stringValue);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(cumulativeCost),
	conf_get_bool(defaultInterface->interfaceName, "cumulativeCost", FALSE));
    sprintf(stringValue, "%.2f", defaultInterface->currentCost);
    gtk_entry_set_text(GTK_ENTRY(currentCost), stringValue);


    /* set up signals to activate OK/Apply.  Wish C had "foreach"... */
    gtk_signal_connect_object (GTK_OBJECT (watchTime), "toggled",
	GTK_SIGNAL_FUNC (gnome_property_box_changed), GTK_OBJECT(properties));
    gtk_signal_connect_object (GTK_OBJECT (confirmStart), "toggled",
	GTK_SIGNAL_FUNC (gnome_property_box_changed), GTK_OBJECT(properties));
    gtk_signal_connect_object (GTK_OBJECT (confirmStop), "toggled",
	GTK_SIGNAL_FUNC (gnome_property_box_changed), GTK_OBJECT(properties));
    gtk_signal_connect_object (GTK_OBJECT (terminateOnClose), "toggled",
	GTK_SIGNAL_FUNC (gnome_property_box_changed), GTK_OBJECT(properties));
    gtk_signal_connect_object (GTK_OBJECT (currencySymbol), "changed",
	GTK_SIGNAL_FUNC (gnome_property_box_changed), GTK_OBJECT(properties));
    gtk_signal_connect_object (GTK_OBJECT (currencyRate), "changed",
	GTK_SIGNAL_FUNC (gnome_property_box_changed), GTK_OBJECT(properties));
    gtk_signal_connect_object (GTK_OBJECT (currencyRateHour), "toggled",
	GTK_SIGNAL_FUNC (gnome_property_box_changed), GTK_OBJECT(properties));
    gtk_signal_connect_object (GTK_OBJECT (currencyRateMinute), "toggled",
	GTK_SIGNAL_FUNC (gnome_property_box_changed), GTK_OBJECT(properties));
    gtk_signal_connect_object (GTK_OBJECT (currencyRateSecond), "toggled",
	GTK_SIGNAL_FUNC (gnome_property_box_changed), GTK_OBJECT(properties));
    gtk_signal_connect_object (GTK_OBJECT (initialCost), "changed",
	GTK_SIGNAL_FUNC (gnome_property_box_changed), GTK_OBJECT(properties));
    gtk_signal_connect_object (GTK_OBJECT (secondsPerTick), "changed",
	GTK_SIGNAL_FUNC (gnome_property_box_changed), GTK_OBJECT(properties));
    gtk_signal_connect_object (GTK_OBJECT (minimumSeconds), "changed",
	GTK_SIGNAL_FUNC (gnome_property_box_changed), GTK_OBJECT(properties));
    gtk_signal_connect_object (GTK_OBJECT (cumulativeCost), "toggled",
	GTK_SIGNAL_FUNC (gnome_property_box_changed), GTK_OBJECT(properties));
    gtk_signal_connect_object (GTK_OBJECT (currentCost), "changed",
	GTK_SIGNAL_FUNC (gnome_property_box_changed), GTK_OBJECT(properties));


    /* now pack everything together */
    basic = gtk_vbox_new(FALSE, 0);

    nameBox = gtk_hbox_new(FALSE, 0);
    gtk_box_pack_start(GTK_BOX(nameBox), buttonName, FALSE, FALSE, 0);
    gtk_box_pack_start(GTK_BOX(nameBox), chooseOptionMenu, FALSE, FALSE, 0);
    gtk_box_pack_start(GTK_BOX(basic), nameBox, FALSE, FALSE, 5);

    watchBox = gtk_hbox_new(FALSE, 0);
    gtk_box_pack_start(GTK_BOX(watchBox), watchLabel, FALSE, FALSE, 0);
    gtk_box_pack_start(GTK_BOX(watchBox), watchTime, FALSE, FALSE, 0);
    gtk_box_pack_start(GTK_BOX(watchBox), watchCost, FALSE, FALSE, 0);
    gtk_box_pack_start(GTK_BOX(basic), watchBox, FALSE, FALSE, 5);

    gtk_box_pack_start(GTK_BOX(basic), confirmStart, FALSE, FALSE, 5);
    gtk_box_pack_start(GTK_BOX(basic), confirmStop, FALSE, FALSE, 5);
    gtk_box_pack_start(GTK_BOX(basic), terminateOnClose, FALSE, FALSE, 5);

    gnome_property_box_append_page(GNOME_PROPERTY_BOX(properties), basic,
	gtk_label_new(_("Basic")));


    costs = gtk_vbox_new(FALSE, 0);

    currencyBox = gtk_hbox_new(FALSE, 0);
    gtk_box_pack_start(GTK_BOX(currencyBox), currencyRateLabel, FALSE, FALSE, 0);
    gtk_box_pack_start(GTK_BOX(currencyBox), currencyRateHour, FALSE, FALSE, 0);
    gtk_box_pack_start(GTK_BOX(currencyBox), currencyRateMinute, FALSE, FALSE, 0);
    gtk_box_pack_start(GTK_BOX(currencyBox), currencyRateSecond, FALSE, FALSE, 0);
    gtk_box_pack_start(GTK_BOX(currencyBox), currencySymbol, FALSE, FALSE, 0);
    gtk_box_pack_start(GTK_BOX(currencyBox), currencyRate, FALSE, FALSE, 0);
    gtk_box_pack_start(GTK_BOX(costs), currencyBox, FALSE, FALSE, 5);

    initialCostBox = gtk_hbox_new(FALSE, 0);
    gtk_box_pack_start(GTK_BOX(initialCostBox), initialCostLabel, FALSE, FALSE, 0);
    gtk_box_pack_start(GTK_BOX(initialCostBox), initialCost, FALSE, FALSE, 0);
    gtk_box_pack_start(GTK_BOX(costs), initialCostBox, FALSE, FALSE, 5);

    secondsPerTickBox = gtk_hbox_new(FALSE, 0);
    gtk_box_pack_start(GTK_BOX(secondsPerTickBox), secondsPerTickLabel, FALSE, FALSE, 0);
    gtk_box_pack_start(GTK_BOX(secondsPerTickBox), secondsPerTick, FALSE, FALSE, 0);
    gtk_box_pack_start(GTK_BOX(costs), secondsPerTickBox, FALSE, FALSE, 5);

    minimumSecondsBox = gtk_hbox_new(FALSE, 0);
    gtk_box_pack_start(GTK_BOX(minimumSecondsBox), minimumSecondsLabel, FALSE, FALSE, 0);
    gtk_box_pack_start(GTK_BOX(minimumSecondsBox), minimumSeconds, FALSE, FALSE, 0);
    gtk_box_pack_start(GTK_BOX(costs), minimumSecondsBox, FALSE, FALSE, 5);

    gtk_box_pack_start(GTK_BOX(costs), cumulativeCost, FALSE, FALSE, 5);

    currentCostBox = gtk_hbox_new(FALSE, 0);
    gtk_box_pack_start(GTK_BOX(currentCostBox), currentCostLabel, FALSE, FALSE, 0);
    gtk_box_pack_start(GTK_BOX(currentCostBox), currentCost, FALSE, FALSE, 0);
    gtk_box_pack_start(GTK_BOX(costs), currentCostBox, FALSE, FALSE, 5);

    gnome_property_box_append_page(GNOME_PROPERTY_BOX(properties), costs,
	gtk_label_new(_("Cost")));


    /* hook up the buttons */
    gtk_signal_connect_object (GTK_OBJECT (properties), "apply",
	GTK_SIGNAL_FUNC (properties_dialog_apply), GTK_OBJECT(properties));
    gtk_signal_connect_object (GTK_OBJECT (properties), "help",
	GTK_SIGNAL_FUNC (properties_dialog_help), GTK_OBJECT(properties));
    gtk_signal_connect_object (GTK_OBJECT (properties), "destroy",
	GTK_SIGNAL_FUNC (properties_dialog_close), GTK_OBJECT(properties));

    gtk_widget_show_all(basic);
    gtk_widget_show_all(costs);
    gtk_widget_show_all(properties);

    g_list_free(interfaceList); /* don't free the components, let them be cached */
}

/* about dialog
 * (dunno where else to put it...)
 */
void
properties_about_show(AppletWidget *app, gpointer data) {
    const gchar *authors[] = {
	"Jonathan Blandford <jrb@redhat.com>",
	"Nalin Dahyabhai <nalin@redhat.com>",
	"Michael K. Johnson <johnsonm@redhat.com>",
	"Havoc Pennington <hp@redhat.com>",
	 NULL,
    };
    gtk_widget_show (gnome_about_new ("rp3", VERSION,
	"Copyright 1999, 2000 Red Hat, Inc.",
	(const gchar **) authors,
	_("rp3, the Red Hat PPP tool, manages PPP connections,"
	  "and, to a lesser extent other types of network connections."),
	NULL));
}

/* update the currentCost if appropriate
 * Another one that doesn't seem to fit anywhere convenient...
 */
void
properties_update_saved_cost(interface *i) {

    if (!i->cumulativeCost) return;

    i->currentCost = interface_current_cost(i);
    conf_set_float(i->interfaceName, "currentCost", i->currentCost);
    conf_sync();
}
