/* Red Hat PPP Dialer applet/application
 * Copyright 1999 Red Hat, Inc.
 *
 * Author: Michael K. Johnson <johnsonm@redhat.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Cambridge, MA 
 * 02139, USA.
 *
 */

#include <config.h>

#include <string.h>

#include <gnome.h>
#include <applet-widget.h>

#include "appapplet.h"
#include "conf.h"
#include "properties.h"
#include "display.h"

static void
applet_change_orient(GtkWidget *w, PanelOrientType o, gpointer data)
{
  switch (o)
    {
    case ORIENT_UP:
    case ORIENT_DOWN:
      display_set_orientation (data, GTK_NETMON_HORIZONTAL);
      break;

    case ORIENT_LEFT:
    case ORIENT_RIGHT:
      display_set_orientation (data, GTK_NETMON_VERTICAL);
      break;
  }
}


/*this is when the panel size changes*/
static void
applet_change_pixel_size(GtkWidget *w, int size, gpointer data)
{
  display_set_panel_size (data, size);
}


static char *chosenInterface = NULL;
static char *chosenTitle = NULL;
static struct poptOption options[] = {
    { "interface", 'i', POPT_ARG_STRING, &chosenInterface, 0,
      N_("logical name of interface to use (eg, ppp0)"), N_("INTERFACE")},
    { "swallow", '\0', POPT_ARG_STRING, &chosenTitle, 0,
      N_("set window title for swallowing"), N_("WINDOW-TITLE")},
    { NULL, '\0', 0, NULL, 0, NULL, NULL},
};


/* Start up as a panel applet */
static int
applet_main(int argc, char **argv) {
    GtkWidget *applet, *display;
    interface *i;

    applet_widget_init(PACKAGE, VERSION, argc, argv, options, 0, NULL);

    applet = applet_widget_new(PACKAGE);
    appapplet_set_is_applet(APPLET_WIDGET(applet));
    if (!chosenInterface) {
	gnome_config_push_prefix(APPLET_WIDGET(applet)->privcfgpath);
	chosenInterface = gnome_config_get_string("rp3/interfaceName");
	gnome_config_pop_prefix();
    }
    i = properties_interface_get(&chosenInterface);
    if (!chosenInterface) {
        gtk_main_quit();
	return 0;
    }
    if (!i) {
        g_print(_("Interface %s does not appear to exist."), chosenInterface);
    }

    display = display_interface_create(i);
    applet_widget_add(APPLET_WIDGET(applet), display);
    appapplet_set_popup_menu(i, display, NULL, NULL);

    gtk_widget_show_all(applet);
    appapplet_start_session_management(argv, i->interfaceName);

    gtk_signal_connect (GTK_OBJECT (applet), "change_pixel_size",
                        GTK_SIGNAL_FUNC (applet_change_pixel_size),
                        display);
    
    gtk_signal_connect (GTK_OBJECT (applet), "change_orient",
                        GTK_SIGNAL_FUNC (applet_change_orient),
                        display);
    
    applet_widget_gtk_main();
    return 0;
}

/* start up as a single-interface application.  The purpose of this one
 * is for the user to be able to drag an interface from rp3-config onto
 * the desktop and for the desktop manager (i.e. mc for now...) to be
 * able to start an application from that icon.
 */
static int
application_main(int argc, char **argv) {
    GtkWidget *app, *display;
    interface *i;

    gnome_init_with_popt_table(PACKAGE, VERSION, argc, argv, options, 0, NULL);

    app = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    appapplet_set_is_app(app, chosenTitle, FALSE);
    i = properties_interface_get(&chosenInterface);
    if (!chosenInterface) {
	return 0;
    }
    if (!i) {
        g_error(_("Interface %s does not appear to exist."), chosenInterface);
    }

    if (!chosenTitle) {
	gtk_window_set_default_size (GTK_WINDOW (app), 125, 75);
	gtk_window_set_title(GTK_WINDOW(app), i->friendlyName);
    }
    display = display_interface_create(i);
    gtk_container_add(GTK_CONTAINER(app), display);
    appapplet_set_popup_menu(i, display, display, NULL);

    gtk_signal_connect(GTK_OBJECT(app), "destroy",
	GTK_SIGNAL_FUNC(gtk_main_quit), NULL);

    gtk_widget_show_all(app);
    appapplet_start_session_management(argv, i->interfaceName);
    gtk_main();
    return 0;
}

/* replace the usernet program.  In vertical format (extra points if we
 * make horizontal a configuration choice... :-) we put one interface
 * display for each interface on the system, and put up tooltips and
 * attach connect/disconnect signals for every interface we can control.
 */
static int
usernet_main(int argc, char **argv) {
    GtkWidget *app, *box, *display, *labelEb, *label;
    interface *i;
    GList *ilist, *listitem;

    gnome_init(PACKAGE, VERSION, argc, argv);

    app = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    appapplet_set_is_app(app, chosenTitle, TRUE);
    box = gtk_vbox_new(FALSE, 0);
    ilist = interface_no_clone_list_generate();
    if (!ilist) return 0;

    gtk_window_set_title(GTK_WINDOW(app), "usernet");
    for (listitem = ilist; listitem; listitem = listitem->next) {
	i = listitem->data;

	labelEb = gtk_event_box_new();
	label = gtk_label_new(i->friendlyName);
	gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
	gtk_misc_set_padding(GTK_MISC(label), 5, 0);
	gtk_container_add(GTK_CONTAINER(labelEb), label);
	display = display_interface_create(i);

	appapplet_set_popup_menu(i, display, display, labelEb);
	gtk_box_pack_start(GTK_BOX(box), labelEb, FALSE, FALSE, 2);
	gtk_box_pack_start(GTK_BOX(box), display, TRUE, TRUE, 2);
    }
    gtk_container_add(GTK_CONTAINER(app), box);

    gtk_signal_connect(GTK_OBJECT(app), "destroy",
	GTK_SIGNAL_FUNC(gtk_main_quit), NULL);

    gtk_widget_show_all(app);
    appapplet_start_session_management(argv, NULL);
    gtk_main();
    return 0;
}

int
main(int argc, char **argv) {
    char *appname;

    bindtextdomain(PACKAGE, GNOMELOCALEDIR);
    textdomain(PACKAGE);

    appname = strrchr(argv[0], '/');
    if (appname) appname++;	/* go past the / character */
    else appname = argv[0];	/* no / in argv[0] to start with... */
    if (appname && !strcmp(appname, "usernet")) {
	return usernet_main(argc, argv);
    }
    if (argc > 1 && !strncmp("--activate-goad-server", argv[1], 22)) {
	return applet_main(argc, argv);
    }
    return application_main(argc, argv);
}
