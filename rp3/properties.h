/* Red Hat PPP Dialer applet/application
 * Copyright 1999 Red Hat, Inc.
 *
 * Author: Michael K. Johnson <johnsonm@redhat.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Cambridge, MA 
 * 02139, USA.
 *
 */

#ifndef _PROPERTIES_H 
#define _PROPERTIES_H 

#include "interface.h"

/* returns interface to use, given command-line hint
 * checks to make sure that chosenInterface exists and is user-manageable
 * if not, calls properties_interface_name_choose.
 * If this function returns NULL, no interface is available and no
 * interface was made available, and the program should either
 * exit or grey itself out or something like that.
 */
interface *
properties_interface_get(char **chosenInterface);

/* offers list of existing interfaces (if any) and the option
 * to set up a new interface.  Modal; does not return until
 * it has an interface or has officially given up (NULL).
 */
interface *
properties_interface_choose(void);

/* run rp3-config
 */
void
properties_rp3config_show_nowait(AppletWidget *app, gpointer data);
void
properties_rp3config_show(void);

/* properties dialog
 */
void
properties_dialog_show(AppletWidget *app, gpointer data);

/* about dialog
 * (dunno where else to put it...)
 */
void
properties_about_show(AppletWidget *app, gpointer data);

/* update the currentCost if appropriate
 * Another one that doesn't seem to fit anywhere convenient...
 */
void
properties_update_saved_cost(interface *i);

#endif /* _PROPERTIES_H */
