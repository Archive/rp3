<chapter id="screens">
    <title>Help Screens</title>

    <sect1 id="s1-screens-create">
      <title>Create a new Internet connection</title>
      <para>
      Welcome to the Red Hat PPP Dialer! This tool will help you set up a new
      Internet connection. You'll need the name and telephone number of your
      ISP, as well as your user name and password.  </para>
      <para>
      To continue, select the <guibutton>Next</guibutton> button. You can stop
      at any time by selecting <guibutton>Cancel</guibutton>, and your changes
      will not be saved. You can also move to previous screens during the
      configuration by using the <guibutton>Back</guibutton> button.
      </para>

    <sect2 id="s2-screens-modem">
      <title>Select a modem</title>
      <para>
	You don't appear to have a modem configured yet. You must configure one
	before continuing.
      </para>
      <para>
      Select the <guibutton>Next</guibutton>, and the Dialer will attempt to
      auto-detect your modem.
      </para>
  </sect2>

  <sect2 id="s2-screens-modem-enter">
      <title>Enter a modem</title>
      <para>
	Sorry; no modems were detected on your system. Please enter your modem
	settings manually below. Select the modem location and highest
	recommended baud rate.
      </para>
      <para>
      (Hint: If your modem is located at COM1 in MS-DOS, it's /dev/ttyS0 in
      Linux; COM2 is /dev/ttyS1, COM3 is /dev/ttyS2, COM4 is /dev/ttyS3.)
      </para>
  </sect2>


  <sect2 id="s2-screens-number">
      <title>Phone number and name</title>
      <para>
	What name do you want to call your new Internet connection?
      </para>
      <para>
      Enter the Internet provider's phone number. You can also enter the prefix
      and area code. If you must dial a number to reach an outside line, enter
      the number in the <guilabel>Prefix</guilabel> field.
      </para>
  </sect2>


  <sect2 id="s2-screens-user-name">
      <title>User name and password</title>
      <para>
	What login name do you use when you connect to your Internet account?
      Enter the name in the <guilabel>User name</guilabel> field.
      </para>
      <para>
      Enter your password for this account.
      </para>
  </sect2>



  <sect2 id="s2-screens-options">
      <title>Other options</title>
      <para>
	If you see your Internet provider listed below, please select that
	entry. If you don't see yours listed, choosing "Generic Account" will be fine.
      </para>
  </sect2>


  <sect2 id="s2-screens-info">
      <title>Create the account?</title>
      <para>
	You have provided the following information to create your new Internet 
account:
      </para>
      <para>
      If the information is correct, press <guibutton>Finish</guibutton> to
      create the account. If you would like to make corrections, press the
      <guibutton>Back</guibutton> button to return to previous entries.
      </para>
  </sect2>

  </sect1>



<sect1 id="s1-screens-accounts-edit">

    <title>Internet Connections</title>
    <para>

From this window, you can view information about your current Internet 
accounts,
      such as the account name, telephone number and the status of the account
      (such as whether the account is active, or connected).
    </para>
<para>
You can perform the following actions by using the menu to the right:

 <itemizedlist mark="bullet">
	<listitem>
	  <para>
          New -- Create a new Internet account, using the configuration 
utility.
	  </para>
	</listitem>

	<listitem>
	  <para>
          Delete -- Delete an account.
	  </para>
	</listitem>

	<listitem>
	  <para>
          Edit -- Open a currently configured account and modify its 
properties,
          such as the account name, telephone number and your modem location.
	  </para>
	</listitem>

	<listitem>
	  <para>
	    Copy -- Copy the account information to a new field (handy for
	    saving a backup copy of the configuration while you edit another
	    copy).
	  </para>
	</listitem>

	<listitem>
	  <para>
          Dial -- Dial the account from this window (useful for testing account
          information, as well as for connecting).
	  </para>
	</listitem>
      </itemizedlist>
    </para>
  </sect1>

<sect1 id="s1-screens-edit-connection">
<title>Edit Internet Connection</title>
<para>
In this tool, you can modify your Internet account's name and telephone number,
      as well as your account's login name and password.
    </para>
<para>
      You can also change the location of your modem and dialing options.
</para>
<para>
      In the <guilabel>Account Info</guilabel> tab, you can modify the 
following
      fields:

 <itemizedlist mark="bullet">
	<listitem>
	  <para>
	    <guilabel>Account Name</guilabel>: The name you want to give this
	    Internet account.
	  </para>
	</listitem>

	<listitem>
	  <para>
	    <guilabel>Username</guilabel>: The login name you use when you
	    connect to this Internet account.
	  </para>
	</listitem>
	<listitem>
	  <para>
	    <guilabel>Password</guilabel>: The password you use when you sign on
	    to the account.
	  </para>
	</listitem>
	<listitem>
	  <para>
	    <guilabel>Dialing options</guilabel>: Includes the prefix, area code
	    and telephone number to access this Internet account. If you're
	    required to dial a number to reach an outside line (for example,
	    "9"), enter that number in the <guilabel>Prefix</guilabel> field.
	  </para>
	</listitem>
      </itemizedlist>

      In the <guilabel>Modem</guilabel> tab, you can specify a new location for
      your modem, either by typing in the modem's location or by asking the
      dialer to attempt auto-detection by clicking the <guibutton>Find
      Modem</guibutton> button.

If you are familiar with your modem's location in MS-DOS, here is a list of
      corresponding locations in Linux:

 <itemizedlist mark="bullet">
	<listitem>
	  <para>
	    COM1 = <filename>/dev/ttyS0</filename>
	  </para>
	</listitem>

	<listitem>
	  <para>
	    COM2 = <filename>/dev/ttyS1</filename>
	  </para>
	</listitem>
	<listitem>
	  <para>
	    COM3 = <filename>/dev/ttyS2</filename>
	  </para>
	</listitem>
	<listitem>
	  <para>
	    COM4 = <filename>/dev/ttyS3</filename>
	  </para>
	</listitem>
      </itemizedlist>
    </para>
  </sect1>




<sect1 id="s1-screens-properties">
<title>RP3 Properties</title>
<para>
In this window, you can customize some of the ways the Red Hat PPP Dialer works
      with your connections. You can also switch your connections, and modify
      some connection details.
    </para>
<para>
In the Basic page, you can adjust the following settings:
    </para>
 <itemizedlist mark="bullet">
	<listitem>
	  <para>
	  <guilabel>Connection name</guilabel>: This drop-down list contains the
	  names of your Internet accounts and, if you have a network interface,
	  the name of the interface (for example
	  <filename>eth0</filename>). Choosing a different entry in this list
	  allows you to switch connections "on the fly." (If you have started the
	  Red Hat PPP Dialer by selecting <application>Usernet</application>, which
	  monitors all user-controllable network devices, your changes will be
	  reflected on the particular device you want to modify, leaving other
	  devices unmodified.)
	  </para>
	</listitem>

	<listitem>
	  <para>
	  <guilabel>Count</guilabel>: You can elect to view your online activity
	  by the time you're spending, or by the cumulative cost of the
	  connection. The second option is most useful for those who do not pay
	  a flat rate for their account, or must connect to their ISP while traveling.
 	  </para>
	</listitem>
	<listitem>
	  <para>
	    <guilabel>Confirm starting connection</guilabel>: This option
	    will confirm when you ask to connect to your ISP.
	</para>
	</listitem>
	<listitem>
	  <para>
	 <guilabel>Confirm stopping connection</guilabel>: This option
	   will confirm when you ask to disconnect from your ISP.   
	  </para>
	</listitem>
      </itemizedlist>
<para>
In the Cost page, you can tell rp3 how to <emphasis>estimate</emphasis> how
much your internet connection is costing you by adjusting the following
settings:
    </para>
 <itemizedlist mark="bullet">
	<listitem>
	  <para>
	  <guilabel>Cost per</guilabel>: If you have selected
	  <guilabel>cost</guilabel> in the <guilabel>Count</guilabel> option,
	  you can specify the cost of the connection per hour,
	  minute, or second, whichever is most convenient to you.
	  </para>
	</listitem>
	<listitem><para>
	  <guilabel>Extra cost to start a connection:</guilabel>
	  This is the cost to start a connection (dial the phone,
	  for example) beside charges for time.  If you are charged
	  only for time, enter 0 here.  Alternatively, since rp3
	  cannot take into account the amount of time you spend
	  dialing (it starts counting after the PPP connection
	  is established), you can estimate the average amount
	  of money it costs you to establish a connection and
	  include it here.
	</para></listitem>
	<listitem><para>
	  <guilabel>Number of seconds in basic accounting unit:</guilabel>
	  If you are charged for 90 seconds of phone use at a time even
	  if you have only used 1 of those 90 seconds, enter 90 here.
	  If you are charged per minute or fraction of a minute, enter
	  60 here.
	</para></listitem>
	<listitem><para>
	  <guilabel>Minimum number of seconds charged:</guilabel>
          If, when you start a call, you are charged for an even longer
	  period than your basic accounting unit, enter than longer
	  period here.  For example, if you are charged 0.17 units of
	  money per minute or any fraction of a minute, but you are
	  charged at for at least 5 minutes, you would enter
	  300 (that's 5 * 60) here.
	</para></listitem>
	<listitem><para>
	  <guilabel>Accumulate cost over multiple connections</guilabel>
	  Keep track of, and display, the total cost for all your
	  connections, instead of the cost for this connection only.
	  If you turn this off and then click on either
	  <guilabel>OK</guilabel> or <guilabel>Apply</guilabel>,
	  <emphasis>the saved cost will be reset to zero.</emphasis>
	</para></listitem>
	<listitem><para>
	  <guilabel>Cost accumulated from past connections:</guilabel>
	  This displays the current cost.  If you wish to reset this
	  to zero, enter 0.0 here.  If you pre-pay for your connection,
	  you can enter the amount you have paid for as a negative
	  number, and then rp3 will then display how much money remains.
	  (Note that no matter what value you place in this field,
	  it will be reset to zero if the <guilabel>Accumulate cost
	  over multiple connections</guilabel> checkbox is not
	  selected when you click on <guilabel>OK</guilabel> or
	  <guilabel>Apply</guilabel>.)
	</para></listitem>
      </itemizedlist>
  </sect1>
</chapter>
